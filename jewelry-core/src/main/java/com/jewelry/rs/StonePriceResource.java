package com.jewelry.rs;

import com.jewelry.dto.StonePriceDTO;
import com.jewelry.dto.ValueDTO;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by aramayis.antonyan on 2/28/14.
 */
public interface StonePriceResource {
    @Path("/update")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<Boolean> updatePrice(List<StonePriceDTO> stonePrices);

    @Path("/all")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<List<StonePriceDTO>> getPrices();

    @Path("/add")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<List<StonePriceDTO>> addPrice(List<StonePriceDTO> stonePrices);

    @Path("/getUpdates")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<List<StonePriceDTO>> getUpdates(DateTime startDate);
}
