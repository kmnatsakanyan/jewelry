package com.jewelry.rs;

import com.jewelry.dto.PhotoDTO;
import com.jewelry.dto.ValueDTO;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * User: karen.mnatsakanyan
 */
public interface PhotoResource {

    @Path("/save")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<Boolean> save(PhotoDTO photoDTO);

    @Path("/get")
    @GET
    @Produces("image/*")
    public Response get(@QueryParam("photo") String photo);

}
