package com.jewelry.rs;

import com.jewelry.dto.OrderDTO;
import com.jewelry.dto.ValueDTO;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public interface OrderResource {

    @Path("/add")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<OrderDTO> addOrder(OrderDTO orderDTO);

    @Path("/update")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<Boolean> updateOrder(OrderDTO orderDTO);

    @Path("/updateOrders")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<List<OrderDTO>> updateOrders(List<OrderDTO> orders);

    @Path("/getUpdates")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<List<OrderDTO>> getUpdates(DateTime startDate,@QueryParam("userId") Integer userId);

    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public ValueDTO<Boolean> delete(@PathParam("id") Integer orderId);
}
