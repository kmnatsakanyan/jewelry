package com.jewelry.rs;

import com.jewelry.dto.CatalogDTO;
import com.jewelry.dto.ValueDTO;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public interface CatalogResource {
    @Path("/add")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<CatalogDTO> addCatalog(CatalogDTO catalogDTO);

    @Path("/update")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<CatalogDTO> updateCatalog(CatalogDTO catalogDTO);

    @Path("/getUpdates")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<List<CatalogDTO>> getUpdates(DateTime startDate);

    @Path("/all")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<List<CatalogDTO>> getAll();

    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public ValueDTO<Boolean> delete(@PathParam("id") Integer userId);
}
