package com.jewelry.rs;


import com.jewelry.dto.StoneQualityDTO;
import com.jewelry.dto.ValueDTO;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
public interface StoneQualityResource {
    @Path("/qualities")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ValueDTO<List<StoneQualityDTO>> getQualities();

    @Path("/getUpdates")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<List<StoneQualityDTO>> getUpdates(DateTime startDate);

    @Path("/add")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<StoneQualityDTO> saveQuality(StoneQualityDTO quality);


    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public ValueDTO<Boolean> delete(@PathParam("id") Integer qualityId);
}
