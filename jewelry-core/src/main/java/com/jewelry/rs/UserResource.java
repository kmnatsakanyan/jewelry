package com.jewelry.rs;

import com.jewelry.dto.UserDTO;
import com.jewelry.dto.ValueDTO;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public interface UserResource {

    @Path("/")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<UserDTO> saveUser(UserDTO userDTO);

    @Path("/login")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ValueDTO<UserDTO> login(@QueryParam("email") String email, @QueryParam("password") String password);

    @Path("/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ValueDTO<List<UserDTO>> getAll();


    @Path("/getUser")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ValueDTO<UserDTO> getUser(@QueryParam("id") Integer userId);


    //Used for manual testing, do not delete .
    @Path("/test")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public DateTime test();

    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public ValueDTO<Boolean> delete(@PathParam("id") Integer userId);

}
