package com.jewelry.rs;

import com.jewelry.dto.ProductCatalogDTO;
import com.jewelry.dto.ValueDTO;
import org.joda.time.DateTime;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public interface ProductCatalogResource {
    @Path("/add")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<ProductCatalogDTO> addProductCatalog(ProductCatalogDTO productCatalogDTO);

    @Path("/update")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<ProductCatalogDTO> updateProductCatalog(ProductCatalogDTO productCatalogDTO);

    @Path("/getUpdates")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<List<ProductCatalogDTO>> getUpdates(DateTime startDate);
}
