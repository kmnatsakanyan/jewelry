package com.jewelry.rs;


import com.jewelry.dto.ProductTypeDTO;
import com.jewelry.dto.ValueDTO;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


/**
 * Created by aramayis.antonyan on 2/27/14.
 */
public interface TypeResource {
    @Path("/type")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ValueDTO<List<ProductTypeDTO>> getTypes();

    @Path("/getUpdates")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<List<ProductTypeDTO>> getUpdates(DateTime startDate);
}
