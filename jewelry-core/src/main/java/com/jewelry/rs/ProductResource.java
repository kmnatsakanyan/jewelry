package com.jewelry.rs;

import com.jewelry.dto.ProductDTO;
import com.jewelry.dto.ValueDTO;
import org.joda.time.DateTime;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Mos on 25.02.14.
 */
public interface ProductResource {

    @Path("/add")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<ProductDTO> saveProduct(ProductDTO productDTO);

    @Path("/update")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<ProductDTO> updateProduct(ProductDTO productDTO);

    @Path("/updateList")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<Boolean> updateProductList(List<ProductDTO> productDTOs);

    @Path("/getUpdates")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<List<ProductDTO>> getUpdates(DateTime startDate);
}
