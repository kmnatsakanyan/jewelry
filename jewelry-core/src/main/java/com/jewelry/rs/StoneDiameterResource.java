package com.jewelry.rs;


import com.jewelry.dto.StoneDiameterDTO;
import com.jewelry.dto.ValueDTO;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
public interface StoneDiameterResource {
    @Path("/diameters")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ValueDTO<List<StoneDiameterDTO>> getDiameters();

    @Path("/getUpdates")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<List<StoneDiameterDTO>> getUpdates(DateTime startDate);

    @Path("/add")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<StoneDiameterDTO> saveDiameter(StoneDiameterDTO diameter);

    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public ValueDTO<Boolean> delete(@PathParam("id") Integer diameterId);
}
