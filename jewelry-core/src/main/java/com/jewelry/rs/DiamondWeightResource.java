package com.jewelry.rs;


import com.jewelry.dto.DiamondWeightDTO;
import com.jewelry.dto.ValueDTO;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by aramayis.antonyan on 10/5/14.
 */
public interface DiamondWeightResource {
    @Path("/diamonds")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ValueDTO<List<DiamondWeightDTO>> getWeightList();

    @Path("/getUpdates")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<List<DiamondWeightDTO>> getUpdates(DateTime startDate);


    @Path("/reloadDiamonds")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ValueDTO<Boolean> loadDiamondWeightFromCSV();

}
