package com.jewelry.rs;

import com.jewelry.dto.OrderTypePriceDTO;
import com.jewelry.dto.OrderTypePriceUpdateDTO;
import com.jewelry.dto.ValueDTO;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by aramayis.antonyan on 2/26/14.
 */
public interface OrderTypePriceResource {
    @Path("/save")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<Boolean> updateCost(OrderTypePriceUpdateDTO orderTypePriceUpdateDTO);


    @Path("/getUpdates")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ValueDTO<List<OrderTypePriceDTO>> getUpdates();
}
