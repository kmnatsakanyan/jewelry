package com.jewelry.util;

/**
 * Created by Mos on 27.02.14.
 */
public class JewelryConstant {
    public static final int RESPONSE_OK = 200;
    public static final int RESPONSE_ERROR = 500;
    public static final int RESPONSE_UNAUTHORIZED = 401;
    public static final int RESPONSE_CATALOG_NOT_FOUND = 800;
    public static final int RESPONSE_USER_NOT_FOUND = 801;
    public static final int RESPONSE_TIMEOUT = 408;
    public static final int RESPONSE_USER_EXIST = 802;

    public static final String THUMBNAIL_PREFIX = "_tmb";
    public static final int THUMBNAIL_X = 160;
    public static final int THUMBNAIL_Y = 160;
}
