package com.jewelry.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by aramayis.antonyan on 3/5/14.
 */
public final class SystemLogger {
    /**
     * SYSTEM logger reference.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerType.SERVICE.toString());

    /**
     * @param message
     */
    public static void e(String message) {
        LOGGER.error(message);
    }

    public static void d(String message) {
        LOGGER.debug(message);
    }

    public static void i(String message) {
        LOGGER.info(message);
    }

    public static void w(String message) {
        LOGGER.warn(message);
    }

    public static boolean isDebug(){
        return LOGGER.isDebugEnabled();
    }
}
