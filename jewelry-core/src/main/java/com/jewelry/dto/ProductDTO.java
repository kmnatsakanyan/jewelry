package com.jewelry.dto;

import java.util.List;

/**
 * Created by Mos on 24.02.14.
 */
public class ProductDTO extends BaseDTO {

    private String productId;

    private Integer type;

    private Float weight;

    private List<ProductPhotoDTO> productPhotos;

    private List<ProductStoneDTO> productStones;

    private List<ProductCatalogDTO> productCatalogs;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public List<ProductPhotoDTO> getProductPhotos() {
        return productPhotos;
    }

    public void setProductPhotos(List<ProductPhotoDTO> productPhotos) {
        this.productPhotos = productPhotos;
    }

    public List<ProductStoneDTO> getProductStones() {
        return productStones;
    }

    public void setProductStones(List<ProductStoneDTO> productStones) {
        this.productStones = productStones;
    }

    public List<ProductCatalogDTO> getProductCatalogs() {
        return productCatalogs;
    }

    public void setProductCatalogs(List<ProductCatalogDTO> productCatalogs) {
        this.productCatalogs = productCatalogs;
    }
}
