package com.jewelry.dto;

/**
 * User: karen.mnatsakanyan
 */
public class ProductCatalogDTO extends BaseDTO{

    private ProductDTO product;

    private CatalogDTO catalog;

    private String productLabel;

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public CatalogDTO getCatalog() {
        return catalog;
    }

    public void setCatalog(CatalogDTO catalog) {
        this.catalog = catalog;
    }

    public String getProductLabel() {
        return productLabel;
    }

    public void setProductLabel(String productLabel) {
        this.productLabel = productLabel;
    }
}
