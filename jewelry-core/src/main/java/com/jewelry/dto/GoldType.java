package com.jewelry.dto;

/**
 * User: karen.mnatsakanyan
 */
public enum GoldType {
    TYPE_585,
    TYPE_750,
    TYPE_999
}
