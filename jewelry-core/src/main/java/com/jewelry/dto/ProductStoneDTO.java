package com.jewelry.dto;

/**
 * Created by Mos on 27.02.14.
 */
public class ProductStoneDTO extends BaseDTO {

    private Integer productId;

    private Float diameter;

    private Integer count;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }


    public Float getDiameter() {
        return diameter;
    }

    public void setDiameter(Float diameter) {
        this.diameter = diameter;
    }
}
