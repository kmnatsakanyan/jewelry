package com.jewelry.dto;


/**
 * Created by aramayis.antonyan on 5/10/14.
 */

public class DiamondWeightDTO extends BaseDTO{


    private Float diameter;

    private Float max;

    private Float min;


    public Float getDiameter() {
        return diameter;
    }

    public void setDiameter(Float diameter) {
        this.diameter = diameter;
    }

    public Float getMax() {
        return max;
    }

    public void setMax(Float max) {
        this.max = max;
    }

    public Float getMin() {
        return min;
    }

    public void setMin(Float min) {
        this.min = min;
    }
}
