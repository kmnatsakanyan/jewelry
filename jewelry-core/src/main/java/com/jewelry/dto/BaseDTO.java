package com.jewelry.dto;


import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * User: karen.mnatsakanyan
 */
public class BaseDTO implements Serializable {

    private Integer id;

    private DateTime updateDate;

    private boolean isDeleted;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

}
