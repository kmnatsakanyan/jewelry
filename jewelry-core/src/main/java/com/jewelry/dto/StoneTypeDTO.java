package com.jewelry.dto;

/**
 * Created by aramayis.antonyan on 2/28/14.
 */
public class StoneTypeDTO extends BaseDTO {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
