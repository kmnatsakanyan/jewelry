package com.jewelry.dto;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
public class StoneDiameterDTO extends BaseDTO {

    private Float start;

    private Float end;

    private Integer stoneId;


    public Float getStart() {
        return start;
    }

    public void setStart(Float start) {
        this.start = start;
    }

    public Float getEnd() {
        return end;
    }

    public void setEnd(Float end) {
        this.end = end;
    }

    public Integer getStoneId() {
        return stoneId;
    }

    public void setStoneId(Integer stoneId) {
        this.stoneId = stoneId;
    }
}
