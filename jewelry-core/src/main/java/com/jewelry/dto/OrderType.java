package com.jewelry.dto;

/**
 * User: aramayis.antonyan
 */
public enum OrderType {
   WHOLESALE,
   RETAIL,
   SHOP,
}
