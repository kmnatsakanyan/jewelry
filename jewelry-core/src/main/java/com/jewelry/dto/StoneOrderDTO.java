package com.jewelry.dto;

/**
 * Created by Mos on 04.03.14.
 */
public class StoneOrderDTO extends BaseDTO{
    private Integer orderId;

   /* private StoneTypeDTO type;*/

    private Integer qualityId;

    private Float diameter;

    private Integer count;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

   /* public StoneTypeDTO getType() {
        return type;
    }*/

    /*public void setType(StoneTypeDTO type) {
        this.type = type;
    }*/

   /* public StoneQualityDTO getQuality() {
        return quality;
    }

    public void setQuality(StoneQualityDTO quality) {
        this.quality = quality;
    }*/

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Float getDiameter() {
        return diameter;
    }

    public void setDiameter(Float diameter) {
        this.diameter = diameter;
    }

    public Integer getQualityId() {
        return qualityId;
    }

    public void setQualityId(Integer qualityId) {
        this.qualityId = qualityId;
    }
}
