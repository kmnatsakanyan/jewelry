package com.jewelry.dto;

/**
 * Created by aramayis.antonyan on 2/28/14.
 */
public class StonePriceDTO extends BaseDTO {

    private Integer stoneID;

    private Integer diameterID;

    private Integer qualityID;

    private Float cost;

    public Integer getStoneID() {
        return stoneID;
    }

    public void setStoneID(Integer stoneID) {
        this.stoneID = stoneID;
    }

    public Integer getDiameterID() {
        return diameterID;
    }

    public void setDiameterID(Integer diameterID) {
        this.diameterID = diameterID;
    }

    public Integer getQualityID() {
        return qualityID;
    }

    public void setQualityID(Integer qualityID) {
        this.qualityID = qualityID;
    }

    public Float getCost() {
        return cost;
    }

    public void setCost(Float cost) {
        this.cost = cost;
    }
}
