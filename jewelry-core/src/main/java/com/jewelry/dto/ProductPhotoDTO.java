package com.jewelry.dto;

/**
 * User: karen.mnatsakanyan
 */
public class ProductPhotoDTO extends BaseDTO {

    private Integer productId;

    private String photoFile;

    private String photoThumbnailFile;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getPhotoFile() {
        return photoFile;
    }

    public void setPhotoFile(String photoFile) {
        this.photoFile = photoFile;
    }

    public String getPhotoThumbnailFile() {
        return photoThumbnailFile;
    }

    public void setPhotoThumbnailFile(String photoThumbnailFile) {
        this.photoThumbnailFile = photoThumbnailFile;
    }
}
