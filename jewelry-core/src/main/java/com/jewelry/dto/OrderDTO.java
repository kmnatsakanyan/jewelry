package com.jewelry.dto;

import org.joda.time.DateTime;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class OrderDTO extends BaseDTO {

    private ProductDTO product;

    private Integer count;

    private Integer done;

    private DateTime date;

    private UserDTO user;

    private List<StoneOrderDTO> stones;

    private String description;

    private GoldType goldType;

    private OrderType orderType;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public List<StoneOrderDTO> getStones() {
        return stones;
    }

    public void setStones(List<StoneOrderDTO> stones) {
        this.stones = stones;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GoldType getGoldType() {
        return goldType;
    }

    public void setGoldType(GoldType goldType) {
        this.goldType = goldType;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public Integer getDone() {
        return done;
    }

    public void setDone(Integer done) {
        this.done = done;
    }
}
