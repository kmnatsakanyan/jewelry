package com.jewelry.dto;

/**
 * Created by aramayis.antonyan on 2/26/14.
 */
public class OrderTypePriceDTO extends BaseDTO {

    private Double cost;

    private OrderType type;

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public OrderType getType() {
        return type;
    }

    public void setType(OrderType type) {
        this.type = type;
    }
}
