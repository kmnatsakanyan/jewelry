package com.jewelry.dto;

import java.io.Serializable;

/**
 * User: karen.mnatsakanyan
 */
public class PhotoDTO implements Serializable {

    private Integer productId;
    private byte[] photo;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }
}
