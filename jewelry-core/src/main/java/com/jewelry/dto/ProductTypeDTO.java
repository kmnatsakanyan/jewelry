package com.jewelry.dto;

/**
 * Created by Mos on 27.02.14.
 */
public class ProductTypeDTO extends BaseDTO {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
