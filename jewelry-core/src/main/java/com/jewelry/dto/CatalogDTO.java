package com.jewelry.dto;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class CatalogDTO extends BaseDTO {

    private String name;

    private List<ProductCatalogDTO> productCatalogs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductCatalogDTO> getProductCatalogs() {
        return productCatalogs;
    }

    public void setProductCatalogs(List<ProductCatalogDTO> productCatalogs) {
        this.productCatalogs = productCatalogs;
    }
}
