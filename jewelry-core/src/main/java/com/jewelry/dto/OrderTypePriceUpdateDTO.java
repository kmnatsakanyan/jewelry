package com.jewelry.dto;

/**
 * User: karen.mnatsakanyan
 */
public class OrderTypePriceUpdateDTO {

    OrderType type ;
    Double price;

    public OrderType getType() {
        return type;
    }

    public void setType(OrderType type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
