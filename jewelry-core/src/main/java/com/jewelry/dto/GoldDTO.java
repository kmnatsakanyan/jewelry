package com.jewelry.dto;

/**
 * Created by aramayis.antonyan on 2/26/14.
 */
public class GoldDTO extends BaseDTO {

    private Double cost;

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }
}
