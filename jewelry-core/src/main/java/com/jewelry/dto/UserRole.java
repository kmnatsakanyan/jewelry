package com.jewelry.dto;

/**
 * User: karen.mnatsakanyan
 */
public enum UserRole {
    ROLE_ADMIN,
    ROLE_SELLER,
    ROLE_SHOP,
    ROLE_CANDY
}
