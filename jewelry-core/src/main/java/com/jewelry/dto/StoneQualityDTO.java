package com.jewelry.dto;

/**
 * Created by aramayis.antonyan on 2/28/14.
 */
public class StoneQualityDTO extends BaseDTO {

    private String quality;

    private Integer stoneId;


    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public Integer getStoneId() {
        return stoneId;
    }

    public void setStoneId(Integer stoneId) {
        this.stoneId = stoneId;
    }
}
