package com.jewelry.dto;

/**
 * User: karen.mnatsakanyan
 */
public class ValueDTO<T> {

    private T value;

    private int resultCode;

    public ValueDTO() {

    }

    public ValueDTO(T value) {
        this.value = value;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
