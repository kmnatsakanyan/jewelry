package com.jewelry.exception;

/**
 * User: karen.mnatsakanyan
 */
public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException(String message) {
        super(message);
    }
}
