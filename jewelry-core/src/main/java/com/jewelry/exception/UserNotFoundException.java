package com.jewelry.exception;

/**
 * User: karen.mnatsakanyan
 */
public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String message) {
        super(message);
    }
}