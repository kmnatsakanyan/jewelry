package com.jewelry.exception;

/**
 * User: karen.mnatsakanyan
 */
public class DiameterNotFoundException extends RuntimeException {

    public DiameterNotFoundException(String message) {
        super(message);
    }
}