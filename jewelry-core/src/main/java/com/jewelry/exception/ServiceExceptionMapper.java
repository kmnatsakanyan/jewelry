package com.jewelry.exception;

import com.jewelry.dto.ValueDTO;
import com.jewelry.util.JewelryConstant;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * User: karen.mnatsakanyan
 */
@Provider
public class ServiceExceptionMapper implements ExceptionMapper<Exception> {
    @Override
    public Response toResponse(Exception e) {
        ValueDTO response = new ValueDTO();
        if (e instanceof UnauthorizedException) {
            response.setResultCode(JewelryConstant.RESPONSE_UNAUTHORIZED);
        } else if (e instanceof UserNotFoundException) {
            response.setResultCode(JewelryConstant.RESPONSE_CATALOG_NOT_FOUND);
        } else if (e instanceof UserExistException) {
            response.setResultCode(JewelryConstant.RESPONSE_USER_EXIST);
        } else {
            response.setResultCode(JewelryConstant.RESPONSE_ERROR);
        }
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(response).build();
    }
}
