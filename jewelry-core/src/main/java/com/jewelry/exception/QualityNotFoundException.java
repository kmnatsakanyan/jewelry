package com.jewelry.exception;

/**
 * User: karen.mnatsakanyan
 */
public class QualityNotFoundException extends RuntimeException {

    public QualityNotFoundException(String message) {
        super(message);
    }
}