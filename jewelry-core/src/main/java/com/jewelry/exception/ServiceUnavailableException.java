package com.jewelry.exception;

/**
 * User: karen.mnatsakanyan
 */
public class ServiceUnavailableException extends RuntimeException {
    private static final long serialVersionUID = 5162710183389028792L;

    private int statusCode;

    public ServiceUnavailableException(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }
}
