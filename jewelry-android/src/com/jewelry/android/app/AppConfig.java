package com.jewelry.android.app;

import android.content.SharedPreferences;

import java.util.Map;
import java.util.Properties;
import java.util.Set;

import static com.jewelry.android.util.JewelryMobileConstant.PHOTO_URL;

/**
 * Class serves config values coming from both shared config and properties file.
 *
 * @author hma
 *         Date: 2/18/13
 */
public final class AppConfig implements SharedPreferences {
    private SharedPreferences prefs;
    private Properties props;
    private static AppConfig INSTANCE;

    private static final String KEY_SERVICES_URL = "services.url";
    private static final String KEY_SERVICES_URL_DEFAULT_VALUE = "Service Url";

    private AppConfig(SharedPreferences prefs, Properties props) {
        if (prefs == null) {
            throw new RuntimeException("Initial SharedPreferences config cannot be null");
        }
        this.prefs = prefs;
        this.props = props;
    }

    public static AppConfig getInstance() {
        if (INSTANCE == null) {
            throw new IllegalStateException("Configuration is not initialized");
        }
        return INSTANCE;
    }

    public static void init(SharedPreferences prefs, Properties props) {
        if (INSTANCE != null) {
            throw new IllegalStateException("Reconfiguration is not allowed");
        }
        INSTANCE = new AppConfig(prefs, props);
    }

    @Override
    public Map<String, ?> getAll() {
        return prefs.getAll();
    }

    @Override
    public String getString(String s, String s2) {
        return prefs.getString(s, s2);
    }

    @Override
    public Set<String> getStringSet(String key, Set<String> defValues) {
        return null;
    }

    @Override
    public int getInt(String s, int i) {
        return prefs.getInt(s, i);
    }

    @Override
    public long getLong(String s, long l) {
        return prefs.getLong(s, l);
    }

    @Override
    public float getFloat(String s, float v) {
        return prefs.getFloat(s, v);
    }

    @Override
    public boolean getBoolean(String s, boolean b) {
        return prefs.getBoolean(s, b);
    }

    @Override
    public boolean contains(String s) {
        return contains(s);
    }

    @Override
    public Editor edit() {
        return prefs.edit();
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        prefs.registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        prefs.unregisterOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    public String getServicesUrl() {
        return props.getProperty(KEY_SERVICES_URL);
    }

    public String getPhotoServiceUrl() {
        return getServicesUrl() + PHOTO_URL ;
    }

}
