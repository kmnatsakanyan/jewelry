package com.jewelry.android.app;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.util.Log;
import com.jewelry.android.client.Client;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.util.IOUtils;
import com.jewelry.android.util.JewelryMobileConstant;
import com.jewelry.dto.UserDTO;
import com.jewelry.dto.UserRole;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;

/**
 * Class represents Android application and initializes it's config test
 *
 * @author hma  Date: 2/18/13
 */
public class JewelryApplication extends Application {
    private AppConfig cfg;
    private Client client;
    private UserDTO loggedInUser;
    private Context context;

    private static JewelryApplication INSTANCE;
    private SharedPreferences prefs;
    public static ProductDAOHelper DAO;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        loadLoggedInUserIfExists();
        InputStream inputStream = null;
        try {
            context = getApplicationContext();
            //BitmapCache.initialize(getApplicationContext());
            prefs = context.getSharedPreferences(JewelryMobileConstant.PREFS_CONFIG_FILE_NANE, MODE_WORLD_WRITEABLE);
            AssetManager assetManager = context.getAssets();
            inputStream = assetManager.open(JewelryMobileConstant.PEROPS_CONFIG_FILE_NAME);
            Properties properties = new Properties();
            properties.load(inputStream);
            AppConfig.init(prefs, properties);
            cfg = AppConfig.getInstance();
            client = Client.configure(cfg, loggedInUser);

            Locale locale = new Locale("am");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            JewelryApplication.instance().getResources().updateConfiguration(config, null);
            DAO = new ProductDAOHelper(this);
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void loadLoggedInUserIfExists() {
        try {
            loggedInUser = (UserDTO) IOUtils.getObjectFromPreferences(getApplicationContext(), JewelryMobileConstant.KEY_PREF_LOGGED_IN_USER);
        } catch (Throwable ex) {
            Log.e("JewelryApplication", ex.getMessage());
        }
    }


    public static JewelryApplication instance() {
        return INSTANCE;
    }

    public Context getAppContext() {
        return INSTANCE.context;
    }

    public AppConfig getAppConfig() {
        return INSTANCE.cfg;
    }

    public Client getClient() {
        return INSTANCE.client;
    }


    public UserDTO getLoggedInUser() {
        return INSTANCE.loggedInUser;
    }

    public boolean isLoggedInUserAdmin() {
        boolean result = false;
        if (INSTANCE.loggedInUser != null && INSTANCE.loggedInUser.getRole() == UserRole.ROLE_ADMIN)
            result = true;
        return result;
    }

    public void setLoggedInUser(UserDTO loggedInUser) {
        INSTANCE.loggedInUser = loggedInUser;
    }

    public Double getGoldCost() {
        Double cost = (double) 0;
        try {
            cost = (Double) IOUtils.getObjectFromPreferences(getApplicationContext(), JewelryMobileConstant.KEY_GOLD_COST);
        } catch (Throwable tr) {
            Log.e("JewelryApplication", "getGoldCost");
        }
        return cost;
    }

    public void setGoldCost(Double cost) {
        IOUtils.storeObjectToPreferences(JewelryApplication.instance(), JewelryMobileConstant.KEY_GOLD_COST, cost);
    }


    public void setWholesale(Double price) {
        IOUtils.storeObjectToPreferences(JewelryApplication.instance(), JewelryMobileConstant.KEY_WHOLESALE, price);
    }


    public void setRetail(Double price) {
        IOUtils.storeObjectToPreferences(JewelryApplication.instance(), JewelryMobileConstant.KEY_RETAIL, price);
    }

    public Double getWholesale() {
        Double price = (double) 0;
        try {
            price = (Double) IOUtils.getObjectFromPreferences(getApplicationContext(), JewelryMobileConstant.KEY_WHOLESALE);
        } catch (Throwable tr) {
            Log.e("JewelryApplication", "getWholesale");
        }
        return price;
    }

    public Double getRetail() {
        Double price = (double) 0;
        try {
            price = (Double) IOUtils.getObjectFromPreferences(getApplicationContext(), JewelryMobileConstant.KEY_RETAIL);
        } catch (Throwable tr) {
            Log.e("JewelryApplication", "getRetail");
        }
        return price;
    }

    public void setShop(Double price) {
        IOUtils.storeObjectToPreferences(JewelryApplication.instance(), JewelryMobileConstant.KEY_SHOP, price);
    }

    public Double getShop() {
        Double price = (double) 0;
        try {
            price = (Double) IOUtils.getObjectFromPreferences(getApplicationContext(), JewelryMobileConstant.KEY_SHOP);
        } catch (Throwable tr) {
            Log.e("JewelryApplication", "getShop");
        }
        return price;
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public SharedPreferences getPrefs() {
        return prefs;
    }
}
