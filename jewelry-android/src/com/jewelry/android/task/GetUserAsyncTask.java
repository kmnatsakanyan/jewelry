package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.UserDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;

/**
 * User: aramayis.antonyan
 */
public class GetUserAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<UserDTO>> {
    private static final String TAG = "GetUsersAsyncTask";
    private Integer userId;

    public GetUserAsyncTask(AsyncTaskHandler<ValueDTO<UserDTO>> handler,Integer id) {
        super(handler);
        userId = id;
    }

    @Override
    protected ValueDTO<UserDTO> doInBackground(Void... strings) {
        ValueDTO<UserDTO> user;
        try {
            user = userResource.getUser(userId);
        } catch (RuntimeException ex) {
            user = new ValueDTO<UserDTO>();
            user.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in login task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
                user.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return user;
    }
}
