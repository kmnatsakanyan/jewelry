package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.StonePriceDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;


import java.util.List;

/**
 * User: mos
 */
public class AddStonePriceAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<List<StonePriceDTO>>> {
    private static final String TAG = "AddQualityForStoneAsyncTask";
	private List<StonePriceDTO> mPriceList;

    public AddStonePriceAsyncTask(AsyncTaskHandler<ValueDTO<List<StonePriceDTO>>> handler, List<StonePriceDTO> priceList) {
        super(handler);
        mPriceList = priceList;
    }

    @Override
    protected ValueDTO<List<StonePriceDTO>> doInBackground(Void... strings) {
	    ValueDTO<List<StonePriceDTO>> result;
        try {
	        result = stonePriceResource.addPrice(mPriceList);
        } catch (RuntimeException ex) {
	        result = new ValueDTO<List<StonePriceDTO>>();
	        result.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in add diameter task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
	            result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return result;
    }
}
