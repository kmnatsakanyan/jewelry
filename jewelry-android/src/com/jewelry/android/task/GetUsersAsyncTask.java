package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.dto.UserDTO;
import com.jewelry.dto.UserRole;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class GetUsersAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<List<UserDTO>>> {
    private static final String TAG = "GetUsersAsyncTask";

    public GetUsersAsyncTask(AsyncTaskHandler<ValueDTO<List<UserDTO>>> handler) {
        super(handler);
    }

    @Override
    protected ValueDTO<List<UserDTO>> doInBackground(Void... strings) {
        ValueDTO<List<UserDTO>> userList;
        try {
            if (JewelryApplication.instance().getLoggedInUser().getRole() != UserRole.ROLE_ADMIN) {
                userList = new ValueDTO<List<UserDTO>>();
                List<UserDTO> userDTOs = new ArrayList<UserDTO>(1);
                UserDTO userDTO = userResource.getUser(JewelryApplication.instance().getLoggedInUser().getId()).getValue();
                userDTOs.add(userDTO);
                userList.setValue(userDTOs);
            } else {
                userList = userResource.getAll();
            }
        } catch (RuntimeException ex) {
            userList = new ValueDTO<List<UserDTO>>();
            userList.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in login task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
                userList.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return userList;
    }
}
