package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;

/**
 * User: karen.mnatsakanyan
 */
public class DeleteQualityAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<Boolean>> {
    private static final String TAG = "DeleteUserAsyncTask";
    private Integer qualityId;

    public DeleteQualityAsyncTask(AsyncTaskHandler<ValueDTO<Boolean>> handler, Integer qualityId) {
        super(handler);
        this.qualityId = qualityId;
    }

    @Override
    protected ValueDTO<Boolean> doInBackground(Void... params) {
        ValueDTO<Boolean> result;
        try {
            result = qualityResource.delete(qualityId);

        } catch (RuntimeException ex) {
            result = new ValueDTO<Boolean>();
            result.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in login task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
                result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return result;
    }
}
