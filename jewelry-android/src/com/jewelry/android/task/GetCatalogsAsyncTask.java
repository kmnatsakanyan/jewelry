package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.CatalogDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class GetCatalogsAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<List<CatalogDTO>>> {
    private static final String TAG = "GetUsersAsyncTask";

    public GetCatalogsAsyncTask(AsyncTaskHandler<ValueDTO<List<CatalogDTO>>> handler) {
        super(handler);
    }

    @Override
    protected ValueDTO<List<CatalogDTO>> doInBackground(Void... strings) {
        ValueDTO<List<CatalogDTO>> catalogs;
        try {
            catalogs = catalogResource.getAll();
        } catch (RuntimeException ex) {
            catalogs = new ValueDTO<List<CatalogDTO>>();
            catalogs.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in login task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
                catalogs.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return catalogs;
    }
}
