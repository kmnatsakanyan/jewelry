package com.jewelry.android.task;

import android.support.v4.app.FragmentActivity;
import android.util.Log;
import com.jewelry.android.bitmap.util.ImageCache;
import com.jewelry.android.bitmap.util.ImageFetcher;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.util.JewelryMobileConstant;
import com.jewelry.dto.*;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;
import org.joda.time.DateTime;

import java.util.List;

public class ProductAsyncTask extends AbstractAsyncTask<Void, Integer, ValueDTO<List<ProductDTO>>> {
    private static final String TAG = "ProductAsyncTask";
    private ImageFetcher mImageFetcherThumb;
    private ImageFetcher mImageFetcherImg;
    private static final String IMAGE_CACHE_DIR_THUMB = "thumbs";
    private static final String IMAGE_CACHE_DIR = "images";
    private AsyncTaskProgressHandler mAsyncTaskProgressHandler;

    public ProductAsyncTask(AsyncTaskHandler<ValueDTO<List<ProductDTO>>> handler, FragmentActivity activity, AsyncTaskProgressHandler asyncTaskProgressHandler) {
        super(handler);
        mAsyncTaskProgressHandler = asyncTaskProgressHandler;
        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(context, IMAGE_CACHE_DIR_THUMB);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        mImageFetcherThumb = new ImageFetcher(context, 0);
        mImageFetcherThumb.addImageCache(activity.getSupportFragmentManager(), cacheParams);
        mImageFetcherThumb.setImageFadeIn(false);
        mImageFetcherThumb.initDiskCacheInternal();


        cacheParams =
                new ImageCache.ImageCacheParams(context, IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        mImageFetcherImg = new ImageFetcher(context, 0);
        mImageFetcherImg.setImageFadeIn(false);
        mImageFetcherImg.addImageCache(activity.getSupportFragmentManager(), cacheParams);
        mImageFetcherImg.initDiskCacheInternal();

    }

    @Override
    protected ValueDTO<List<ProductDTO>> doInBackground(Void... strings) {
        ValueDTO<List<ProductDTO>> productList;
        ValueDTO<List<ProductTypeDTO>> typeList;
        ValueDTO<List<StonePriceDTO>> priceList;
        ValueDTO<List<StoneDiameterDTO>> diameterList;
        ValueDTO<List<StoneQualityDTO>> qualityList;
        ValueDTO<List<DiamondWeightDTO>> diamondWeight;
        ValueDTO<List<CatalogDTO>> catalogs;
        ValueDTO<List<ProductCatalogDTO>> productCatalogs;

        ProductDAOHelper productDAO = JewelryApplication.DAO;


        try {

            ValueDTO<Double> cost = goldResource.getCost();
            if (cost.getValue() != null) {
                JewelryApplication.instance().setGoldCost(cost.getValue());
            }

            ValueDTO<List<OrderTypePriceDTO>> orderTypePrices = orderTypePriceResource.getUpdates();
            for (OrderTypePriceDTO orderTypePriceDTO : orderTypePrices.getValue()) {
                if (orderTypePriceDTO.getType().equals(OrderType.RETAIL)) {
                    JewelryApplication.instance().setRetail(orderTypePriceDTO.getCost());
                } else if (orderTypePriceDTO.getType().equals(OrderType.WHOLESALE)) {
                    JewelryApplication.instance().setWholesale(orderTypePriceDTO.getCost());
                } else if (orderTypePriceDTO.getType().equals(OrderType.SHOP)) {
                    JewelryApplication.instance().setShop(orderTypePriceDTO.getCost());
                }
            }

            DateTime dt = productDAO.getProductLastUpdateDate();
            productList = productResource.getUpdates(dt);
            if (productList.getValue() != null && productList.getValue().size() > 0) {
                //productDAO.saveProductList(productList.getValue());
                List<ProductDTO> value = productList.getValue();
                for (int i = 0; i < value.size(); i++) {
                    try {
                        ProductDTO productDTO = value.get(i);
                        for (ProductPhotoDTO productPhotoDTO : productDTO.getProductPhotos()) {
                            Log.e(TAG, "Downloading image:" + productPhotoDTO.getPhotoFile());
                            mImageFetcherImg.processBitmap(productPhotoDTO.getPhotoFile());
                            mImageFetcherThumb.processBitmap(productPhotoDTO.getPhotoThumbnailFile());
                        }
                        productDAO.saveProduct(productDTO);
                        if (mAsyncTaskProgressHandler != null)
                            publishProgress((int) ((i / (float) value.size()) * 100));
                    } catch (Exception ex) {
                        Log.d(TAG, "Download image Finish");
                    }
                }
            }
            Log.d(TAG, "Download image Finish");


            dt = productDAO.getDiamondWeightLastUpdateDate();
            diamondWeight = diamondWeightResource.getUpdates(dt);
            if (diamondWeight.getValue() != null && diamondWeight.getValue().size() > 0) {
                productDAO.saveDiamondWeightList(diamondWeight.getValue());
            }

            dt = productDAO.getProductTypeLastUpdateDate();
            typeList = typeResource.getUpdates(dt);
            if (typeList.getValue() != null && typeList.getValue().size() > 0) {
                productDAO.saveTypeList(typeList.getValue());
            }

            dt = productDAO.getStonePriceLastUpdateDate();
            priceList = stonePriceResource.getUpdates(dt);
            if (priceList.getValue() != null && priceList.getValue().size() > 0) {
                productDAO.savePriceList(priceList.getValue());
            }

            dt = productDAO.getStoneDiameterLastUpdateDate();
            diameterList = stoneDiameterResource.getUpdates(dt);
            if (diameterList.getValue() != null && diameterList.getValue().size() > 0) {
                productDAO.saveDiameterList(diameterList.getValue());
            }

            dt = productDAO.getStoneQualityLastUpdateDate();
            qualityList = qualityResource.getUpdates(dt);
            if (qualityList.getValue() != null && qualityList.getValue().size() > 0) {
                productDAO.saveQualityList(qualityList.getValue());
            }

            dt = productDAO.getCatalogsLastUpdateDate();
            catalogs = catalogResource.getUpdates(dt);
            if (catalogs.getValue() != null && catalogs.getValue().size() > 0) {
                productDAO.saveCatalogs(catalogs.getValue());
            }

            dt = productDAO.getProductCatalogsLastUpdateDate();
            productCatalogs = productCatalogResource.getUpdates(dt);
            if (productCatalogs.getValue() != null && productCatalogs.getValue().size() > 0) {
                productDAO.saveProductCatalogs(productCatalogs.getValue());
            }

            JewelryApplication.instance().getPrefs().edit().putLong(JewelryMobileConstant.PREFS_LAST_UPDATE_TIMESTAMP, System.currentTimeMillis()).apply();
        } catch (RuntimeException ex) {
            productList = new ValueDTO<List<ProductDTO>>();
            productList.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in login task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
                productList.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return productList;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (mAsyncTaskProgressHandler != null)
            mAsyncTaskProgressHandler.onProgressUpdate(values);
    }
}
