package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;

/**
 * User: mos
 */
public class UpdateGoldAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<Boolean>> {
    private static final String TAG = "UpdateGoldAsyncTask";
	private Double mGold;

    public UpdateGoldAsyncTask(AsyncTaskHandler<ValueDTO<Boolean>> handler,Double gold) {
        super(handler);
	    mGold = gold;
    }

    @Override
    protected ValueDTO<Boolean> doInBackground(Void... strings) {
	    ValueDTO<Boolean> result;
        try {
	        result = goldResource.updateCost(mGold);
        } catch (RuntimeException ex) {
	        result = new ValueDTO<Boolean>();
	        result.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in update cost task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
	            result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return result;
    }
}
