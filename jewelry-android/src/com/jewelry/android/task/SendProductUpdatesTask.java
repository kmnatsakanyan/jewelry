package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.domain.ProductTemp;
import com.jewelry.android.util.BitmapUtils;
import com.jewelry.dto.PhotoDTO;
import com.jewelry.dto.ProductDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class SendProductUpdatesTask extends AbstractAsyncTask<Void, Void, ValueDTO<Boolean>> {
    private static final String TAG = "SendProductUpdatesTask";
    private List<ProductTemp> mTemps;
    private ProductDAOHelper dao = JewelryApplication.DAO;

    public SendProductUpdatesTask(AsyncTaskHandler<ValueDTO<Boolean>> handler, List<ProductTemp> temps) {
        super(handler);
        mTemps = temps;
    }

    @Override
    protected ValueDTO<Boolean> doInBackground(Void... strings) {
        ValueDTO<Boolean> result = new ValueDTO<Boolean>(true);
        try {
            for (ProductTemp temp : mTemps) {
                ProductDTO productDTO;
                if (temp.getProductId() != null) {
                    productDTO = dao.getProduct(temp.getProductId());
                } else {
                    productDTO = new ProductDTO();
                }
                productDTO.setId(temp.getProductId());
                productDTO.setWeight(temp.getWeight());
                productDTO.setProductId(temp.getProductName());
                productDTO.setType(temp.getType());
                productDTO.setProductStones(temp.getProductStones());
                productDTO.setDeleted(temp.getIsDeleted());
                productDTO.setProductCatalogs(temp.getProductCatalogs());
                if (temp.getProductId() != null) {
                    productDTO = productResource.updateProduct(productDTO).getValue();
                } else {
                    productDTO = productResource.saveProduct(productDTO).getValue();
                }
                if (temp.getProductPhotoKey() != null) {
                    List<String> files = BitmapUtils.getProductPhotosIfExists(temp.getProductPhotoKey());
                    for (String fileStr : files) {
                        byte[] fileBytes = BitmapUtils.getBytesFormFile(fileStr);
                        PhotoDTO photoDTO = new PhotoDTO();
                        photoDTO.setPhoto(fileBytes);
                        photoDTO.setProductId(productDTO.getId());
                        Boolean updResult = photoResource.save(photoDTO).getValue();
                    }
                }
                if (temp.getProductPhotoKey() != null) {
                    BitmapUtils.deleteProductPhotosIfExists(temp.getProductPhotoKey());
                }
                dao.deleteTempProduct(temp.getId());
            }
        } catch (RuntimeException ex) {
            result = new ValueDTO<Boolean>();
            result.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in updating products" + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
                result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return result;
    }
}