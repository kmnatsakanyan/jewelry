package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;

/**
 * User: karen.mnatsakanyan
 */
public class DeleteUserAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<Boolean>> {
    private static final String TAG = "DeleteQualityAsyncTask";
    private Integer mUserId;

    public DeleteUserAsyncTask(AsyncTaskHandler<ValueDTO<Boolean>> handler, Integer userId) {
        super(handler);
        mUserId = userId;
    }

    @Override
    protected ValueDTO<Boolean> doInBackground(Void... params) {
        ValueDTO<Boolean> result;
        try {
            result = userResource.delete(mUserId);
        } catch (RuntimeException ex) {
            result = new ValueDTO<Boolean>();
            result.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in login task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
                result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return result;
    }
}
