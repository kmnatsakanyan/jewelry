package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.dto.OrderDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;
import org.joda.time.DateTime;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class GetOrdersAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<List<OrderDTO>>> {
    private static final String TAG = "GetOrdersAsyncTask";

    public GetOrdersAsyncTask(AsyncTaskHandler<ValueDTO<List<OrderDTO>>> handler) {
        super(handler);
    }

    @Override
    protected ValueDTO<List<OrderDTO>> doInBackground(Void... strings) {
        ValueDTO<List<OrderDTO>> orders;
        try {
            ProductDAOHelper productDAO = JewelryApplication.DAO;
            DateTime dt = productDAO.getOrdersLastUpdateDate();
            orders = orderResource.getUpdates(dt,JewelryApplication.instance().getLoggedInUser().getId());
            productDAO.saveOrderList(orders.getValue());
        } catch (RuntimeException ex) {
            orders = new ValueDTO<List<OrderDTO>>();
            orders.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in login task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
                orders.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return orders;
    }
}
