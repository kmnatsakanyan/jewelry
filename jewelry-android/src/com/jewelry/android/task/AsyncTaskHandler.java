package com.jewelry.android.task;

import com.jewelry.dto.ValueDTO;

/**
 * Base class for all task handlers
 * User: hma
 * Date: 3/2/13
 */
public interface AsyncTaskHandler<T extends ValueDTO> {
    void handle(T result);

    void handleError(T result);
}
