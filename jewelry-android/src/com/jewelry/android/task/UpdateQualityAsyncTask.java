package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.StoneQualityDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;
import org.joda.time.DateTime;

/**
 * User: mos
 */
public class UpdateQualityAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<StoneQualityDTO>> {
    private static final String TAG = "AddQualityAsyncTask";
	private StoneQualityDTO mQualityDTO;

    public UpdateQualityAsyncTask(AsyncTaskHandler<ValueDTO<StoneQualityDTO>> handler, StoneQualityDTO quality) {
        super(handler);
        mQualityDTO = quality;
        mQualityDTO.setDeleted(false);
        mQualityDTO.setUpdateDate(DateTime.now());
    }

    @Override
    protected ValueDTO<StoneQualityDTO> doInBackground(Void... strings) {
	    ValueDTO<StoneQualityDTO> result;
        try {
	        result = qualityResource.saveQuality(mQualityDTO);
        } catch (RuntimeException ex) {
	        result = new ValueDTO<StoneQualityDTO>();
	        result.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in add diameter task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
	            result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return result;
    }
}
