package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.OrderType;
import com.jewelry.dto.OrderTypePriceUpdateDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;

/**
 * User: mos
 */
public class UpdateOrderTypePriceAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<Boolean>> {
    private static final String TAG = "OrderTypePriceAsyncTask";
	private Double price;
    private OrderType type;

    public UpdateOrderTypePriceAsyncTask(AsyncTaskHandler<ValueDTO<Boolean>> handler, OrderType type, Double price) {
        super(handler);
        this.price = price;
        this.type = type;
    }

    @Override
    protected ValueDTO<Boolean> doInBackground(Void... strings) {
	    ValueDTO<Boolean> result;
        try {
            OrderTypePriceUpdateDTO orderTypePriceUpdateDTO = new OrderTypePriceUpdateDTO();
            orderTypePriceUpdateDTO.setType(type);
            orderTypePriceUpdateDTO.setPrice(price);
	        result = orderTypePriceResource.updateCost(orderTypePriceUpdateDTO);
        } catch (RuntimeException ex) {
	        result = new ValueDTO<Boolean>();
	        result.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in update cost task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
	            result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return result;
    }
}
