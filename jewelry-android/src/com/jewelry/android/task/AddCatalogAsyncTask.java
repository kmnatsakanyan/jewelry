package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.CatalogDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;

/**
 * User: karen.mnatsakanyan
 */
public class AddCatalogAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<CatalogDTO>>{

    private static final String TAG = "AddUserAsyncTask";
    private CatalogDTO mUserDTO;

    public AddCatalogAsyncTask(AsyncTaskHandler<ValueDTO<CatalogDTO>> handler, CatalogDTO userDTO) {
        super(handler);
        mUserDTO = userDTO;
    }

    @Override
    protected ValueDTO<CatalogDTO> doInBackground(Void... params) {
        ValueDTO<CatalogDTO> result;
        try {
            result = catalogResource.addCatalog(mUserDTO);
        } catch (RuntimeException ex) {
            result = new ValueDTO<CatalogDTO>();
            result.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in login task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
                result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return result;
    }
}
