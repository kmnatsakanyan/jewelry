package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.OrderDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;

import java.util.List;

public class UpdateOrderAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<List<OrderDTO>>> {
    private static final String TAG = "OrderAsyncTask";
    private List<OrderDTO> orders;

    public UpdateOrderAsyncTask(AsyncTaskHandler<ValueDTO<List<OrderDTO>>> handler, List<OrderDTO> orders) {
        super(handler);
        this.orders = orders;
    }

    @Override
    protected ValueDTO<List<OrderDTO>> doInBackground(Void... strings) {
        ValueDTO<List<OrderDTO>> result;
        try {
                result = orderResource.updateOrders(orders);
            } catch (RuntimeException ex) {
                result = new ValueDTO<List<OrderDTO>>();
                result.setResultCode(JewelryConstant.RESPONSE_ERROR);
                Log.e(TAG, "Exception in update orders task " + ex.getMessage());
                if (ex.getCause() instanceof ServiceUnavailableException) {
                    result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
                }
            }
        return result;
    }

}
