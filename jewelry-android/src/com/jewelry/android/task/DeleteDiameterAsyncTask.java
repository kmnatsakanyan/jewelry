package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;

/**
 * User: aramayis.antonyan
 */
public class DeleteDiameterAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<Boolean>> {
    private static final String TAG = "DeleteDiameterAsyncTask";
    private Integer diameterId;

    public DeleteDiameterAsyncTask(AsyncTaskHandler<ValueDTO<Boolean>> handler, Integer diameterId) {
        super(handler);
        this.diameterId = diameterId;
    }

    @Override
    protected ValueDTO<Boolean> doInBackground(Void... params) {
        ValueDTO<Boolean> result;
        try {
            result = stoneDiameterResource.delete(diameterId);

        } catch (RuntimeException ex) {
            result = new ValueDTO<Boolean>();
            result.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in login task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
                result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return result;
    }
}
