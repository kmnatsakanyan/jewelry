package com.jewelry.android.task;

import android.content.Context;
import android.util.Log;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.dto.ValueDTO;
import com.jewelry.rs.*;

/**
 * Base class for all Asynchronous Tasks
 *
 * @author hma
 *         Date: 3/2/13
 */
public abstract class AbstractAsyncTask<PARAMS, PROGRESS, RESULT extends ValueDTO>
        extends ModernAsyncTask<PARAMS, PROGRESS, RESULT> {
    private AsyncTaskHandler<RESULT> handler;

    protected UserResource userResource;
    protected GoldResource goldResource;
    protected OrderResource orderResource;
    protected PhotoResource photoResource;
    protected ProductResource productResource;
    protected StoneDiameterResource stoneDiameterResource;
    protected StonePriceResource stonePriceResource;
    protected TypeResource typeResource;
    protected StoneQualityResource qualityResource;
    protected OrderTypePriceResource orderTypePriceResource;
    protected DiamondWeightResource diamondWeightResource;
    protected CatalogResource catalogResource;
    protected ProductCatalogResource productCatalogResource;
    protected Context context;

    public AbstractAsyncTask(AsyncTaskHandler<RESULT> handler) {
        this.handler = handler;
        userResource = JewelryApplication.instance().getClient().getUserResource();
        goldResource = JewelryApplication.instance().getClient().getGoldResource();
        orderResource = JewelryApplication.instance().getClient().getOrderResource();
        photoResource = JewelryApplication.instance().getClient().getPhotoResource();
        productResource = JewelryApplication.instance().getClient().getProductResource();
        stoneDiameterResource = JewelryApplication.instance().getClient().getStoneDiameterResource();
        stonePriceResource = JewelryApplication.instance().getClient().getStonePriceResource();
        typeResource = JewelryApplication.instance().getClient().getTypeResource();
        qualityResource = JewelryApplication.instance().getClient().getStoneQualityResource();
        orderTypePriceResource = JewelryApplication.instance().getClient().getOrderTypePriceResource();
        diamondWeightResource = JewelryApplication.instance().getClient().getDiamondWeightResource();
        catalogResource = JewelryApplication.instance().getClient().getCatalogResource();
        productCatalogResource = JewelryApplication.instance().getClient().getProductCatalogResource();
        context = JewelryApplication.instance().getAppContext();
    }

    @Override
    protected void onPostExecute(RESULT result) {
        if (responseOk(result)) {
            handler.handle(result);
        } else if (result != null) {
            handler.handleError(result);
        }
        Log.d("ACHTUNG", "BAD RESPONSE");
    }

    private boolean responseOk(RESULT result) {
        return result != null && result.getResultCode() == 0;
    }
}
