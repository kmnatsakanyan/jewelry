package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;

/**
 * User: aramayis.antonyan
 */
public class DeleteOrderAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<Boolean>> {
    private static final String TAG = "DeleteOrderAsyncTask";
    private Integer orderId;

    public DeleteOrderAsyncTask(AsyncTaskHandler<ValueDTO<Boolean>> handler, Integer orderId) {
        super(handler);
        this.orderId = orderId;
    }

    @Override
    protected ValueDTO<Boolean> doInBackground(Void... params) {
        ValueDTO<Boolean> result;
        try {
            result = orderResource.delete(orderId);
        } catch (RuntimeException ex) {
            result = new ValueDTO<Boolean>();
            result.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in login task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
                result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return result;
    }
}
