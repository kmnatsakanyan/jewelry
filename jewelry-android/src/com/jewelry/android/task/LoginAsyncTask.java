package com.jewelry.android.task;


import android.util.Log;
import com.jewelry.dto.UserDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;

public class LoginAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<UserDTO>> {
    private static   String TAG = "LoginAsyncTask";

    private String mEmail;
    private String mPassword;

    public LoginAsyncTask(String email, String password, AsyncTaskHandler<ValueDTO<UserDTO>> handler) {
        super(handler);
        this.mEmail = email;
        this.mPassword = password;
    }

    @Override
    protected ValueDTO<UserDTO> doInBackground(Void... strings) {
        ValueDTO<UserDTO> loggedInUser;
        try {
            loggedInUser = userResource.login(mEmail, mPassword);
        } catch (RuntimeException ex) {
            loggedInUser = new ValueDTO<UserDTO>();
            loggedInUser.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in login task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
                loggedInUser.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return loggedInUser;
    }


}
