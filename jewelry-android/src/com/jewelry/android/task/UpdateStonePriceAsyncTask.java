package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.StonePriceDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;

import java.util.List;

/**
 * User: mos
 */
public class UpdateStonePriceAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<Boolean>> {
    private static final String TAG = "UpdateStonePriceAsyncTask";
	private List<StonePriceDTO> mPriceList;

    public UpdateStonePriceAsyncTask(AsyncTaskHandler<ValueDTO<Boolean>> handler, List<StonePriceDTO> priceList) {
        super(handler);
        mPriceList = priceList;
    }

    @Override
    protected ValueDTO<Boolean> doInBackground(Void... strings) {
	    ValueDTO<Boolean> result;
        try {
	        result = stonePriceResource.updatePrice(mPriceList);
        } catch (RuntimeException ex) {
	        result = new ValueDTO<Boolean>();
	        result.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in add diameter task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
	            result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return result;
    }
}
