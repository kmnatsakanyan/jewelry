package com.jewelry.android.task;

/**
 * User: karen.mnatsakanyan
 */
public interface AsyncTaskProgressHandler {

    public void onProgressUpdate(Integer... progress);
}
