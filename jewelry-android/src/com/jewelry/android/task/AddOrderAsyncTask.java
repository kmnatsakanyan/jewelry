package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.util.BitmapUtils;
import com.jewelry.dto.*;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;
import org.joda.time.DateTime;

import java.util.List;

public class AddOrderAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<OrderDTO>> {
    private static final String TAG = "OrderAsyncTask";
    private OrderDTO order;

    public AddOrderAsyncTask(AsyncTaskHandler<ValueDTO<OrderDTO>> handler,OrderDTO order) {
        super(handler);
        this.order = order;
    }

    @Override
    protected ValueDTO<OrderDTO> doInBackground(Void... strings) {
        ValueDTO<OrderDTO> result;
        try {
                result = orderResource.addOrder(order);
            } catch (RuntimeException ex) {
                result = new ValueDTO<OrderDTO>();
                result.setResultCode(JewelryConstant.RESPONSE_ERROR);
                Log.e(TAG, "Exception in update orders task " + ex.getMessage());
                if (ex.getCause() instanceof ServiceUnavailableException) {
                    result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
                }
            }
        return result;
    }

}
