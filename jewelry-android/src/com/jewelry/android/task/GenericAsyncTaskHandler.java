package com.jewelry.android.task;

import android.widget.Toast;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.dto.ValueDTO;
import com.jewelry.util.JewelryConstant;

/**
 * User: karen.mnatsakanyan
 */
public abstract class GenericAsyncTaskHandler<T extends ValueDTO> implements AsyncTaskHandler<T> {

    @Override
    public void handleError(T result) {
        switch (result.getResultCode()) {
            case JewelryConstant.RESPONSE_ERROR:
                Toast.makeText(JewelryApplication.instance(), JewelryApplication.instance().getAppContext().getResources().getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                break;
            case JewelryConstant.RESPONSE_TIMEOUT:
                Toast.makeText(JewelryApplication.instance(), JewelryApplication.instance().getAppContext().getResources().getString(R.string.error_service_timeout), Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
