package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.StoneDiameterDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.util.JewelryConstant;
import org.joda.time.DateTime;

/**
 * User: mos
 */
public class UpdateDiameterAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<StoneDiameterDTO>> {
    private static final String TAG = "AddDiameterAsyncTask";
	private StoneDiameterDTO mDiameterDTO;

    public UpdateDiameterAsyncTask(AsyncTaskHandler<ValueDTO<StoneDiameterDTO>> handler, StoneDiameterDTO diameterDTO) {
        super(handler);
        mDiameterDTO = diameterDTO;
        mDiameterDTO.setDeleted(false);
        mDiameterDTO.setUpdateDate(DateTime.now());
    }

    @Override
    protected ValueDTO<StoneDiameterDTO> doInBackground(Void... strings) {
	    ValueDTO<StoneDiameterDTO> result;
        try {
	        result = stoneDiameterResource.saveDiameter(mDiameterDTO);
        } catch (RuntimeException ex) {
	        result = new ValueDTO<StoneDiameterDTO>();
	        result.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in add diameter task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
	            result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return result;
    }
}
