package com.jewelry.android.task;

import android.util.Log;
import com.jewelry.dto.UserDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.exception.UserExistException;
import com.jewelry.util.JewelryConstant;

/**
 * User: karen.mnatsakanyan
 */
public class AddUserAsyncTask extends AbstractAsyncTask<Void, Void, ValueDTO<UserDTO>>{

    private static final String TAG = "AddUserAsyncTask";
    private UserDTO mUserDTO;

    public AddUserAsyncTask(AsyncTaskHandler<ValueDTO<UserDTO>> handler, UserDTO userDTO) {
        super(handler);
        mUserDTO = userDTO;
    }

    @Override
    protected ValueDTO<UserDTO> doInBackground(Void... params) {
        ValueDTO<UserDTO> result;
        try {
            result = userResource.saveUser(mUserDTO);
        } catch (RuntimeException ex) {
            result = new ValueDTO<UserDTO>();
            result.setResultCode(JewelryConstant.RESPONSE_ERROR);
            Log.e(TAG, "Exception in add user task " + ex.getMessage());
            if (ex.getCause() instanceof ServiceUnavailableException) {
                result.setResultCode(((ServiceUnavailableException) ex.getCause()).getStatusCode());
            }
        }
        return result;
    }
}
