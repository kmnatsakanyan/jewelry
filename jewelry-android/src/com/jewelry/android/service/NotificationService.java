package com.jewelry.android.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.ui.MainActivity;
import com.jewelry.android.util.JewelryMobileConstant;
import com.jewelry.dto.OrderDTO;
import com.jewelry.dto.ValueDTO;
import org.joda.time.DateTime;

import java.util.List;
import java.util.TimerTask;

/**
 * User: karen.mnatsakanyan
 */
public class NotificationService extends Service {

    private static final String TAG = "MyService";

    ProductDAOHelper DAO;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Log.d(TAG, "onStart");

        DAO = new ProductDAOHelper(NotificationService.this.getApplicationContext());
        new Thread(runnable).start();
        new Thread(updateTask).start();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
    }

    private TimerTask runnable = new TimerTask() {
        @Override
        public void run() {
            while (true) {
                if (JewelryApplication.instance().getLoggedInUser() != null) {
                    List temps = DAO.getTempProductList();
                    if (temps != null && temps.size() > 0) {
                        displayNotification(getString(R.string.notification_product_title),
                                getString(R.string.notification_product_text),
                                getString(R.string.notification_product_ticker),
                                JewelryMobileConstant.TEMP_PRODUCT_NOTIFICATION);
                    }

                    temps = DAO.getTempOrderList();
                    if (temps != null && temps.size() > 0) {
                        displayNotification(getString(R.string.notification_order_title),
                                getString(R.string.notification_order_text),
                                getString(R.string.notification_order_ticker),
                                JewelryMobileConstant.TEMP_ORDER_NOTIFICATION);
                    }
                }
                try {
                    Thread.sleep(JewelryMobileConstant.NOTIFICATION_SERVICE_INTERVAL);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private TimerTask updateTask = new TimerTask() {
        @Override
        public void run() {
            while (true) {
                    if (JewelryApplication.instance().getLoggedInUser() != null) {
                        ValueDTO<List<OrderDTO>> result;
                        try {
                            DateTime dt = DAO.getOrdersLastUpdateDate();
                            result = JewelryApplication.instance().getClient().getOrderResource().getUpdates(dt, JewelryApplication.instance().getLoggedInUser().getId());
                            DAO.saveOrderList(result.getValue());
                        } catch (RuntimeException ex) {
                            result = new ValueDTO<List<OrderDTO>>();
                        }
                        if (result.getValue() != null && result.getValue().size() > 0) {
                            displayNotification(getString(R.string.notification_new_order_title),
                                    getString(R.string.notification_new_order_text),
                                    getString(R.string.notification_new_order_ticker),
                                    JewelryMobileConstant.NEW_ORDER_NOTIFICATION);
                        }
                    }
                try {
                    Thread.sleep(JewelryMobileConstant.UPDATE_SERVICE_INTERVAL);
                } catch (Throwable e) {
                    Log.e("updateTask",e.getMessage());
                }
            }
        }
    };

    protected void displayNotification(String title, String text, String ticker, int notificationId) {
        // Invoking the default notification service
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);

        mBuilder.setContentTitle(title);
        mBuilder.setContentText(text);
        mBuilder.setTicker(ticker);
        mBuilder.setSmallIcon(R.drawable.ic_launcher);
        mBuilder.setAutoCancel(true);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra("notificationId", notificationId);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_ONE_SHOT //can only be used once
                );
        // start the activity when the user clicks the notification text
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager myNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // pass the Notification object to the system
        myNotificationManager.notify(notificationId, mBuilder.build());
    }
}