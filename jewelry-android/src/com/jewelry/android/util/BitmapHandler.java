
package com.jewelry.android.util;

import android.graphics.Bitmap;

public interface BitmapHandler {
    public void handleResponse(Bitmap bitmap);
}
