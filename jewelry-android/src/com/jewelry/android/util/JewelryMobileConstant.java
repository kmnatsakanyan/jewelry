package com.jewelry.android.util;

/**
 * Created by Mos on 27.02.14.
 */
public class JewelryMobileConstant {
    public static final String JPG_FORMAT = ".jpg";
    public static final String THUMBNAIL_PREFIX = "_thumbnail";

    public static final float GOLD_TYPE_RATE = 1.20f;
    public static final float GOLD_UNIT = 31.1f;
    public static final float GOLD_TRANSPORT_PRICE = 0.4f;
    public static final float GOLD_TYPE_CARAT_0 = 1.7f;
    public static final float GOLD_TYPE_CARAT_1 = 1.33f;
    public static final String KEY_PREF_LOGGED_IN_USER = "KEY_PREF_LOGGED_IN_USER";
    public static final String PREFS_CONFIG_FILE_NANE = "jewelry";
    public static final String PREFS_LAST_UPDATE_TIMESTAMP = "lastUpdateTimestamp";
    public static final String KEY_GOLD_COST = "KEY_GOLD_COST";
    public static final String KEY_WHOLESALE = "WHOLESALE";
    public static final String KEY_RETAIL = "RETAIL";
    public static final String KEY_SHOP = "SHOP";
    public static final String PEROPS_CONFIG_FILE_NAME = "jewelry.properties";
    public static final String DATABASE_NAME = "JEWELRY";
    public static final int DATABASE_VERSION = 1;
    public static final String PHOTO_URL = "photo/get?photo=";
    public static final int CAPTURE_PHOTO_SIZE_X = 1000;
    public static final int CAPTURE_PHOTO_SIZE_Y = 1000;

    public static final int SHOW_PHOTO_SIZE_X = 500;
    public static final int SHOW_PHOTO_SIZE_Y = 500;

    public static final String IMAGE_CACHE_DIR = "thumbs";

    public static final long NOTIFICATION_SERVICE_INTERVAL = 7200000;
    public static final long UPDATE_SERVICE_INTERVAL = 600000;
    public static final int TEMP_PRODUCT_NOTIFICATION = 4;
    public static final int TEMP_ORDER_NOTIFICATION = 1;
    public static final int NEW_ORDER_NOTIFICATION = 1;
    public static final Long ONE_DAY_IN_MILLISECONDS = 86400000L;
}
