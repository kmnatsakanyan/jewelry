package com.jewelry.android.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Base64OutputStream;
import android.util.Log;

import java.io.*;

public class IOUtils {
    private static String TAG = "IOUtils";

    public static boolean safeClose(Closeable in) {
        try {
            if (in != null) {
                in.close();
            }
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public static void storeObjectToPreferences(Context context, String key, Object object) {
        SharedPreferences pref = context.getSharedPreferences(context.getApplicationContext().getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = pref.edit();
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

        ObjectOutputStream objectOutput;
        try {
            objectOutput = new ObjectOutputStream(arrayOutputStream);
            objectOutput.writeObject(object);
            byte[] data = arrayOutputStream.toByteArray();
            objectOutput.close();
            arrayOutputStream.close();

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            Base64OutputStream b64 = new Base64OutputStream(out, Base64.DEFAULT);
            b64.write(data);
            b64.close();
            out.close();

            ed.putString(key, new String(out.toByteArray()));

            ed.commit();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public static Object getObjectFromPreferences(Context context, String key) {
        SharedPreferences pref = context.getSharedPreferences(context.getApplicationContext().getPackageName(), Context.MODE_PRIVATE);
        byte[] bytes = pref.getString(key, "{}").getBytes();
        if (bytes.length == 0) {
            return null;
        }
        ByteArrayInputStream byteArray = new ByteArrayInputStream(bytes);
        Base64InputStream base64InputStream = new Base64InputStream(byteArray, Base64.DEFAULT);
        Object result = null;
        try {
            ObjectInputStream in = new ObjectInputStream(base64InputStream);
            result = in.readObject();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } catch (ClassNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }
        return result;
    }
}
