
package com.jewelry.android.util;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.task.ModernAsyncTask;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BitmapUtils {

    // directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "JewelryPhotos";

    private static class BitmapLoaderAsyncTask extends ModernAsyncTask<Void, Void, Bitmap> {
        private BitmapHandler mHandler;
        private ContentResolver mResolver;
        private Uri mUri;
        private int mReqWidth;
        private int mReqHeight;
        private String mFileName;

        public BitmapLoaderAsyncTask(BitmapHandler handler, ContentResolver resolver, Uri uri,
                                     int reqWidth, int reqHeight, String filename) {
            mHandler = handler;
            mResolver = resolver;
            mUri = uri;
            mReqWidth = reqWidth;
            mReqHeight = reqHeight;
            mFileName = filename;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            return getDownsampledBitmap(mResolver, mUri, mReqWidth, mReqHeight, mFileName);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            if (result != null) {
                mHandler.handleResponse(result);
            }
        }
    }


    public static Bitmap getDownsampledBitmap(ContentResolver resolver, Uri uri, int targetWidth, int targetHeight, String filename) {
        Bitmap bitmap = null;
        try {
            BitmapFactory.Options outDimens = getBitmapDimensions(resolver, uri);

            int sampleSize = calculateSampleSize(outDimens.outWidth, outDimens.outHeight, targetWidth, targetHeight);

            bitmap = downsampleBitmap(resolver, uri, sampleSize);

        } catch (Exception e) {
            //handle the exception(s)
        }
        if (bitmap != null) {
            try {
                bitmap = BitmapUtils.rotateBitmapIfNeded(bitmap, filename);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bitmap;
    }

    private static BitmapFactory.Options getBitmapDimensions(ContentResolver resolver, Uri uri) throws FileNotFoundException, IOException {
        BitmapFactory.Options outDimens = new BitmapFactory.Options();
        outDimens.inJustDecodeBounds = true; // the decoder will return null (no bitmap)

        InputStream is = resolver.openInputStream(uri);
        // if Options requested only the size will be returned
        BitmapFactory.decodeStream(is, null, outDimens);
        is.close();

        return outDimens;
    }

    private static int calculateSampleSize(int width, int height, int targetWidth, int targetHeight) {

        int bitmapResolution = (int) ((float) width * (float) height);
        int targetResolution = targetWidth * targetHeight;

        int sampleSize = 1;

        if (targetResolution == 0) {
            return sampleSize;
        }

        for (int i = 1; (bitmapResolution / i) > targetResolution; i *= 2) {
            sampleSize = i;
        }

        return sampleSize;
    }



    private static Bitmap downsampleBitmap(ContentResolver resolver, Uri uri, int sampleSize) throws IOException {
        Bitmap resizedBitmap;
        BitmapFactory.Options outBitmap = new BitmapFactory.Options();
        outBitmap.inJustDecodeBounds = false; // the decoder will return a bitmap
        outBitmap.inSampleSize = sampleSize;

        InputStream is = resolver.openInputStream(uri);
        resizedBitmap = BitmapFactory.decodeStream(is, null, outBitmap);
        is.close();

        return resizedBitmap;
    }


    public static void decodeBitmapAsync(BitmapHandler handler, ContentResolver resolver, Uri uri,
                                         int reqWidth, int reqHeight, String filename) {
        new BitmapLoaderAsyncTask(handler, resolver, uri, reqWidth, reqHeight, filename).execute();
    }



    public static String getAlbumStorageDir() {
        // Get the directory for the app's private pictures directory.
        File file = new File(JewelryApplication.instance().getExternalFilesDir(
                Environment.DIRECTORY_PICTURES), "products");
        if (!file.exists() && !file.mkdirs()) {
            Log.e("TAG", "Directory not created");
        }
        return file.getAbsolutePath();
    }

    public static Bitmap rotateBitmapIfNeded(Bitmap b, String fileName)
            throws IOException {
        Bitmap imageBitmap = b;

        // Change bitmap's orientation if needed
        ExifInterface exif = new ExifInterface(fileName);
        int oreientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                -1);
        Matrix matrix = new Matrix();
        if (oreientation == ExifInterface.ORIENTATION_ROTATE_90) {
            matrix.postRotate(90);
        } else if (oreientation == ExifInterface.ORIENTATION_ROTATE_180) {
            matrix.postRotate(180);
        } else if (oreientation == ExifInterface.ORIENTATION_ROTATE_270) {
            matrix.postRotate(270);
        }
        imageBitmap = Bitmap.createBitmap(imageBitmap, 0, 0,
                imageBitmap.getWidth(), imageBitmap.getHeight(), matrix, true);

        return imageBitmap;
    }



    public static void deletePhoto(String path) {
        try {
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception ex) {
            Log.e("BitmapUtils.deletePhoto", ex.getMessage());
        }
    }

    public static void deleteProductPhotosIfExists(String productPhotoKey) {
        List<String> files = getProductPhotosIfExists(productPhotoKey);
        for (String file : files) {
            deletePhoto(file);
        }
    }

    public static List<String> getProductPhotosIfExists(String photoKey) {
        List<String> paths = new ArrayList<String>();
        File directory = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                BitmapUtils.IMAGE_DIRECTORY_NAME
        );
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.getName().contains(photoKey)) {
                    paths.add(file.getAbsolutePath());
                }
            }
        }
        return paths;
    }

    public static byte[] getBytesFormFile(String fileName) {
        File file = new File(fileName);
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }
}
