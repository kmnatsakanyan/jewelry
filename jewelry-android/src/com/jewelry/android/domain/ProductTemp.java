package com.jewelry.android.domain;

import com.jewelry.dto.ProductCatalogDTO;
import com.jewelry.dto.ProductStoneDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class ProductTemp {

    private Integer id;

    private Integer productId;

    private String productName;

    private Integer type;

    private Float weight;

    private List<ProductStoneDTO> productStones;

    private List<ProductCatalogDTO> productCatalogs = new ArrayList<ProductCatalogDTO>();

    private String productPhotoKey;

    private Boolean isDeleted;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getProductPhotoKey() {
        return productPhotoKey;
    }

    public void setProductPhotoKey(String productPhotoKey) {
        this.productPhotoKey = productPhotoKey;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public List<ProductStoneDTO> getProductStones() {
        return productStones;
    }

    public void setProductStones(List<ProductStoneDTO> productStones) {
        this.productStones = productStones;
    }

    public List<ProductCatalogDTO> getProductCatalogs() {
        return productCatalogs;
    }

    public void setProductCatalogs(List<ProductCatalogDTO> productCatalogs) {
        this.productCatalogs = productCatalogs;
    }
}
