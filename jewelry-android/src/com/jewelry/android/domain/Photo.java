package com.jewelry.android.domain;

import java.io.Serializable;

/**
 * User: karen.mnatsakanyan
 */
public class Photo implements Serializable {
    String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
