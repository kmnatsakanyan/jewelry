package com.jewelry.android.domain;

import com.jewelry.dto.OrderDTO;
/**
 * User: karen.mnatsakanyan
 */
public class OrderTemp {

    private Integer id;

    private OrderDTO orderDTO;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public OrderDTO getOrderDTO() {
        return orderDTO;
    }

    public void setOrderDTO(OrderDTO orderDTO) {
        this.orderDTO = orderDTO;
    }
}
