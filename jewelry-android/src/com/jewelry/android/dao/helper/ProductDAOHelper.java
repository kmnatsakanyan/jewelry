package com.jewelry.android.dao.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.jewelry.android.domain.OrderTemp;
import com.jewelry.android.domain.ProductTemp;
import com.jewelry.dto.*;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import static com.jewelry.android.util.JewelryMobileConstant.DATABASE_NAME;
import static com.jewelry.android.util.JewelryMobileConstant.DATABASE_VERSION;

/**
 * Created by Mos on 23.03.14.
 */
public class ProductDAOHelper extends AbstractDAOHelper {

    private static final String TABLE_PRODUCT = "PRODUCT";
    private static final String TABLE_PRODUCT_TEMP = "PRODUCT_TEMP";
    private static final String TABLE_PRODUCT_STONE_TEMP = "PRODUCT_STONE_TEMP";
    private static final String TABLE_PRODUCT_STONE = "PRODUCT_STONE";
    private static final String TABLE_PRODUCT_PHOTO = "PRODUCT_PHOTO";

    private static final String KEY_TYPE = "TYPE";
    private static final String KEY_WEIGHT = "WEIGHT";

    //Photo table
    private static final String KEY_PHOTO_FILE = "PHOTO_FILE";
    private static final String KEY_PHOTO_THUMBNAIL_FILE = "PHOTO_THUMBNAIL_FILE";


    //Order
    private static final String TABLE_ORDER = "PRODUCT_ORDER";
    private static final String TABLE_STONE_ORDER = "STONE_ORDER";
    private static final String TABLE_ORDER_TEMP = "PRODUCT_ORDER_TEMP";
    private static final String TABLE_STONE_ORDER_TEMP = "STONE_ORDER_TEMP";
    private static final String KEY_ORDER_ID = "ORDER_ID";
    private static final String KEY_ORDER_TYPE = "ORDER_TYPE";
    private static final String KEY_COUNT = "COUNT";
    private static final String KEY_DONE = "DONE_COUNT";
    private static final String KEY_PRODUCT_ID = "PRODUCT_ID";
    private static final String KEY_DATE = "DATE";
    private static final String KEY_USER_ID = "USER_ID";
    private static final String KEY_USER_NAME = "KEY_USER_NAME";
    private static final String KEY_QUALITY_ID = "QUALITY_ID";
    private static final String KEY_TEMP_PRODUCT_PHOTO_KEY = "TEMP_PRODUCT_PHOTO_KEY";
    private static final String KEY_PRODUCT_NAME = "PRODUCT_NAME";
    private static final String KEY_DESCRIPTION = "DESCRIPTION";
    private static final String KEY_GOLD_TYPE = "GOLD_TYPE";

    //Catalog
    private static final String TABLE_CATALOG = "CATALOG";
    private static final String TABLE_PRODUCT_CATALOG = "PRODUCT_CATALOG";
    private static final String TABLE_TEMP_PRODUCT_CATALOG = "TEMP_PRODUCT_CATALOG";
    private static final String KEY_LABEL = "PRODUCT_LABEL";
    private static final String KEY_CATALOG_ID = "CATALOG_ID";


    //Stone
    private static final String TABLE_STONE_PRICE = "STONE_PRICE";
    private static final String TABLE_STONE_DIAMETER = "STONE_DIAMETER";
    private static final String TABLE_DIAMOND_WEIGHT = "DIAMOND_WEIGHT";
    private static final String TABLE_STONE = "STONE";
    private static final String TABLE_STONE_QUALITY = "STONE_QUALITY";
    private static final String KEY_QUALITY = "QUALITY";
    private static final String KEY_NAME = "NAME";
    private static final String KEY_STONE_ID = "STONE_ID";
    private static final String KEY_DIAMETER_ID = "DIAMETER_ID";
    private static final String KEY_COST = "COST";
    private static final String KEY_DIAMETER = "DIAMETER";
    private static final String KEY_START = "START";
    private static final String KEY_END = "END";
    private static final String KEY_MAX = "MAX";
    private static final String KEY_MIN = "MIN";

    //Product Type
    private static final String TABLE_PRODUCT_TYPE = "PRODUCT_TYPE";


    // Stone
    private static final String CREATE_STONE_PRICE = "CREATE TABLE "
            + TABLE_STONE_PRICE + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_STONE_ID + " INTEGER," + KEY_DIAMETER_ID + " INTEGER,"
            + KEY_QUALITY_ID + " INTEGER," + KEY_COST + " REAL,"
            + KEY_IS_DELETED + " BOOLEAN," + KEY_UPDATED_DATE + " INTEGER" + ")";

    private static final String CREATE_STONE_DIAMETER = "CREATE TABLE "
            + TABLE_STONE_DIAMETER + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_STONE_ID + " INTEGER,"
            + KEY_START + " TEXT," + KEY_END + " TEXT," + KEY_IS_DELETED + " BOOLEAN,"
            + KEY_UPDATED_DATE + " INTEGER" + ")";

    private static final String CREATE_DIAMOND_WEIGHT = "CREATE TABLE "
            + TABLE_DIAMOND_WEIGHT + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_DIAMETER + " TEXT," + KEY_MAX + " REAL," + KEY_MIN + " REAL," + KEY_IS_DELETED + " BOOLEAN,"
            + KEY_UPDATED_DATE + " INTEGER" + ")";

    private static final String CREATE_STONE = "CREATE TABLE "
            + TABLE_STONE + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_NAME + " TEXT," + KEY_IS_DELETED + " BOOLEAN,"
            + KEY_UPDATED_DATE + " INTEGER" + ")";


    private static final String CREATE_STONE_QUALITY = "CREATE TABLE "
            + TABLE_STONE_QUALITY + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_STONE_ID + " INTEGER," + KEY_QUALITY + " TEXT,"
            + KEY_IS_DELETED + " BOOLEAN," + KEY_UPDATED_DATE + " INTEGER" + ")";


    // Order
    private static final String CREATE_TABLE_ORDER = "CREATE TABLE "
            + TABLE_ORDER + "(" +
            KEY_ID + " INTEGER PRIMARY KEY," +
            KEY_PRODUCT_ID + " INTEGER," +
            KEY_ORDER_TYPE + " TEXT," +
            KEY_COUNT + " INTEGER," +
            KEY_DONE + " INTEGER," +
            KEY_USER_ID + " INTEGER," +
            KEY_USER_NAME + " TEXT," +
            KEY_DATE + " INTEGER," +
            KEY_IS_DELETED + " BOOLEAN," +
            KEY_UPDATED_DATE + " INTEGER," +
            KEY_DESCRIPTION + " TEXT," +
            KEY_GOLD_TYPE + " INTEGER" + ")";

    private static final String CREATE_TABLE_STONE_ORDER = "CREATE TABLE "
            + TABLE_STONE_ORDER + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_ORDER_ID
            + " INTEGER," + KEY_QUALITY_ID + " INTEGER," + KEY_DIAMETER + " REAL,"
            + KEY_COUNT + " INTEGER,"
            + KEY_IS_DELETED + " BOOLEAN," + KEY_UPDATED_DATE + " INTEGER" + ")";

    private static final String CREATE_TABLE_ORDER_TEMP = "CREATE TABLE "
            + TABLE_ORDER_TEMP + "(" +
            KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            KEY_ORDER_ID + " INTEGER," +
            KEY_PRODUCT_ID + " INTEGER," +
            KEY_COUNT + " INTEGER," +
            KEY_DONE + " INTEGER," +
            KEY_ORDER_TYPE + " TEXT," +
            KEY_USER_ID + " INTEGER," +
            KEY_USER_NAME + " TEXT," +
            KEY_DATE + " INTEGER," +
            KEY_IS_DELETED + " BOOLEAN," +
            KEY_UPDATED_DATE + " INTEGER," +
            KEY_DESCRIPTION + " TEXT," +
            KEY_GOLD_TYPE + " INTEGER" + ")";

    private static final String CREATE_TABLE_STONE_ORDER_TEMP = "CREATE TABLE "
            + TABLE_STONE_ORDER_TEMP + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_ORDER_ID
            + " INTEGER," + KEY_QUALITY_ID + " INTEGER," + KEY_DIAMETER + " REAL,"
            + KEY_COUNT + " INTEGER,"
            + KEY_IS_DELETED + " BOOLEAN," + KEY_UPDATED_DATE + " INTEGER" + ")";

    // Product Type
    private static final String CREATE_TABLE_PRODUCT_TYPE = "CREATE TABLE "
            + TABLE_PRODUCT_TYPE + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_IS_DELETED + " BOOLEAN," + KEY_UPDATED_DATE
            + " INTEGER" + ")";

    // Table Create Statements
    private static final String CREATE_TABLE_PRODUCT = "CREATE TABLE "
            + TABLE_PRODUCT + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_PRODUCT_ID
            + " TEXT," + KEY_TYPE + " INTEGER," + KEY_WEIGHT
            + " REAL," + KEY_IS_DELETED + " BOOLEAN," + KEY_UPDATED_DATE
            + " INTEGER" + ")";

    private static final String CREATE_TABLE_PRODUCT_STONE = "CREATE TABLE "
            + TABLE_PRODUCT_STONE + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_PRODUCT_ID
            + " INTEGER," + KEY_DIAMETER + " REAL," + KEY_COUNT
            + " INTEGER," + KEY_IS_DELETED + " BOOLEAN," + KEY_UPDATED_DATE
            + " INTEGER" + ")";

    private static final String CREATE_TABLE_PRODUCT_PHOTO = "CREATE TABLE "
            + TABLE_PRODUCT_PHOTO + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_PRODUCT_ID
            + " INTEGER," + KEY_PHOTO_FILE + " TEXT," + KEY_PHOTO_THUMBNAIL_FILE
            + " TEXT," + KEY_IS_DELETED + " BOOLEAN," + KEY_UPDATED_DATE
            + " INTEGER" + ")";

    // Table Create Statements
    private static final String CREATE_TABLE_TEMP_PRODUCT = "CREATE TABLE "
            + TABLE_PRODUCT_TEMP + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            + KEY_PRODUCT_ID + " INTEGER,"
            + KEY_PRODUCT_NAME + " TEXT,"
            + KEY_TYPE + " INTEGER," + KEY_WEIGHT + " REAL,"
            + KEY_IS_DELETED + " BOOLEAN,"
            + KEY_TEMP_PRODUCT_PHOTO_KEY + " TEXT)";

    private static final String CREATE_TABLE_TEMP_STONE = "CREATE TABLE "
            + TABLE_PRODUCT_STONE_TEMP + "(" +
            KEY_ID + " INTEGER PRIMARY KEY," +
            KEY_PRODUCT_ID + " INTEGER," +
            KEY_DIAMETER + " REAL," +
            KEY_COUNT + " INTEGER," +
            KEY_IS_DELETED + " BOOLEAN)";


    //CATALOG

    private static final String CREATE_TABLE_CATALOG = "CREATE TABLE "
            + TABLE_CATALOG + "(" +
            KEY_ID + " INTEGER PRIMARY KEY," +
            KEY_NAME + " TEXT," +
            KEY_IS_DELETED + " BOOLEAN," +
            KEY_UPDATED_DATE + " INTEGER" + ")";
    private static final String CREATE_TABEL_PRODUCT_CATALOG = "CREATE TABLE "
            + TABLE_PRODUCT_CATALOG + "(" +
            KEY_ID + " INTEGER PRIMARY KEY," +
            KEY_LABEL + " TEXT," +
            KEY_CATALOG_ID + " INTEGER," +
            KEY_PRODUCT_ID + " INTEGER," +
            KEY_IS_DELETED + " BOOLEAN," +
            KEY_UPDATED_DATE + " INTEGER" + ")";

    private static final String CREATE_TABEL_TEMP_PRODUCT_CATALOG = "CREATE TABLE "
            + TABLE_TEMP_PRODUCT_CATALOG + "(" +
            KEY_ID + " INTEGER PRIMARY KEY," +
            KEY_LABEL + " TEXT," +
            KEY_CATALOG_ID + " INTEGER," +
            KEY_PRODUCT_ID + " INTEGER," +
            KEY_IS_DELETED + " BOOLEAN," +
            KEY_UPDATED_DATE + " INTEGER" + ")";

    public ProductDAOHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_PRODUCT);
        db.execSQL(CREATE_TABLE_PRODUCT_STONE);
        db.execSQL(CREATE_TABLE_PRODUCT_PHOTO);
        db.execSQL(CREATE_TABLE_ORDER);
        db.execSQL(CREATE_TABLE_STONE_ORDER);
        db.execSQL(CREATE_TABLE_PRODUCT_TYPE);
        db.execSQL(CREATE_STONE_PRICE);
        db.execSQL(CREATE_STONE_DIAMETER);
        db.execSQL(CREATE_STONE);
        db.execSQL(CREATE_STONE_QUALITY);
        db.execSQL(CREATE_TABLE_TEMP_PRODUCT);
        db.execSQL(CREATE_TABLE_TEMP_STONE);
        db.execSQL(CREATE_DIAMOND_WEIGHT);
        db.execSQL(CREATE_TABLE_ORDER_TEMP);
        db.execSQL(CREATE_TABLE_CATALOG);
        db.execSQL(CREATE_TABLE_STONE_ORDER_TEMP);
        db.execSQL(CREATE_TABEL_PRODUCT_CATALOG);
        db.execSQL(CREATE_TABEL_TEMP_PRODUCT_CATALOG);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i2) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT_STONE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT_PHOTO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STONE_ORDER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT_TYPE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STONE_PRICE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STONE_DIAMETER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STONE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STONE_QUALITY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT_TEMP);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT_STONE_TEMP);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DIAMOND_WEIGHT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STONE_ORDER_TEMP);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDER_TEMP);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATALOG);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT_CATALOG);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEMP_PRODUCT_CATALOG);
        onCreate(db);
    }

    public void clearAllData() {
        SQLiteDatabase db = this.getReadableDatabase();
        onUpgrade(db, 0, 0);
    }

    public DateTime getProductLastUpdateDate() {
        return getLastUpdatedDate(TABLE_PRODUCT);
    }


    public DateTime getDiamondWeightLastUpdateDate() {
        return getLastUpdatedDate(TABLE_DIAMOND_WEIGHT);
    }

    public DateTime getOrdersLastUpdateDate() {
        return getLastUpdatedDate(TABLE_ORDER);
    }

    public DateTime getCatalogsLastUpdateDate() {
        return getLastUpdatedDate(TABLE_CATALOG);
    }

    public DateTime getProductCatalogsLastUpdateDate() {
        return getLastUpdatedDate(TABLE_PRODUCT_CATALOG);
    }

    public List<ProductDTO> getProductList() {
        String selectQuery = "SELECT " + KEY_ID + "," + KEY_PRODUCT_ID + "," + KEY_TYPE + "," + KEY_WEIGHT + " FROM " + TABLE_PRODUCT + " WHERE " + KEY_IS_DELETED + "=0" +
                " ORDER BY   CAST(" + KEY_PRODUCT_ID +" AS INTEGER)";
        return getProductByQuery(selectQuery);
    }

    public List<ProductTemp> getTempProductList() {
        String selectQuery = "SELECT * FROM " + TABLE_PRODUCT_TEMP;
        SQLiteDatabase db = this.getReadableDatabase();
        List<ProductTemp> productList = new ArrayList<ProductTemp>();

        Log.d(ProductDAOHelper.class + "", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    ProductTemp productTemp = new ProductTemp();
                    productTemp.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                    if (!c.isNull(c.getColumnIndex(KEY_PRODUCT_ID))) {
                        productTemp.setProductId(c.getInt(c.getColumnIndex(KEY_PRODUCT_ID)));
                    }
                    productTemp.setType(c.getInt(c.getColumnIndex(KEY_TYPE)));
                    productTemp.setProductPhotoKey(c.getString(c.getColumnIndex(KEY_TEMP_PRODUCT_PHOTO_KEY)));
                    productTemp.setProductName(c.getString(c.getColumnIndex(KEY_PRODUCT_NAME)));
                    productTemp.setWeight(c.getFloat(c.getColumnIndex(KEY_WEIGHT)));
                    productTemp.setIsDeleted(c.getInt(c.getColumnIndex(KEY_IS_DELETED)) == 1);
                    productTemp.setProductStones(getTempStoneProduct(productTemp.getId()));
                    productTemp.setProductCatalogs(getTempProductCatalogs(productTemp.getId(), productTemp.getProductId()));
                    productList.add(productTemp);
                } while (c.moveToNext());

            }
            c.close();
        }
        return productList;
    }

    public int getTempProductListCount() {
        int count = 0;
        String selectQuery = "SELECT COUNT(*) as count FROM " + TABLE_PRODUCT_TEMP;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null) {
            if (c.moveToFirst()) {
                count = c.getInt(c.getColumnIndex("count"));
            }
        }
        return count;
    }

    public void updateProductTemp(ProductTemp product) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try {
            db.beginTransaction();
            values.put(KEY_PRODUCT_ID, product.getProductId());
            values.put(KEY_TYPE, product.getType());
            values.put(KEY_WEIGHT, product.getWeight());
            values.put(KEY_TEMP_PRODUCT_PHOTO_KEY, product.getProductPhotoKey());
            values.put(KEY_PRODUCT_NAME, product.getProductName());
            values.put(KEY_IS_DELETED, product.getIsDeleted());
            deleteTempProductStone(product.getId());
            saveProductTempStoneList(product.getProductStones(), product.getId());
            saveTempProductCatalogs(product.getProductCatalogs(), product.getId());
            db.update(TABLE_PRODUCT_TEMP, values, KEY_ID + "=" + product.getId(), null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("updateProductTemp", e.getMessage());
        } finally {
            db.endTransaction();
        }

    }

    public void saveTempProduct(ProductTemp product) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(KEY_PRODUCT_ID, product.getProductId());
            values.put(KEY_TYPE, product.getType());
            values.put(KEY_WEIGHT, product.getWeight());
            values.put(KEY_TEMP_PRODUCT_PHOTO_KEY, product.getProductPhotoKey());
            values.put(KEY_PRODUCT_NAME, product.getProductName());
            values.put(KEY_IS_DELETED, product.getIsDeleted());
            int id = (int) db.insert(TABLE_PRODUCT_TEMP, null, values);
            if (product.getProductStones() != null) {
                saveProductTempStoneList(product.getProductStones(), id);
            }
            if (product.getProductCatalogs() != null) {
                saveTempProductCatalogs(product.getProductCatalogs(), id);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("saveProductTemp", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    protected void saveProductTempStoneList(List<ProductStoneDTO> productStoneList, Integer productId) {
        SQLiteDatabase db = this.getWritableDatabase();

        for (ProductStoneDTO stone : productStoneList) {
            try {
                ContentValues values = new ContentValues();
                values.put(KEY_ID, stone.getId());
                values.put(KEY_IS_DELETED, stone.isDeleted());
                values.put(KEY_PRODUCT_ID, productId);
                values.put(KEY_DIAMETER, stone.getDiameter());
                values.put(KEY_COUNT, stone.getCount());
                db.insert(TABLE_PRODUCT_STONE_TEMP, null, values);
            } catch (Exception e) {
                Log.d("saveProductStoneTemp", e.getMessage());
            }

        }
    }

    protected List<ProductStoneDTO> getTempStoneProduct(Integer id) {
        String selectQuery = "SELECT * FROM " + TABLE_PRODUCT_STONE_TEMP + " WHERE " + KEY_PRODUCT_ID + "=" + id;
        SQLiteDatabase db = this.getReadableDatabase();
        List<ProductStoneDTO> stoneList = new ArrayList<ProductStoneDTO>();

        Log.d(ProductDAOHelper.class + "", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    ProductStoneDTO stoneTemp = new ProductStoneDTO();
                    stoneTemp.setDiameter(c.getFloat(c.getColumnIndex(KEY_DIAMETER)));
                    stoneTemp.setCount(c.getInt(c.getColumnIndex(KEY_COUNT)));
                    stoneTemp.setDeleted(c.getInt(c.getColumnIndex(KEY_IS_DELETED)) == 1);
                    stoneList.add(stoneTemp);
                } while (c.moveToNext());
            }
            c.close();
        }
        return stoneList;
    }

    public ProductTemp getTempProductByProductId(Integer id) {
        ProductTemp product = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PRODUCT_TEMP + " WHERE " + KEY_PRODUCT_ID + "=" + id;

        Log.d(ProductDAOHelper.class + "", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null) {
            if (c.moveToFirst()) {
                product = new ProductTemp();
                product.setId(id);
                product.setProductId(c.getInt(c.getColumnIndex(KEY_PRODUCT_ID)));
                product.setType(c.getInt(c.getColumnIndex(KEY_TYPE)));
                product.setWeight(c.getFloat(c.getColumnIndex(KEY_WEIGHT)));
                product.setProductPhotoKey(c.getString(c.getColumnIndex(KEY_TEMP_PRODUCT_PHOTO_KEY)));
                product.setProductName(c.getString(c.getColumnIndex(KEY_PRODUCT_NAME)));
                product.setIsDeleted(c.getInt(c.getColumnIndex(KEY_IS_DELETED)) == 1);
                product.setProductStones(getTempStoneProduct(id));
                product.setProductCatalogs(getTempProductCatalogs(id, null));
                c.close();
            }
        }
        return product;
    }

    public ProductTemp getTempProductById(Integer id) {
        ProductTemp product = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PRODUCT_TEMP + " WHERE " + KEY_ID + "=" + id;

        Log.d(ProductDAOHelper.class + "", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null) {
            if (c.moveToFirst()) {
                product = new ProductTemp();
                product.setId(id);
                product.setProductId(c.getInt(c.getColumnIndex(KEY_PRODUCT_ID)));
                product.setType(c.getInt(c.getColumnIndex(KEY_TYPE)));
                product.setWeight(c.getFloat(c.getColumnIndex(KEY_WEIGHT)));
                product.setProductPhotoKey(c.getString(c.getColumnIndex(KEY_TEMP_PRODUCT_PHOTO_KEY)));
                product.setProductName(c.getString(c.getColumnIndex(KEY_PRODUCT_NAME)));
                product.setProductStones(getTempStoneProduct(id));
                product.setProductCatalogs(getTempProductCatalogs(id, null));
                c.close();
            }
        }
        return product;
    }

    public void deleteTempProduct(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            db.beginTransaction();
            deleteTempProductStone(id);
            deleteTempProductCatalogs(id);
            db.delete(TABLE_PRODUCT_TEMP, KEY_ID + "=" + id, null);
            db.setTransactionSuccessful();

        } catch (Exception e) {
            Log.d("deleteProductTemp", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    public void deleteTempProductStone(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_PRODUCT_STONE_TEMP, KEY_PRODUCT_ID + "=" + id, null);
    }

    public void deleteTempProductCatalogs(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_TEMP_PRODUCT_CATALOG, KEY_PRODUCT_ID + "=" + id, null);
    }


    private List<ProductDTO> getProductByQuery(String selectQuery) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<ProductDTO> productList = new ArrayList<ProductDTO>();

        Log.d(ProductDAOHelper.class + "", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    ProductDTO productDTO = new ProductDTO();
                    productDTO.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                    productDTO.setProductId(c.getString(c.getColumnIndex(KEY_PRODUCT_ID)));
                    productDTO.setType(c.getInt(c.getColumnIndex(KEY_TYPE)));
                    productDTO.setWeight(c.getFloat(c.getColumnIndex(KEY_WEIGHT)));
                    productList.add(productDTO);
                } while (c.moveToNext());
            }
            c.close();
        }
        return productList;
    }

    public List<ProductDTO> searchProductByType(Integer id, String query) {
        String selectQuery = "SELECT " + KEY_ID + "," + KEY_PRODUCT_ID + "," + KEY_TYPE + "," + KEY_WEIGHT
                + " FROM " + TABLE_PRODUCT + " WHERE " + KEY_TYPE
                + "=" + "'" + id + "' and " + KEY_IS_DELETED + "=0";
        if (query != null) {
            selectQuery = selectQuery + " and " + KEY_PRODUCT_ID + " = " + "'%" + query + "%'";
        }
        return getProductByQuery(selectQuery);
    }

    public List<ProductDTO> searchProductByName(String query) {
        String selectQuery = "SELECT " + KEY_ID + "," + KEY_PRODUCT_ID + "," + KEY_TYPE + "," + KEY_WEIGHT
                + " FROM " + TABLE_PRODUCT + " WHERE " + KEY_IS_DELETED + "=0 " + " and " + KEY_PRODUCT_ID + " LIKE " + "'%" + query + "%'";
        return getProductByQuery(selectQuery);
    }

    public List<ProductDTO> searchProductByCatalog(Integer catalogId, String query) {
        String selectQuery = "SELECT PRODUCT." + KEY_ID + ",PRODUCT_CATALOG." + KEY_LABEL + " as " + KEY_PRODUCT_ID + ",PRODUCT." + KEY_TYPE + ",PRODUCT." + KEY_WEIGHT
                + " FROM PRODUCT " +
                " INNER JOIN PRODUCT_CATALOG ON PRODUCT_CATALOG.PRODUCT_ID=PRODUCT.ID" +
                " WHERE PRODUCT." + KEY_IS_DELETED + "=0 " +
                " and PRODUCT_CATALOG.IS_DELETED=0 and PRODUCT_CATALOG.CATALOG_ID=" + catalogId;
        if (query != null) {
            selectQuery = selectQuery + " and PRODUCT_CATALOG." + KEY_LABEL + " LIKE " + "'%" + query + "%'";
        }
        selectQuery = selectQuery + " ORDER BY CAST(PRODUCT_CATALOG.PRODUCT_LABEL AS INTEGER)";
        return getProductByQuery(selectQuery);
    }

    public List<ProductDTO> searchProductByTypeAndCatalog(Integer type, Integer catalogID, String query) {
        if (type == null && catalogID == null && query == null) {
            return getProductList();
        }
        if (catalogID == null && type != null) {
            return searchProductByType(type, query);
        }
        if (type == null && catalogID != null) {
            return searchProductByCatalog(catalogID, query);
        }

        if (query != null && catalogID == null) {
            return searchProductByName(query);
        }

        String selectQuery = "SELECT PRODUCT." + KEY_ID + ",PRODUCT_CATALOG." + KEY_LABEL + " as " + KEY_PRODUCT_ID + ",PRODUCT." + KEY_TYPE + ",PRODUCT." + KEY_WEIGHT
                + " FROM PRODUCT " +
                " INNER JOIN PRODUCT_CATALOG ON PRODUCT_CATALOG.PRODUCT_ID=PRODUCT.ID" +
                " WHERE PRODUCT." + KEY_TYPE +
                "=" + "'" + type + "' and PRODUCT." + KEY_IS_DELETED + "=0 " + " and PRODUCT_CATALOG.IS_DELETED=0 and PRODUCT_CATALOG.CATALOG_ID=" + catalogID;

        if (query != null) {
            selectQuery = selectQuery + " and PRODUCT_CATALOG." + KEY_LABEL + " LIKE " + "'%" + query + "%'";
        }
        selectQuery = selectQuery + " ORDER BY CAST(PRODUCT_CATALOG.PRODUCT_LABEL AS INTEGER)";
        return getProductByQuery(selectQuery);
    }

    public void saveProduct(ProductDTO product) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(KEY_ID, product.getId());
            values.put(KEY_IS_DELETED, product.isDeleted());
            values.put(KEY_UPDATED_DATE, product.getUpdateDate().getMillis());
            values.put(KEY_PRODUCT_ID, product.getProductId());
            values.put(KEY_TYPE, product.getType());
            values.put(KEY_WEIGHT, product.getWeight());
            deleteProductStone(product.getId());
            saveProductStoneList(product.getProductStones());
            saveProductPhotos(product.getProductPhotos());
            db.insertWithOnConflict(TABLE_PRODUCT, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("saveProductList", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    public void deleteProductStone(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_PRODUCT_STONE, KEY_PRODUCT_ID + "=" + id, null);
    }

    protected void saveProductStoneList(List<ProductStoneDTO> productStoneList) {
        SQLiteDatabase db = this.getWritableDatabase();

        for (ProductStoneDTO stone : productStoneList) {
            try {
                ContentValues values = new ContentValues();
                values.put(KEY_ID, stone.getId());
                values.put(KEY_IS_DELETED, stone.isDeleted());
                values.put(KEY_UPDATED_DATE, stone.getUpdateDate().getMillis());
                values.put(KEY_PRODUCT_ID, stone.getProductId());
                values.put(KEY_DIAMETER, stone.getDiameter());
                values.put(KEY_COUNT, stone.getCount());
                db.insert(TABLE_PRODUCT_STONE, null, values);
            } catch (Exception e) {
                Log.d("ProductDAOHelper", e.getMessage());
            }
        }
    }

    public ProductDTO getProduct(Integer id) {
        ProductDTO product = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PRODUCT + " WHERE " + KEY_ID + "=" + id + " and " + KEY_IS_DELETED + "=0";

        Log.d(ProductDAOHelper.class + "", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null) {
            if (c.moveToFirst()) {
                product = new ProductDTO();
                product.setId(id);
                product.setProductId(c.getString(c.getColumnIndex(KEY_PRODUCT_ID)));
                product.setType(c.getInt(c.getColumnIndex(KEY_TYPE)));
                product.setUpdateDate(new DateTime(c.getLong(c.getColumnIndex(KEY_UPDATED_DATE))));
                product.setWeight(c.getFloat(c.getColumnIndex(KEY_WEIGHT)));
                product.setProductStones(getStoneList(product.getId()));
                product.setProductPhotos(loadProductPhotosByProductId(product.getId()));
                product.setProductCatalogs(getProductCatalogs(product.getId()));
                c.close();
            }
        }
        return product;
    }

    protected List<ProductStoneDTO> getStoneList(Integer productId) {
        List<ProductStoneDTO> stoneList = new ArrayList<ProductStoneDTO>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PRODUCT_STONE + " WHERE " + KEY_PRODUCT_ID + "=" + productId;
        Log.d(ProductDAOHelper.class + "", selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    ProductStoneDTO productStoneDTO = new ProductStoneDTO();
                    productStoneDTO.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                    productStoneDTO.setDiameter(c.getFloat(c.getColumnIndex(KEY_DIAMETER)));
                    productStoneDTO.setCount(c.getInt(c.getColumnIndex(KEY_COUNT)));
                    stoneList.add(productStoneDTO);
                } while (c.moveToNext());
            }
            c.close();
        }

        return stoneList;
    }

    public void saveProductPhotos(List<ProductPhotoDTO> productPhotoDTOs) {
        SQLiteDatabase db = this.getWritableDatabase();

        for (ProductPhotoDTO productPhotoDTO : productPhotoDTOs) {
            try {
                ContentValues values = new ContentValues();
                values.put(KEY_ID, productPhotoDTO.getId());
                values.put(KEY_IS_DELETED, productPhotoDTO.isDeleted());
                values.put(KEY_UPDATED_DATE, productPhotoDTO.getUpdateDate().getMillis());
                values.put(KEY_PRODUCT_ID, productPhotoDTO.getProductId());
                values.put(KEY_PHOTO_FILE, productPhotoDTO.getPhotoFile());
                values.put(KEY_PHOTO_THUMBNAIL_FILE, productPhotoDTO.getPhotoThumbnailFile());
                db.insertWithOnConflict(TABLE_PRODUCT_PHOTO, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            } catch (Exception e) {
                Log.d("ProductDAOHelper", e.getMessage());
            }
        }
    }

    public void deletePhotoFromProduct(String imageName, Integer productId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_UPDATED_DATE, new DateTime().getMillis());
            values.put(KEY_IS_DELETED, 1);
            db.update(TABLE_PRODUCT_PHOTO, values, KEY_PHOTO_FILE + "='" + imageName + "' and " + KEY_PRODUCT_ID + "=" + productId, null);
        } catch (Exception e) {
            Log.d("ProductDAOHelper", e.getMessage());
        }
    }

    public List<ProductPhotoDTO> loadProductPhotosByProductId(Integer productId) {
        List<ProductPhotoDTO> productPhotoDTOs = new ArrayList<ProductPhotoDTO>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PRODUCT_PHOTO + " WHERE " + KEY_PRODUCT_ID + " = " + productId + " and " + KEY_IS_DELETED + "=0";
        Log.d(ProductDAOHelper.class + "", selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    ProductPhotoDTO productPhotoDTO = new ProductPhotoDTO();
                    productPhotoDTO.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                    productPhotoDTO.setUpdateDate(new DateTime(c.getLong(c.getColumnIndex(KEY_UPDATED_DATE))));
                    productPhotoDTO.setPhotoFile(c.getString(c.getColumnIndex(KEY_PHOTO_FILE)));
                    productPhotoDTO.setPhotoThumbnailFile(c.getString(c.getColumnIndex(KEY_PHOTO_THUMBNAIL_FILE)));
                    productPhotoDTO.setProductId(c.getInt(c.getColumnIndex(KEY_PRODUCT_ID)));
                    productPhotoDTOs.add(productPhotoDTO);
                } while (c.moveToNext());
            }
            c.close();
        }
        return productPhotoDTOs;
    }

    //Order
    public void saveOrder(OrderDTO order) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_IS_DELETED, order.isDeleted());
            values.put(KEY_UPDATED_DATE, order.getUpdateDate() != null ? order.getUpdateDate().getMillis() : null);
            values.put(KEY_PRODUCT_ID, order.getProduct().getId());
            values.put(KEY_ORDER_TYPE, order.getOrderType().toString());
            values.put(KEY_COUNT, order.getCount());
            values.put(KEY_DONE, order.getDone());
            values.put(KEY_USER_ID, order.getUser().getId());
            values.put(KEY_DATE, order.getDate().getMillis());
            values.put(KEY_DESCRIPTION, order.getDescription());
            values.put(KEY_GOLD_TYPE, order.getGoldType().ordinal());
            values.put(KEY_USER_ID, order.getUser().getId());
            values.put(KEY_USER_NAME, order.getUser().getEmail());
            values.put(KEY_ID, order.getId());
            db.insertWithOnConflict(TABLE_ORDER, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        } catch (Exception e) {
            Log.d("saveOrder", e.getMessage());
        }
    }

    public void setOrderIsDeleted(Integer orderId, Boolean isDeleted) {
        SQLiteDatabase db = this.getWritableDatabase();
        OrderDTO orderDTO = getOrdersById(orderId);
        if (orderDTO != null) {
            ContentValues values = new ContentValues();
            values.put(KEY_IS_DELETED, isDeleted);
            db.update(TABLE_ORDER, values, KEY_ID + "=" + orderId, null);
        }
    }

    public OrderDTO getOrdersById(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_ORDER + " WHERE " + KEY_ID + "=" + id;
        Log.d(ProductDAOHelper.class + "", selectQuery);
        OrderDTO orderDTO = null;

        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null) {

            if (c.moveToFirst()) {
                orderDTO = new OrderDTO();
                orderDTO.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                orderDTO.setCount(c.getInt(c.getColumnIndex(KEY_COUNT)));
                orderDTO.setDone(c.getInt(c.getColumnIndex(KEY_DONE)));
                orderDTO.setDate(new DateTime(c.getLong(c.getColumnIndex(KEY_DATE))));
                orderDTO.setDescription(c.getString(c.getColumnIndex(KEY_DESCRIPTION)));
                orderDTO.setOrderType(OrderType.valueOf(c.getString(c.getColumnIndex(KEY_ORDER_TYPE))));
                orderDTO.setGoldType(GoldType.values()[c.getInt(c.getColumnIndex(KEY_GOLD_TYPE))]);
                orderDTO.setProduct(getProduct(c.getInt(c.getColumnIndex(KEY_PRODUCT_ID))));
                orderDTO.setUpdateDate(new DateTime(c.getLong(c.getColumnIndex(KEY_UPDATED_DATE))));
                UserDTO userDTO = new UserDTO();
                userDTO.setId(c.getInt(c.getColumnIndex(KEY_USER_ID)));
                userDTO.setEmail(c.getString(c.getColumnIndex(KEY_USER_NAME)));
                orderDTO.setUser(userDTO);
                orderDTO.setStones(getOrderStones(orderDTO.getId()));
            }
        }
        return orderDTO;
    }

    public void saveOrderList(List<OrderDTO> orderList) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (OrderDTO order : orderList) {
                ContentValues values = new ContentValues();
                values.put(KEY_ID, order.getId());
                values.put(KEY_IS_DELETED, order.isDeleted());
                values.put(KEY_UPDATED_DATE, order.getUpdateDate().getMillis());
                values.put(KEY_ORDER_TYPE, order.getOrderType().toString());
                values.put(KEY_PRODUCT_ID, order.getProduct().getId());
                values.put(KEY_COUNT, order.getCount());
                values.put(KEY_DONE, order.getDone());
                values.put(KEY_USER_ID, order.getUser().getId());
                values.put(KEY_DATE, order.getDate().getMillis());
                values.put(KEY_DESCRIPTION, order.getDescription());
                values.put(KEY_GOLD_TYPE, order.getGoldType().ordinal());
                values.put(KEY_USER_ID, order.getUser().getId());
                values.put(KEY_USER_NAME, order.getUser().getEmail());
                saveOrderStones(order.getStones());
                db.insertWithOnConflict(TABLE_ORDER, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("saveOrderList", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    public List<OrderDTO> getOrders() {
        List<OrderDTO> orders = new ArrayList<OrderDTO>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_ORDER + " WHERE " + KEY_IS_DELETED + "=0"
                + " ORDER BY " + KEY_DATE + " DESC ";

        Log.d(ProductDAOHelper.class + "", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    OrderDTO orderDTO = new OrderDTO();
                    orderDTO.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                    orderDTO.setCount(c.getInt(c.getColumnIndex(KEY_COUNT)));
                    orderDTO.setDone(c.getInt(c.getColumnIndex(KEY_DONE)));
                    orderDTO.setDate(new DateTime(c.getLong(c.getColumnIndex(KEY_DATE))));
                    orderDTO.setDescription(c.getString(c.getColumnIndex(KEY_DESCRIPTION)));
                    orderDTO.setOrderType(OrderType.valueOf(c.getString(c.getColumnIndex(KEY_ORDER_TYPE))));
                    orderDTO.setGoldType(GoldType.values()[c.getInt(c.getColumnIndex(KEY_GOLD_TYPE))]);
                    orderDTO.setProduct(getProduct(c.getInt(c.getColumnIndex(KEY_PRODUCT_ID))));
                    if (orderDTO.getProduct() == null) {
                        continue;
                    }
                    UserDTO userDTO = new UserDTO();
                    userDTO.setId(c.getInt(c.getColumnIndex(KEY_USER_ID)));
                    userDTO.setEmail(c.getString(c.getColumnIndex(KEY_USER_NAME)));
                    orderDTO.setUser(userDTO);
                    orderDTO.setStones(getOrderStones(orderDTO.getId()));
                    orders.add(orderDTO);
                } while (c.moveToNext());
                c.close();
            }
        }
        return orders;
    }

    protected void saveOrderStones(List<StoneOrderDTO> orderStoneList) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (StoneOrderDTO stone : orderStoneList) {
                ContentValues values = new ContentValues();
                values.put(KEY_ID, stone.getId());
                values.put(KEY_IS_DELETED, stone.isDeleted());
                values.put(KEY_UPDATED_DATE, stone.getUpdateDate().getMillis());
                values.put(KEY_ORDER_ID, stone.getOrderId());
                values.put(KEY_DIAMETER, stone.getDiameter());
                values.put(KEY_COUNT, stone.getCount());
                values.put(KEY_QUALITY_ID, stone.getQualityId());
                db.insert(TABLE_STONE_ORDER, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("saveOrderStones", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    public List<StoneOrderDTO> getOrderStones(Integer orderId) {
        List<StoneOrderDTO> stoneOrderDTOs = new ArrayList<StoneOrderDTO>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_STONE_ORDER + " WHERE " + KEY_ORDER_ID + "=" + orderId + " and " + KEY_IS_DELETED + "=0";

        Log.d(ProductDAOHelper.class + "", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    StoneOrderDTO orderDTO = new StoneOrderDTO();
                    orderDTO.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                    orderDTO.setCount(c.getInt(c.getColumnIndex(KEY_COUNT)));
                    orderDTO.setQualityId(c.getInt(c.getColumnIndex(KEY_QUALITY_ID)));
                    orderDTO.setDiameter(c.getFloat(c.getColumnIndex(KEY_DIAMETER)));
                    stoneOrderDTOs.add(orderDTO);
                } while (c.moveToNext());
                c.close();
            }
        }
        return stoneOrderDTOs;
    }


    public void deleteOrder(Integer id) {

        SQLiteDatabase db = this.getReadableDatabase();
        try {
            db.beginTransaction();
            deleteOrderStone(id);
            db.delete(TABLE_ORDER, KEY_ID + "=" + id, null);
            db.setTransactionSuccessful();

        } catch (Exception e) {
            Log.d("deleteProductTemp", e.getMessage());
        } finally {
            db.endTransaction();
        }

    }

    public void deleteOrderStone(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_STONE_ORDER, KEY_ORDER_ID + "=" + id, null);
    }


    public void saveTempOrder(OrderTemp orderTemp) {
        SQLiteDatabase db = this.getWritableDatabase();
        OrderDTO order = orderTemp.getOrderDTO();
        try {
            db.beginTransaction();

            ContentValues values = new ContentValues();

            values.put(KEY_IS_DELETED, order.isDeleted());
            values.put(KEY_UPDATED_DATE, order.getUpdateDate().getMillis());
            values.put(KEY_PRODUCT_ID, order.getProduct().getId());
            values.put(KEY_ORDER_TYPE, order.getOrderType().toString());
            values.put(KEY_COUNT, order.getCount());
            values.put(KEY_DONE, order.getDone());
            values.put(KEY_USER_ID, order.getUser().getId());
            values.put(KEY_DATE, order.getDate().getMillis());
            values.put(KEY_DESCRIPTION, order.getDescription());
            values.put(KEY_GOLD_TYPE, order.getGoldType().ordinal());
            values.put(KEY_USER_ID, order.getUser().getId());
            values.put(KEY_USER_NAME, order.getUser().getEmail());
            Integer orderId = order.getId();
            values.put(KEY_ORDER_ID, orderId);

            Integer id = orderTemp.getId();
            if (id == null) {
                id = (int) db.insert(TABLE_ORDER_TEMP, null, values);
            } else {
                values.put(KEY_ID, id);
                db.update(TABLE_ORDER_TEMP, values, KEY_ID + "=" + id, null);
            }
            if (order.getStones().size() > 0) {
                saveOrderStonesTemp(order.getStones(), id);
            }
            if (orderId != null) {

                setOrderIsDeleted(orderId, true);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("saveOrderTemp", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    public void deleteOrderTemp(Integer id) {

        SQLiteDatabase db = this.getReadableDatabase();
        try {
            db.beginTransaction();
            deleteOrderTempStone(id);
            db.delete(TABLE_ORDER_TEMP, KEY_ID + "=" + id, null);
            db.setTransactionSuccessful();

        } catch (Exception e) {
            Log.d("deleteProductTemp", e.getMessage());
        } finally {
            db.endTransaction();
        }

    }

    public void deleteOrderTempStone(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_STONE_ORDER_TEMP, KEY_ORDER_ID + "=" + id, null);
    }

    public void deleteAllTempOrder() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_STONE_ORDER_TEMP, null, null);

    }

    protected void saveOrderStonesTemp(List<StoneOrderDTO> stoneList, Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();

        for (StoneOrderDTO stone : stoneList) {
            try {
                ContentValues values = new ContentValues();
                values.put(KEY_ID, stone.getId());
                values.put(KEY_IS_DELETED, stone.isDeleted());
                values.put(KEY_ORDER_ID, id);
                values.put(KEY_DIAMETER, stone.getDiameter());
                values.put(KEY_COUNT, stone.getCount());
                values.put(KEY_QUALITY_ID, stone.getQualityId());
                db.insert(TABLE_STONE_ORDER_TEMP, null, values);
            } catch (Exception e) {
                Log.d("saveOrderStoneTemp", e.getMessage());
            }

        }
    }

    public List<OrderTemp> getTempOrderList() {
        String selectQuery = "SELECT * FROM " + TABLE_ORDER_TEMP;
        SQLiteDatabase db = this.getReadableDatabase();
        List<OrderTemp> orderList = new ArrayList<OrderTemp>();

        Log.d(ProductDAOHelper.class + "", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    OrderTemp orderTemp = new OrderTemp();
                    OrderDTO order = new OrderDTO();
                    if (!c.isNull(c.getColumnIndex(KEY_ORDER_ID))) {
                        order.setId(c.getInt(c.getColumnIndex(KEY_ORDER_ID)));
                    }
                    order.setProduct(getProduct(c.getInt(c.getColumnIndex(KEY_PRODUCT_ID))));
                    order.setOrderType(OrderType.valueOf(c.getString(c.getColumnIndex(KEY_ORDER_TYPE))));
                    UserDTO userDTO = new UserDTO();
                    userDTO.setId(c.getInt(c.getColumnIndex(KEY_USER_ID)));
                    order.setUser(userDTO);
                    order.setDescription(c.getString(c.getColumnIndex(KEY_DESCRIPTION)));
                    order.setGoldType(GoldType.values()[c.getInt(c.getColumnIndex(KEY_GOLD_TYPE))]);
                    order.setCount(c.getInt(c.getColumnIndex(KEY_COUNT)));
                    order.setDone(c.getInt(c.getColumnIndex(KEY_DONE)));
                    order.setDate(new DateTime(c.getLong(c.getColumnIndex(KEY_DATE))));
                    userDTO.setEmail(c.getString(c.getColumnIndex(KEY_USER_NAME)));
                    order.setStones(getOrderTempStones(order.getId()));
                    orderTemp.setOrderDTO(order);
                    orderTemp.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                    orderList.add(orderTemp);
                } while (c.moveToNext());

            }
            c.close();
        }
        return orderList;
    }

    protected List<StoneOrderDTO> getOrderTempStones(Integer orderId) {
        List<StoneOrderDTO> stoneOrderDTOs = new ArrayList<StoneOrderDTO>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_STONE_ORDER_TEMP + " WHERE " + KEY_ORDER_ID + "=" + orderId + " and " + KEY_IS_DELETED + "=0";

        Log.d(ProductDAOHelper.class + "", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    StoneOrderDTO orderDTO = new StoneOrderDTO();
                    orderDTO.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                    orderDTO.setCount(c.getInt(c.getColumnIndex(KEY_COUNT)));
                    orderDTO.setQualityId(c.getInt(c.getColumnIndex(KEY_QUALITY_ID)));
                    orderDTO.setDiameter(c.getFloat(c.getColumnIndex(KEY_DIAMETER)));
                    stoneOrderDTOs.add(orderDTO);

                } while (c.moveToNext());
                c.close();
            }
        }
        return stoneOrderDTOs;

    }

    //Product Type
    public DateTime getProductTypeLastUpdateDate() {
        return getLastUpdatedDate(TABLE_PRODUCT_TYPE);
    }


    public void saveTypeList(List<ProductTypeDTO> typeList) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (ProductTypeDTO type : typeList) {
                ContentValues values = new ContentValues();
                values.put(KEY_ID, type.getId());
                values.put(KEY_IS_DELETED, type.isDeleted());
                values.put(KEY_UPDATED_DATE, type.getUpdateDate().getMillis());
                values.put(KEY_NAME, type.getName());
                db.insert(TABLE_PRODUCT_TYPE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("saveTypeList", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    //Stone
    public DateTime getStonePriceLastUpdateDate() {
        return getLastUpdatedDate(TABLE_STONE_PRICE);
    }

    public DateTime getStoneDiameterLastUpdateDate() {
        return getLastUpdatedDate(TABLE_STONE_DIAMETER);
    }


    public DateTime getStoneQualityLastUpdateDate() {
        return getLastUpdatedDate(TABLE_STONE_QUALITY);
    }

    public DiamondWeightDTO getDiamondWeight(Float diameter) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_DIAMOND_WEIGHT + " WHERE " + KEY_DIAMETER + "=" + diameter;
        Log.d(ProductDAOHelper.class + "", selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        DiamondWeightDTO diamondWeightDTO = null;
        if (c != null) {
            if (c.moveToFirst()) {
                diamondWeightDTO = new DiamondWeightDTO();
                diamondWeightDTO.setMax(c.getFloat(c.getColumnIndex(KEY_MAX)));
                diamondWeightDTO.setMin(c.getFloat(c.getColumnIndex(KEY_MIN)));

            }
            c.close();
        }
        return diamondWeightDTO;
    }


    public void saveDiamondWeightList(List<DiamondWeightDTO> weightList) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (DiamondWeightDTO weight : weightList) {
                ContentValues values = new ContentValues();
                values.put(KEY_ID, weight.getId());
                values.put(KEY_IS_DELETED, weight.isDeleted());
                values.put(KEY_UPDATED_DATE, weight.getUpdateDate().getMillis());
                values.put(KEY_DIAMETER, weight.getDiameter().toString());
                values.put(KEY_MAX, weight.getMax());
                values.put(KEY_MIN, weight.getMin());
                db.insert(TABLE_DIAMOND_WEIGHT, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("saveDiamondList", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    public StonePriceDTO getPrice(Integer stoneId, Integer diameterId, Integer qualityId) {

        SQLiteDatabase db = this.getReadableDatabase();
        StonePriceDTO stonePriceDTO = new StonePriceDTO();
        stonePriceDTO.setQualityID(qualityId);
        stonePriceDTO.setDiameterID(diameterId);
        stonePriceDTO.setStoneID(stoneId);

        String selectQuery = "SELECT  * FROM " + TABLE_STONE_PRICE + " WHERE " + KEY_STONE_ID + "=" + stoneId
                + " AND " + KEY_QUALITY_ID + "=" + qualityId + " AND " + KEY_DIAMETER_ID + "=" + diameterId;
        Log.d(ProductDAOHelper.class + "", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            if (c.moveToFirst()) {
                stonePriceDTO.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                stonePriceDTO.setCost(c.getFloat(c.getColumnIndex(KEY_COST)));
            } else {
                stonePriceDTO.setCost(0f);
            }
            c.close();
        }
        return stonePriceDTO;

    }

    public void savePriceList(List<StonePriceDTO> priceList) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();

            for (StonePriceDTO stonePrice : priceList) {
                ContentValues values = new ContentValues();
                values.put(KEY_ID, stonePrice.getId());
                values.put(KEY_IS_DELETED, stonePrice.isDeleted());
                values.put(KEY_UPDATED_DATE, stonePrice.getUpdateDate().getMillis());
                values.put(KEY_COST, stonePrice.getCost());
                values.put(KEY_DIAMETER_ID, stonePrice.getDiameterID());
                values.put(KEY_QUALITY_ID, stonePrice.getQualityID());
                values.put(KEY_STONE_ID, stonePrice.getStoneID());
                db.insertWithOnConflict(TABLE_STONE_PRICE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("savePriceList", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    public List<StoneQualityDTO> getQualityByType(int stoneId) {

        SQLiteDatabase db = this.getReadableDatabase();
        List<StoneQualityDTO> qualityList = new ArrayList<StoneQualityDTO>();
        String selectQuery = "SELECT *  FROM " + TABLE_STONE_QUALITY + " WHERE "
                + KEY_STONE_ID + "=" + stoneId + " AND " + KEY_IS_DELETED + "=0";
        Log.d(ProductDAOHelper.class + "", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    StoneQualityDTO stoneQuality = new StoneQualityDTO();
                    stoneQuality.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                    stoneQuality.setQuality(c.getString(c.getColumnIndex(KEY_QUALITY)));
                    stoneQuality.setStoneId(c.getInt(c.getColumnIndex(KEY_STONE_ID)));
                    qualityList.add(stoneQuality);

                } while (c.moveToNext());
            }
            c.close();
        }
        return qualityList;
    }

    public List<StoneDiameterDTO> getStoneDiameterList(Integer stoneId) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<StoneDiameterDTO> diameterList = new ArrayList<StoneDiameterDTO>();
        String selectQuery = "SELECT  * FROM " + TABLE_STONE_DIAMETER + " WHERE " + KEY_STONE_ID + "=" + stoneId
                + " AND " + KEY_IS_DELETED + "=0";
        Log.d(ProductDAOHelper.class + "", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    StoneDiameterDTO diameter = new StoneDiameterDTO();
                    diameter.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                    diameter.setStoneId(c.getColumnIndex(KEY_STONE_ID));
                    diameter.setStart(c.getFloat(c.getColumnIndex(KEY_START)));
                    diameter.setEnd(c.getFloat(c.getColumnIndex(KEY_END)));
                    diameterList.add(diameter);

                } while (c.moveToNext());
            }
            c.close();
        }
        return diameterList;
    }


    public Integer getStoneDiameterIdByDiameter(Float diameter, Integer stoneId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Integer diameterId = null;
        String selectQuery = "SELECT * FROM " + TABLE_STONE_DIAMETER + " WHERE ((" + KEY_START + "=" + diameter + ")OR("
                + KEY_START + "<" + diameter + " AND " + KEY_END + ">" + diameter + ")OR(" + KEY_END + " = " + diameter + "))AND "
                + KEY_STONE_ID + "=" + stoneId + " AND " + KEY_IS_DELETED + "=0";
        Log.d(ProductDAOHelper.class + "", selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            if (c.moveToFirst()) {
                diameterId = c.getInt(c.getColumnIndex(KEY_ID));
            }
            c.close();
        }
        return diameterId;
    }

    public void saveDiameterList(List<StoneDiameterDTO> diameterList) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (StoneDiameterDTO stoneDiameter : diameterList) {
                ContentValues values = new ContentValues();
                values.put(KEY_ID, stoneDiameter.getId());
                values.put(KEY_IS_DELETED, stoneDiameter.isDeleted());
                values.put(KEY_UPDATED_DATE, stoneDiameter.getUpdateDate() != null ? stoneDiameter.getUpdateDate().getMillis() : null);
                values.put(KEY_START, stoneDiameter.getStart() + "");
                values.put(KEY_END, stoneDiameter.getEnd()!= null ? stoneDiameter.getEnd()+ "":"0.0");
                values.put(KEY_STONE_ID, stoneDiameter.getStoneId());
                db.insert(TABLE_STONE_DIAMETER, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("saveDiameterList", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    public void saveQualityList(List<StoneQualityDTO> qualityList) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (StoneQualityDTO quality : qualityList) {
                ContentValues values = new ContentValues();
                values.put(KEY_ID, quality.getId());
                values.put(KEY_IS_DELETED, quality.isDeleted());
                values.put(KEY_UPDATED_DATE, quality.getUpdateDate().getMillis());
                values.put(KEY_STONE_ID, quality.getStoneId());
                values.put(KEY_QUALITY, quality.getQuality());
                db.insert(TABLE_STONE_QUALITY, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("saveQualityList", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    public StoneQualityDTO getQualityByID(int qualityID) {
        StoneQualityDTO quality = new StoneQualityDTO();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_STONE_QUALITY + " WHERE " + KEY_ID + " = " + qualityID;
        Log.d(ProductDAOHelper.class + "", selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            if (c.moveToFirst()) {
                try {
                    db.beginTransaction();
                    quality.setId(qualityID);
                    quality.setQuality(c.getString(c.getColumnIndex(KEY_QUALITY)));
                    quality.setStoneId(c.getInt(c.getColumnIndex(KEY_STONE_ID)));
                    db.setTransactionSuccessful();

                } catch (Exception e) {
                    Log.d("getQuality", e.getMessage());
                } finally {
                    db.endTransaction();
                }
            }
            c.close();
        }
        return quality;
    }

    public boolean checkDiameterExist(Float diameter, Integer stoneId) {
        String selectQuery = "SELECT * FROM " + TABLE_STONE_DIAMETER + " WHERE ((" + KEY_START + "=" + diameter + ")OR("
                + KEY_START + "<" + diameter + " AND " + KEY_END + ">" + diameter + ")OR(" + KEY_END + " = " + diameter + "))AND "
                + KEY_STONE_ID + "=" + stoneId + " AND " + KEY_IS_DELETED + "=0";
        return checkExist(selectQuery);
    }

    public boolean checkDiameterRange(Float diameterStart, Float diameterEnd, Integer stoneId) {
        String selectQuery = "SELECT * FROM " + TABLE_STONE_DIAMETER + " WHERE (" + KEY_START + ">" + diameterStart + ")AND("
                + KEY_END + "<" + diameterEnd + ")AND "
                + KEY_STONE_ID + "=" + stoneId + " AND " + KEY_IS_DELETED + "=0";
        return checkExist(selectQuery);
    }

    public void addStoneDiameter(StoneDiameterDTO diameter) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_IS_DELETED, diameter.isDeleted());
            values.put(KEY_UPDATED_DATE, diameter.getUpdateDate().getMillis());
            values.put(KEY_START, diameter.getStart().toString());
            values.put(KEY_STONE_ID, diameter.getStoneId());
            values.put(KEY_END, diameter.getEnd()!= null ? diameter.getEnd().toString():"0.0");
            values.put(KEY_ID, diameter.getId());
            db.insert(TABLE_STONE_DIAMETER, null, values);
        } catch (Exception e) {
            Log.d("saveDiameter", e.getMessage());
        }
    }

    public boolean checkQualityExist(String quality, Integer stoneId) {
        String selectQuery = "SELECT * FROM " + TABLE_STONE_QUALITY + " WHERE " + KEY_QUALITY + " = '" + quality + "' AND "
                + KEY_STONE_ID + "=" + stoneId + " AND " + KEY_IS_DELETED + "=0";
        return checkExist(selectQuery);
    }

    private boolean checkExist(String selectQuery) {
        SQLiteDatabase db = this.getReadableDatabase();
        boolean result = false;
        Log.d(ProductDAOHelper.class + "", selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null) {
            if (c.moveToFirst()) {
                result = true;
            }
        }
        if (c != null) {
            c.close();
        }
        return result;
    }

    public void deleteStoneQuality(StoneQualityDTO qualityDTO) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_UPDATED_DATE, new DateTime().getMillis());
            values.put(KEY_IS_DELETED, 1);
            db.update(TABLE_STONE_QUALITY, values, KEY_ID + "=" + qualityDTO.getId(), null);
        } catch (Exception e) {
            Log.d("ProductDAOHelper", e.getMessage());
        }
    }

    public void deleteStoneDiameter(StoneDiameterDTO diameterDTO) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_UPDATED_DATE, new DateTime().getMillis());
            values.put(KEY_IS_DELETED, 1);
            db.update(TABLE_STONE_DIAMETER, values, KEY_ID + "=" + diameterDTO.getId(), null);
        } catch (Exception e) {
            Log.d("ProductDAOHelper", e.getMessage());
        }
    }

    public void addStoneQuality(StoneQualityDTO qualityDTO) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_IS_DELETED, qualityDTO.isDeleted());
            values.put(KEY_UPDATED_DATE, qualityDTO.getUpdateDate().getMillis());
            values.put(KEY_STONE_ID, qualityDTO.getStoneId());
            values.put(KEY_QUALITY, qualityDTO.getQuality());
            values.put(KEY_ID, qualityDTO.getId());
            db.insert(TABLE_STONE_QUALITY, null, values);
        } catch (Exception e) {
            Log.d("saveQuality", e.getMessage());
        }
    }

    public void deleteCatalog(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_CATALOG, KEY_ID + "=" + id, null);
    }

    public void saveCatalogs(List<CatalogDTO> catalogDTOs) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (CatalogDTO catalogDTO : catalogDTOs) {
                ContentValues values = new ContentValues();
                values.put(KEY_ID, catalogDTO.getId());
                values.put(KEY_IS_DELETED, catalogDTO.isDeleted());
                values.put(KEY_UPDATED_DATE, catalogDTO.getUpdateDate().getMillis());
                values.put(KEY_NAME, catalogDTO.getName());
                db.insertWithOnConflict(TABLE_CATALOG, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("saveOrderStoneTemp", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    public List<CatalogDTO> getCatalogs() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<CatalogDTO> qualityList = new ArrayList<CatalogDTO>();
        String selectQuery = "SELECT *  FROM " + TABLE_CATALOG + " WHERE " + KEY_IS_DELETED + "=0";
        Log.d(ProductDAOHelper.class + "", selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    CatalogDTO stoneQuality = new CatalogDTO();
                    stoneQuality.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                    stoneQuality.setUpdateDate(new DateTime(c.getLong(c.getColumnIndex(KEY_UPDATED_DATE))));
                    stoneQuality.setDeleted(c.getInt(c.getColumnIndex(KEY_IS_DELETED)) == 1);
                    stoneQuality.setName(c.getString(c.getColumnIndex(KEY_NAME)));
                    qualityList.add(stoneQuality);
                } while (c.moveToNext());
            }
            c.close();
        }
        return qualityList;
    }

    public CatalogDTO getCatalogById(Integer id) {
        SQLiteDatabase db = this.getReadableDatabase();
        CatalogDTO catalogDTO = null;
        String selectQuery = "SELECT *  FROM " + TABLE_CATALOG + " WHERE " + KEY_IS_DELETED + "=0 AND " + KEY_ID + " =" + id;
        Log.d(ProductDAOHelper.class + "", selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null) {
            if (c.moveToFirst()) {
                catalogDTO = new CatalogDTO();
                catalogDTO.setId(c.getInt(c.getColumnIndex(KEY_ID)));
                catalogDTO.setUpdateDate(new DateTime(c.getLong(c.getColumnIndex(KEY_UPDATED_DATE))));
                catalogDTO.setDeleted(c.getInt(c.getColumnIndex(KEY_IS_DELETED)) == 1);
                catalogDTO.setName(c.getString(c.getColumnIndex(KEY_NAME)));
            }
            c.close();
        }
        return catalogDTO;
    }

    public void saveProductCatalogs(List<ProductCatalogDTO> productCatalogDTOs) {
        saveProductCatalogs(productCatalogDTOs, TABLE_PRODUCT_CATALOG, null);
    }

    public void saveTempProductCatalogs(List<ProductCatalogDTO> productCatalogDTOs, Integer productId) {
        if (productId != null) {
            deleteTempProductCatalogs(productId);
        }
        saveProductCatalogs(productCatalogDTOs, TABLE_TEMP_PRODUCT_CATALOG, productId);
    }

    private void saveProductCatalogs(List<ProductCatalogDTO> productCatalogDTOs, String table, Integer productId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (ProductCatalogDTO productCatalogDTO : productCatalogDTOs) {
                ContentValues values = new ContentValues();
                values.put(KEY_ID, productCatalogDTO.getId());
                values.put(KEY_IS_DELETED, productCatalogDTO.isDeleted());
                if (productCatalogDTO.getUpdateDate() != null) {
                    values.put(KEY_UPDATED_DATE, productCatalogDTO.getUpdateDate().getMillis());
                }
                values.put(KEY_LABEL, productCatalogDTO.getProductLabel());
                if (productId != null) {
                    values.put(KEY_PRODUCT_ID, productId);
                } else {
                    values.put(KEY_PRODUCT_ID, productCatalogDTO.getProduct().getId());
                }
                values.put(KEY_CATALOG_ID, productCatalogDTO.getCatalog().getId());
                db.insertWithOnConflict(table, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("saveOrderStoneTemp", e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    public List<ProductCatalogDTO> getTempProductCatalogs(Integer tempProductId, Integer originalProductId) {
        String selectQuery = "SELECT *  FROM " + TABLE_TEMP_PRODUCT_CATALOG + " WHERE " + KEY_PRODUCT_ID + "=" + tempProductId;
        return getProductCatalogs(selectQuery, originalProductId);
    }

    public List<ProductCatalogDTO> getProductCatalogs(Integer productId) {
        String selectQuery = "SELECT *  FROM " + TABLE_PRODUCT_CATALOG + " WHERE " + KEY_IS_DELETED + "=0 AND " + KEY_PRODUCT_ID + "=" + productId;
        return getProductCatalogs(selectQuery, null);
    }

    private List<ProductCatalogDTO> getProductCatalogs(String selectQuery, Integer originalProductId) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<ProductCatalogDTO> productCatalogDTOs = new ArrayList<ProductCatalogDTO>();
        Log.d(ProductDAOHelper.class + "", selectQuery);
        Cursor c = null;
        if (db != null) {
            c = db.rawQuery(selectQuery, null);
        }
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    ProductCatalogDTO productCatalogDTO = new ProductCatalogDTO();
                    productCatalogDTO.setUpdateDate(new DateTime(c.getLong(c.getColumnIndex(KEY_UPDATED_DATE))));
                    productCatalogDTO.setDeleted(c.getInt(c.getColumnIndex(KEY_IS_DELETED)) == 1);
                    productCatalogDTO.setProductLabel(c.getString(c.getColumnIndex(KEY_LABEL)));
                    ProductDTO productDTO = new ProductDTO();
                    if (originalProductId == null) {
                        productDTO.setId(c.getInt(c.getColumnIndex(KEY_PRODUCT_ID)));
                    } else {
                        productDTO.setId(originalProductId);
                    }
                    productCatalogDTO.setProduct(productDTO);
                    CatalogDTO catalog = getCatalogById(c.getInt(c.getColumnIndex(KEY_CATALOG_ID)));
                    if(catalog!=null){
                        productCatalogDTO.setCatalog(catalog);
                        productCatalogDTOs.add(productCatalogDTO);
                    }
                } while (c.moveToNext());
            }
            c.close();
        }
        return productCatalogDTOs;
    }

}
