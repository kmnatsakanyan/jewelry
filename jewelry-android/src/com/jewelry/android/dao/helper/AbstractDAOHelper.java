package com.jewelry.android.dao.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import org.joda.time.DateTime;

/**
 * User: karen.mnatsakanyan
 */
public abstract class AbstractDAOHelper extends SQLiteOpenHelper {

    protected static final String KEY_ID = "ID";
    protected static final String KEY_UPDATED_DATE = "UPDATE_DATE";
    protected static final String KEY_IS_DELETED = "IS_DELETED";

    public AbstractDAOHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public AbstractDAOHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    public DateTime getLastUpdatedDate(String tableName) {
        DateTime uptDate = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT MAX(" + KEY_UPDATED_DATE + ") as " + KEY_UPDATED_DATE + " FROM " + tableName;
        Log.d(ProductDAOHelper.class + "", selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null) {
            if (c.moveToFirst()) {
                uptDate = new DateTime(c.getLong(c.getColumnIndex(KEY_UPDATED_DATE)));
            }
        }
        return uptDate;
    }
}
