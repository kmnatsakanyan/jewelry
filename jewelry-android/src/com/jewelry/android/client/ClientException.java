package com.jewelry.android.client;

/**
 * Exception used during client initialization
 * User: hma
 * Date: 2/24/13
 */
public class ClientException extends RuntimeException {
    public ClientException(String msg, Throwable cause) {
        super(msg, cause);
    }
    public ClientException(String msg) {
        super(msg);
    }
}
