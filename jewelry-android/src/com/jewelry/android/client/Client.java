package com.jewelry.android.client;

import com.jewelry.android.app.AppConfig;
import com.jewelry.dto.UserDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.exception.ServiceUnavailableException;
import com.jewelry.rs.*;
import com.jewelry.util.JewelryConstant;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.ProxyFactory;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Proxying client class for all services and authorization issues.
 *
 * @author hma
 *         Date: 2/17/13
 */
public class Client {
    private static transient Client INSTANCE;
    private AppConfig cfg;
    private UserDTO loggedInUser;

    private GoldResource goldResource;
    private OrderResource orderResource;
    private PhotoResource photoResource;
    private ProductResource productResource;
    private StoneDiameterResource stoneDiameterResource;
    private StonePriceResource stonePriceResource;
    private TypeResource typeResource;
    private UserResource userResource;
    private StoneQualityResource stoneQualityResource;
    private OrderTypePriceResource orderTypePriceResource;
    private DiamondWeightResource diamondWeightResource;
    protected CatalogResource catalogResource;
    protected ProductCatalogResource productCatalogResource;

    private static final int HTTP_TIMEOUT = 30000;

    private static final String USER_RESOURCE_PATH = "user";
    private static final String GOLD_RESOURCE_PATH = "gold";
    private static final String PRODUCT_RESOURCE_PATH = "product";
    private static final String PHOTO_RESOURCE_PATH = "photo";
    private static final String TYPE_RESOURCE_PATH = "type";
    private static final String STONE_DIAMETER_RESOURCE_PATH = "stoneDiameter";
    private static final String STONE_PRICE_RESOURCE_PATH = "stonePrice";
    private static final String STONE_QUALITY_RESOURCE_PATH = "stoneQuality";
    private static final String ORDER_RESOURCE_PATH = "order";
    private static final String ORDER_TYPE_PRICE_PATH = "orderTypePrice";
    private static final String DIAMOND_WEIGHT_PATH = "diamondWeight";
    private static final String CATALOG = "catalog";
    private static final String PRODUCT_CATALOG = "productCatalog";


    private static final String ERROR_MSG_URI = "Wrong serviceUrl provided for initialization";

    /**
     * Constructs Client for all services and holds them together in centralized place
     *
     * @param cfg application config, used to get service URL, client id and client secret
     * @author hma
     */
    private Client(AppConfig cfg, UserDTO loggedInUser) throws ClientException {
        try {
            this.cfg = cfg;
            this.loggedInUser = loggedInUser;
            initProxies();
        } catch (URISyntaxException urie) {
            throw new ClientException(ERROR_MSG_URI, urie);
        }
    }


    /**
     * Initializes all services proxies
     */
    private void initProxies() throws URISyntaxException {
        goldResource = initServiceProxy(GoldResource.class, GOLD_RESOURCE_PATH);
        orderResource = initServiceProxy(OrderResource.class, ORDER_RESOURCE_PATH);
        photoResource = initServiceProxy(PhotoResource.class, PHOTO_RESOURCE_PATH);
        productResource = initServiceProxy(ProductResource.class, PRODUCT_RESOURCE_PATH);
        stoneDiameterResource = initServiceProxy(StoneDiameterResource.class, STONE_DIAMETER_RESOURCE_PATH);
        stonePriceResource = initServiceProxy(StonePriceResource.class, STONE_PRICE_RESOURCE_PATH);
        stoneQualityResource = initServiceProxy(StoneQualityResource.class, STONE_QUALITY_RESOURCE_PATH);
        typeResource = initServiceProxy(TypeResource.class, TYPE_RESOURCE_PATH);
        userResource = initServiceProxy(UserResource.class, USER_RESOURCE_PATH);
        orderTypePriceResource = initServiceProxy(OrderTypePriceResource.class, ORDER_TYPE_PRICE_PATH);
        diamondWeightResource = initServiceProxy(DiamondWeightResource.class, DIAMOND_WEIGHT_PATH);
        catalogResource = initServiceProxy(CatalogResource.class, CATALOG);
        productCatalogResource = initServiceProxy(ProductCatalogResource.class, PRODUCT_CATALOG);

    }

    /**
     * Constructs single service's proxy implementation.
     *
     * @param clazz
     * @param path
     * @param <T>
     * @return T Proxy implementation
     */
    private <T> T initServiceProxy(Class<T> clazz, String path) throws URISyntaxException {
        RegisterBuiltin.register(ResteasyProviderFactory.getInstance());
        URI serviceURI = new URI(cfg.getServicesUrl() + path);
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        DefaultHttpClient httpClient = new DefaultHttpClient();              // dummy connection, to get defaults.
        ClientConnectionManager connMgr = httpClient.getConnectionManager();
        HttpParams httpParams = httpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, HTTP_TIMEOUT);
        HttpConnectionParams.setSoTimeout(httpParams, HTTP_TIMEOUT);

        HttpParams params = httpClient.getParams();

        ConnPerRouteBean connPerRoute = new ConnPerRouteBean(5);
        ConnManagerParams.setMaxConnectionsPerRoute(params, connPerRoute);

        connMgr = new ThreadSafeClientConnManager(params, connMgr.getSchemeRegistry());
        httpClient = new DefaultHttpClient(connMgr, params);
        if (loggedInUser != null) {
            Credentials credentials = new UsernamePasswordCredentials(loggedInUser.getEmail(), loggedInUser.getPassword());
            httpClient.getCredentialsProvider().setCredentials(org.apache.http.auth.AuthScope.ANY, credentials);
        }
        ClientExecutor executor = new ApacheHttpClient4Executor(httpClient) {
            @Override
            public ClientResponse execute(ClientRequest request) throws Exception {
                ClientResponse clientResponse;
                try {
                    clientResponse = super.execute(request);
                    if (clientResponse.getStatus() != JewelryConstant.RESPONSE_OK) {
                        if (clientResponse.getStatus() == JewelryConstant.RESPONSE_ERROR) {
                            try {
                                throw new ServiceUnavailableException(((ValueDTO)clientResponse.getEntity(ValueDTO.class)).getResultCode());
                            } catch (Throwable throwable) {
                                if(throwable instanceof ServiceUnavailableException){
                                    throw (Exception)throwable;
                                }
                                throw new ServiceUnavailableException(JewelryConstant.RESPONSE_ERROR);
                            }
                        } else {
                            throw new ServiceUnavailableException(JewelryConstant.RESPONSE_ERROR);
                        }
                    }
                } catch (ConnectTimeoutException ex) {
                    throw new ServiceUnavailableException(JewelryConstant.RESPONSE_TIMEOUT);
                }
                return clientResponse;
            }
        };

        ResteasyProviderFactory providerFactory = ResteasyProviderFactory.getInstance();
        return ProxyFactory.create(clazz, serviceURI, executor, providerFactory);
    }

    /**
     * Gets the instance of client, accessible from everywhere
     *
     * @param cfg
     * @return Client the client's only instance
     */
    public static Client configure(AppConfig cfg, UserDTO loggedInUser) {
        if (INSTANCE == null) {
            INSTANCE = new Client(cfg, loggedInUser);
        }

        return INSTANCE;
    }

    public static Client reconfigure(UserDTO loggedInUser) {
        if (INSTANCE != null) {
            try {
                INSTANCE.loggedInUser = loggedInUser;
                INSTANCE.initProxies();
            } catch (URISyntaxException e) {
                throw new ClientException(ERROR_MSG_URI, e);
            }
        }
        return INSTANCE;
    }

    public static Client getInstance() {
        return INSTANCE;
    }


    public UserResource getUserResource() {
        return userResource;
    }

    public GoldResource getGoldResource() {
        return goldResource;
    }

    public PhotoResource getPhotoResource() {
        return photoResource;
    }

    public ProductResource getProductResource() {
        return productResource;
    }

    public StoneDiameterResource getStoneDiameterResource() {
        return stoneDiameterResource;
    }

    public StoneQualityResource getStoneQualityResource() {
        return stoneQualityResource;
    }

    public TypeResource getTypeResource() {
        return typeResource;
    }

    public OrderResource getOrderResource() {
        return orderResource;
    }

    public StonePriceResource getStonePriceResource() {
        return stonePriceResource;
    }

    public OrderTypePriceResource getOrderTypePriceResource() {
        return orderTypePriceResource;
    }

    public DiamondWeightResource getDiamondWeightResource() {
        return diamondWeightResource;
    }

    public CatalogResource getCatalogResource() {
        return catalogResource;
    }

    public ProductCatalogResource getProductCatalogResource() {
        return productCatalogResource;
    }
}