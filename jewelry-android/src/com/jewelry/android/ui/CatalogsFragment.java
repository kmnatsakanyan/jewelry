package com.jewelry.android.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.task.AsyncTaskHandler;
import com.jewelry.android.task.DeleteCatalogAsyncTask;
import com.jewelry.android.task.GenericAsyncTaskHandler;
import com.jewelry.android.task.GetCatalogsAsyncTask;
import com.jewelry.dto.CatalogDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.util.JewelryConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class CatalogsFragment extends BaseFragment {
    ListView mListView;
    CatalogListAdapter catalogListAdapter;
    private static final int DELETE_CATALOG_MENU_ITEM = 0;
    ProductDAOHelper dao = JewelryApplication.DAO;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        mListView = (ListView) rootView.findViewById(R.id.list);
        catalogListAdapter = new CatalogListAdapter(JewelryApplication.instance());
        mListView.setAdapter(catalogListAdapter);
        registerForContextMenu(mListView);
        initProgressBar(rootView);
        fetchCatalogList();
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.list) {
            menu.add(0, DELETE_CATALOG_MENU_ITEM, 0, getResources().getString(R.string.menu_delete));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case DELETE_CATALOG_MENU_ITEM:
                AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                TextView tx = (TextView) acmi.targetView;
                final Integer catalogId = (Integer) tx.getTag();
                AsyncTaskHandler<ValueDTO<Boolean>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<Boolean>>() {
                    @Override
                    public void handle(ValueDTO<Boolean> result) {
                        if (result.getValue()) {
                            catalogListAdapter.getCatalogs().clear();
                            dao.deleteCatalog(catalogId);
                            catalogListAdapter.notifyDataSetChanged();
                            fetchCatalogList();
                        }

                    }
                    @Override
                    public void handleError(ValueDTO<Boolean> result) {
                        switch (result.getResultCode()) {
                            case JewelryConstant.RESPONSE_CATALOG_NOT_FOUND:
                                Toast.makeText(JewelryApplication.instance(), getResources().getString(R.string.error_catalog_not_found), Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                super.handleError(result);
                        }
                    }
                };
                DeleteCatalogAsyncTask deleteCatalogAsyncTask = new DeleteCatalogAsyncTask(asyncTaskHandler, catalogId);
                deleteCatalogAsyncTask.execute();
                return true;
            default:
                return super.onContextItemSelected(item);
        }


    }

    private void fetchCatalogList() {
        showProgress();
        GetCatalogsAsyncTask getCatalogsAsyncTask = new GetCatalogsAsyncTask(new GenericAsyncTaskHandler<ValueDTO<List<CatalogDTO>>>() {
            @Override
            public void handleError(ValueDTO<List<CatalogDTO>> result) {
                hideProgress();
                super.handleError(result);
            }

            @Override
            public void handle(ValueDTO<List<CatalogDTO>> result) {
                hideProgress();
                dao.saveCatalogs(result.getValue());
                catalogListAdapter.setCatalogs(result.getValue());
                catalogListAdapter.notifyDataSetChanged();
            }
        });
        getCatalogsAsyncTask.execute();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem item = menu.add(Menu.NONE, R.id.action_add, Menu.NONE, R.string.menu_action_add_product).setIcon(android.R.drawable.ic_menu_add);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent it = new Intent(getActivity(), AddCatalogActivity.class);
                startActivityForResult(it, 1);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {

            if (resultCode == Activity.RESULT_OK) {
                fetchCatalogList();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    class CatalogListAdapter extends BaseAdapter {
        List<CatalogDTO> catalogDTOs = new ArrayList<CatalogDTO>();
        Context mContext;

        public CatalogListAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return catalogDTOs.size();
        }

        @Override
        public Object getItem(int i) {
            return catalogDTOs.size() > i ? catalogDTOs.get(i) : null;
        }

        @Override
        public long getItemId(int i) {
            return catalogDTOs.get(i).getId();
        }

        public List<CatalogDTO> getCatalogs() {
            return catalogDTOs;
        }

        public void setCatalogs(List<CatalogDTO> catalogs) {
            this.catalogDTOs = catalogs;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater mInflater = (LayoutInflater)
                        mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = mInflater.inflate(android.R.layout.simple_list_item_1, null);
            }

            TextView name = (TextView) convertView.findViewById(android.R.id.text1);
            CatalogDTO rowItem = (CatalogDTO) getItem(position);
            name.setText(rowItem.getName());
            name.setTag(rowItem.getId());
            return convertView;
        }
    }
}
