package com.jewelry.android.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.task.AsyncTaskHandler;
import com.jewelry.android.task.GenericAsyncTaskHandler;
import com.jewelry.android.task.UpdateGoldAsyncTask;
import com.jewelry.android.task.UpdateOrderTypePriceAsyncTask;
import com.jewelry.dto.OrderType;
import com.jewelry.dto.ValueDTO;
import com.jewelry.util.JewelryConstant;


/**
 * User: mos;
 */
public class GoldFragment extends BaseFragment {
    EditText mCostText;
    Double goldCost;
    Button saveBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        goldCost = JewelryApplication.instance().getGoldCost();
        View rootView = inflater.inflate(R.layout.fragment_gold, container, false);
        mCostText = (EditText) rootView.findViewById(R.id.gold_cost);
        mCostText.setText(goldCost.toString());
        saveBtn = (Button) rootView.findViewById(R.id.save);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mCostText.getText().toString().isEmpty()) {
                    Double cost = Double.parseDouble(mCostText.getText().toString());
                    if (!cost.equals(goldCost)) {
                        JewelryApplication.instance().setGoldCost(cost);
                        if (JewelryApplication.instance().isLoggedInUserAdmin()) {
                            AsyncTaskHandler<ValueDTO<Boolean>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<Boolean>>() {
                                @Override
                                public void handle(ValueDTO<Boolean> result) {
                                    hideProgress();
                                }

                                @Override
                                public void handleError(ValueDTO<Boolean> result) {
                                    hideProgress();
                                    switch (result.getResultCode()) {
                                        case JewelryConstant.RESPONSE_UNAUTHORIZED:
                                            Toast.makeText(JewelryApplication.instance(), getResources().getString(R.string.error_unauthorized_user), Toast.LENGTH_SHORT).show();
                                            break;
                                        default:
                                            super.handleError(result);
                                    }
                                }
                            };
                            UpdateGoldAsyncTask updateGoldAsyncTask = new UpdateGoldAsyncTask(asyncTaskHandler, cost);
                            updateGoldAsyncTask.execute();
                            showProgress();
                        }
                    }
                }else {
                    mCostText.setError(getString(R.string.error_empty));
                }
            }
        });
        if (JewelryApplication.instance().isLoggedInUserAdmin()) {
            onCreateAdminView(rootView);
        }
        initProgressBar(rootView);
        return rootView;
    }

    private void onCreateAdminView(View rootView) {

        final Double retailCost = JewelryApplication.instance().getRetail();
        final Double wholesaleCost = JewelryApplication.instance().getWholesale();
        final Double shopCost = JewelryApplication.instance().getShop();

        final EditText retailText = (EditText) rootView.findViewById(R.id.retail_cost);
        final EditText wholesaleText = (EditText) rootView.findViewById(R.id.wholesale_cost);
        final EditText shopText = (EditText) rootView.findViewById(R.id.shop_cost);
        rootView.findViewById(R.id.retail_cost_text).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.wholesale_cost_text).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.shop_cost_text).setVisibility(View.VISIBLE);

        retailText.setText(retailCost.toString());
        retailText.setVisibility(View.VISIBLE);
        wholesaleText.setText(wholesaleCost.toString());
        wholesaleText.setVisibility(View.VISIBLE);
        shopText.setText(shopCost.toString());
        shopText.setVisibility(View.VISIBLE);

        Button saveRetail = (Button) rootView.findViewById(R.id.save_retail);
        saveRetail.setVisibility(View.VISIBLE);
        Button saveWholesale = (Button) rootView.findViewById(R.id.save_wholesale);
        saveWholesale.setVisibility(View.VISIBLE);
        Button saveShop = (Button) rootView.findViewById(R.id.save_shop);
        saveShop.setVisibility(View.VISIBLE);

        final AsyncTaskHandler<ValueDTO<Boolean>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<Boolean>>() {
            @Override
            public void handle(ValueDTO<Boolean> result) {
                hideProgress();
            }

            @Override
            public void handleError(ValueDTO<Boolean> result) {
                hideProgress();
                switch (result.getResultCode()) {
                    case JewelryConstant.RESPONSE_UNAUTHORIZED:
                        Toast.makeText(JewelryApplication.instance(), getResources().getString(R.string.error_unauthorized_user), Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        super.handleError(result);
                }
            }
        };

        saveRetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!retailText.getText().toString().isEmpty()) {
                    Double cost = Double.parseDouble(retailText.getText().toString());
                    if (!cost.equals(retailCost)) {
                        JewelryApplication.instance().setRetail(cost);
                        UpdateOrderTypePriceAsyncTask updateGoldAsyncTask = new UpdateOrderTypePriceAsyncTask(asyncTaskHandler, OrderType.RETAIL, cost);
                        updateGoldAsyncTask.execute();
                        showProgress();

                    }
                }else {
                    retailText.setError(getString(R.string.error_empty));
                }
            }
        });

        saveWholesale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!wholesaleText.getText().toString().isEmpty()) {
                    Double cost = Double.parseDouble(wholesaleText.getText().toString());
                    if (!cost.equals(wholesaleCost)) {
                        JewelryApplication.instance().setWholesale(cost);
                        UpdateOrderTypePriceAsyncTask updateGoldAsyncTask = new UpdateOrderTypePriceAsyncTask(asyncTaskHandler, OrderType.WHOLESALE, cost);
                        updateGoldAsyncTask.execute();
                        showProgress();

                    }
                } else {
                    wholesaleText.setError(getString(R.string.error_empty));
                }
            }
        });

        saveShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!shopText.getText().toString().isEmpty()) {
                    Double cost = Double.parseDouble(shopText.getText().toString());
                    if (!cost.equals(shopCost)) {
                        JewelryApplication.instance().setShop(cost);
                        UpdateOrderTypePriceAsyncTask updateGoldAsyncTask = new UpdateOrderTypePriceAsyncTask(asyncTaskHandler, OrderType.SHOP, cost);
                        updateGoldAsyncTask.execute();
                        showProgress();

                    }
                }else{
                    shopText.setError(getString(R.string.error_empty));
                }
            }
        });

    }

}
