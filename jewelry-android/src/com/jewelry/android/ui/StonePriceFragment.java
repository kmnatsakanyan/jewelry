package com.jewelry.android.ui;


import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.*;
import android.widget.*;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.task.AsyncTaskHandler;
import com.jewelry.android.task.GenericAsyncTaskHandler;
import com.jewelry.android.task.UpdateStonePriceAsyncTask;
import com.jewelry.android.ui.component.StoneType;
import com.jewelry.dto.StoneDiameterDTO;
import com.jewelry.dto.StonePriceDTO;
import com.jewelry.dto.StoneQualityDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.util.JewelryConstant;
import org.joda.time.DateTime;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * User: mos;
 */
public class StonePriceFragment extends BaseFragment {

    ProductDAOHelper mProductDAOHelper =JewelryApplication.DAO;
    final Map <Integer,StonePriceDTO> editedMap = new HashMap<Integer, StonePriceDTO>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_stone_price, container, false);
        setHasOptionsMenu(true);
        final TableLayout tableLayout = (TableLayout)rootView.findViewById(R.id.table_body);
        Spinner typeSpinner = (Spinner)rootView.findViewById(R.id.stone_types);
        List<String> typeItems = new ArrayList<String>();
        typeItems.addAll(StoneType.getNameList());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_spinner_item, typeItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(adapter);

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                Integer stoneId = pos;
                editedMap.clear();
                List <StoneQualityDTO> qualityList = mProductDAOHelper.getQualityByType(stoneId);
                List <StoneDiameterDTO> diameterList = mProductDAOHelper.getStoneDiameterList(stoneId);
                Context context = getActivity();
                tableLayout.removeAllViews();

                if(qualityList.size()!= 0 ){

                    //Add table header

                    TableRow header = new TableRow(context);
                    TextView diameterUnit = new TextView(context);
                    diameterUnit.setText(R.string.diameter_unit);
                    header.addView(diameterUnit);
                    diameterUnit.setTextSize(20);

                    for(StoneQualityDTO quality : qualityList){
                        TextView text = new TextView(context);
                        text.setMinWidth(100);
                        text.setText(quality.getQuality());
                        text.setBackgroundResource(R.drawable.cell_shape);
                        text.setTextSize(20);
                        text.setGravity(Gravity.CENTER);
                        header.addView(text);
                    }
                    tableLayout.addView(header);
                    //Add table body
                    for(StoneDiameterDTO diameter : diameterList ){
                        TableRow row = new TableRow(context);
                        TextView diameterView = new TextView(context);
                        diameterView.setText(diameter.getStart().toString());
                        if(diameter.getEnd()>0){
                            diameterView.append(" -" + diameter.getEnd());
                        }


                        row.addView(diameterView);
                        for(StoneQualityDTO quality : qualityList){
                            final  EditText priceText = new EditText(context);
                            final  StonePriceDTO priceDTO = mProductDAOHelper.getPrice(stoneId, diameter.getId(), quality.getId());
                            if(priceDTO!= null){
                                priceText.setText(priceDTO.getCost().toString());
                            }
                            priceText.setBackgroundResource(R.drawable.cell_shape);
                            priceText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL );


                            priceText.addTextChangedListener(new TextWatcher() {
                                        public void afterTextChanged(Editable s) {
                                            String value = priceText.getText().toString().trim();
                                            if(value.equals("")){
                                                priceDTO.setCost(0.0f);
                                            }
                                            else{
                                                priceDTO.setCost(Float.parseFloat(value));
                                            }
                                            priceDTO.setUpdateDate(DateTime.now());
                                            editedMap.put(priceDTO.getId(), priceDTO);
                                        }

                                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                                        public void onTextChanged(CharSequence s, int start, int before, int count) {}
                                    });
                            row.addView(priceText);

                        }
                        tableLayout.addView(row);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
        initProgressBar(rootView);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit_actions, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_save_product:
                savePrice();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void savePrice(){

        if(editedMap.size() != 0){
            final  List<StonePriceDTO> list = new ArrayList<StonePriceDTO>(editedMap.values());
            AsyncTaskHandler<ValueDTO<Boolean>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<Boolean>>() {
                @Override
                public void handle(ValueDTO<Boolean> result) {
                    hideProgress();
                    mProductDAOHelper.savePriceList(list);
                    editedMap.clear();
                }
                @Override
                public void handleError(ValueDTO<Boolean> result) {
                    hideProgress();
                    switch (result.getResultCode()) {
                        case JewelryConstant.RESPONSE_UNAUTHORIZED:
                            Toast.makeText(JewelryApplication.instance(), getResources()
                                    .getString(R.string.error_unauthorized_user), Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            super.handleError(result);
                    }
                }
            };
            UpdateStonePriceAsyncTask updateStonePriceAsyncTask = new UpdateStonePriceAsyncTask(asyncTaskHandler, list);
            updateStonePriceAsyncTask.execute();
            showProgress();

        }
    }

  }

