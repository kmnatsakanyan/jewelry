package com.jewelry.android.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.jewelry.android.R;
import com.jewelry.android.task.AddCatalogAsyncTask;
import com.jewelry.android.task.AsyncTaskHandler;
import com.jewelry.android.task.GenericAsyncTaskHandler;
import com.jewelry.dto.CatalogDTO;
import com.jewelry.dto.ValueDTO;

/**
 * User: karen.mnatsakanyan
 */
public class AddCatalogActivity extends FragmentActivity {

    LinearLayout progressBarLayout;
    TextView mlName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_catalog);
        mlName = (TextView) findViewById(R.id.name);
        progressBarLayout = (LinearLayout) findViewById(R.id.progressLayout);
    }

    private void saveCatalog() {
        String lName = mlName.getText().toString();
        boolean validate = true;

        if (lName.isEmpty()) {
            mlName.setError(getString(R.string.validation_name));
            validate = false;
        }
        if (validate) {
            CatalogDTO catalogDTO = new CatalogDTO();
            catalogDTO.setName(lName);
            AsyncTaskHandler<ValueDTO<CatalogDTO>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<CatalogDTO>>() {

                @Override
                public void handle(ValueDTO<CatalogDTO> result) {
                    hideProgress();
                    AddCatalogActivity.this.setResult(RESULT_OK);
                    AddCatalogActivity.this.finish();
                }

                @Override
                public void handleError(ValueDTO<CatalogDTO> result) {
                    hideProgress();
                    super.handleError(result);
                }
            };
            AddCatalogAsyncTask addCatalogAsyncTask = new AddCatalogAsyncTask(asyncTaskHandler, catalogDTO);
            addCatalogAsyncTask.execute();
            showProgress();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_save_product:
                saveCatalog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void hideProgress() {
        progressBarLayout.setVisibility(View.GONE);
    }

    public void showProgress() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }
}
