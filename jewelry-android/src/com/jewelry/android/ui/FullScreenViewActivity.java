package com.jewelry.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import com.jewelry.android.R;
import com.jewelry.android.domain.Photo;
import com.jewelry.android.ui.component.FullScreenImageAdapter;

import java.util.ArrayList;
import java.util.List;

public class FullScreenViewActivity extends FragmentActivity {
    public static final String ARG_PRODUCT_PHOTOS = "product_photos";

    private FullScreenImageAdapter adapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_view);

        viewPager = (ViewPager) findViewById(R.id.pager);


        Intent i = getIntent();
        int position = i.getIntExtra("position", 0);
        List<Photo> imagePaths = new ArrayList<Photo>();
        if (getIntent().getSerializableExtra(ARG_PRODUCT_PHOTOS) != null) {
            imagePaths = (List<Photo>) getIntent().getSerializableExtra(ARG_PRODUCT_PHOTOS);
        }

        adapter = new FullScreenImageAdapter(FullScreenViewActivity.this, imagePaths);

        viewPager.setAdapter(adapter);

        // displaying selected image first
        viewPager.setCurrentItem(position);
    }
}
