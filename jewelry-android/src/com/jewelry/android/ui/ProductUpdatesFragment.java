package com.jewelry.android.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.domain.ProductTemp;
import com.jewelry.android.task.*;
import com.jewelry.android.ui.component.ProductUpdatesListAdapter;
import com.jewelry.android.util.BitmapUtils;
import com.jewelry.dto.ProductDTO;
import com.jewelry.dto.ValueDTO;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class ProductUpdatesFragment extends BaseFragment {

    private static final int DELETE_UPDATE = 0;
    private static final int EDIT_UPDATE = 1;

    ProductUpdatesListAdapter mAdapter;
    ProductDAOHelper dao = JewelryApplication.DAO;
    List<ProductTemp> productTemps;
    ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_product_list, container, false);
        listView = (ListView) rootView.findViewById(R.id.product_list);

        productTemps = dao.getTempProductList();
        mAdapter = new ProductUpdatesListAdapter((FragmentActivity) this.getActivity());
        mAdapter.setRowItems(productTemps);
        registerForContextMenu(listView);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                startEditActivity(position);
            }
        });
        initProgressBar(rootView);
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem item = menu.add(Menu.NONE, R.id.action_sync, Menu.NONE, R.string.menu_action_sync).setIcon(R.drawable.ic_action_refresh);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sync:

                AsyncTaskHandler<ValueDTO<Boolean>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<Boolean>>() {
                    @Override
                    public void handle(ValueDTO<Boolean> result) {
                        productTemps = dao.getTempProductList();
                        mAdapter.setRowItems(productTemps);
                        mAdapter.notifyDataSetChanged();
                        ProductAsyncTask productAsyncTask = new ProductAsyncTask(new GenericAsyncTaskHandler<ValueDTO<List<ProductDTO>>>() {
                            @Override
                            public void handle(ValueDTO<List<ProductDTO>> result) {
                                hideProgress();
                            }

                            @Override
                            public void handleError(ValueDTO<List<ProductDTO>> result) {
                                hideProgress();
                                super.handleError(result);
                            }
                        }, (FragmentActivity) ProductUpdatesFragment.this.getActivity(), new AsyncTaskProgressHandler() {
                            @Override
                            public void onProgressUpdate(Integer... progress) {
                                ProductUpdatesFragment.this.onProgressUpdate(progress[0]);
                            }
                        });
                        productAsyncTask.execute();
                    }

                    @Override
                    public void handleError(ValueDTO<Boolean> result) {
                        hideProgress();
                        super.handleError(result);
                    }
                };
                SendProductUpdatesTask sendProductUpdatesTask = new SendProductUpdatesTask(asyncTaskHandler, productTemps);
                sendProductUpdatesTask.execute();
                showProgress();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.product_list) {
            menu.add(0, DELETE_UPDATE, 0, getResources().getString(R.string.menu_delete));
            menu.add(0, EDIT_UPDATE, 1, getResources().getString(R.string.menu_action_edit));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case DELETE_UPDATE:
                AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                int position = acmi.position;
                deleteTempProduct(mAdapter.getItem(position));
                return true;
            case EDIT_UPDATE:
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                int index = info.position;
                startEditActivity(index);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                productTemps = dao.getTempProductList();
                mAdapter.setRowItems(productTemps);
                mAdapter.notifyDataSetChanged();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void startEditActivity(int position) {
        if (!mAdapter.getItem(position).getIsDeleted()) {
            Intent intent = new Intent(getActivity(), EditProductActivity.class);
            intent.putExtra(EditProductActivity.ARG_PRODUCT_ID, mAdapter.getItem(position).getProductId());
            intent.putExtra(EditProductActivity.ARG_PRODUCT_UPDATE_ID, mAdapter.getItem(position).getId());
            startActivityForResult(intent, 0);
        }else{
            Toast.makeText(this.getActivity(), R.string.deleted_item_edit, Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteTempProduct(ProductTemp temp) {
        dao.deleteTempProduct(temp.getId());
        if (temp.getProductPhotoKey() != null) {
            BitmapUtils.deleteProductPhotosIfExists(temp.getProductPhotoKey());
        }
        mAdapter.removeItem(temp);
        mAdapter.notifyDataSetChanged();
    }
}
