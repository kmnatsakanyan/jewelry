package com.jewelry.android.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.task.*;
import com.jewelry.android.ui.component.UserListAdapter;
import com.jewelry.dto.UserDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.util.JewelryConstant;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class UserListFragment extends BaseFragment {
    ListView mListView;
    UserListAdapter mUserListAdapter;
    private static final int DELETE_USER_MENU_ITEM = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        mListView = (ListView) rootView.findViewById(R.id.list);
        mUserListAdapter = new UserListAdapter(JewelryApplication.instance());
        mListView.setAdapter(mUserListAdapter);

       mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent intent = new Intent(getActivity(), AddEditUserActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putSerializable(AddEditUserActivity.ARG_USER,(UserDTO)mUserListAdapter.getItem(position));
                intent.putExtras(mBundle);
                startActivityForResult(intent, 1);
                //startActivity(intent);
            }
        });

        if(JewelryApplication.instance().isLoggedInUserAdmin()){
            registerForContextMenu(mListView);
            setHasOptionsMenu(true);
        }
        initProgressBar(rootView);
        fetchUserList();
        return rootView;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.list) {
            menu.add(0, DELETE_USER_MENU_ITEM, 0, getResources().getString(R.string.menu_delete));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case DELETE_USER_MENU_ITEM:
                AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                TextView tx = (TextView) acmi.targetView;
                Integer userId = (Integer) tx.getTag();
                AsyncTaskHandler<ValueDTO<Boolean>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<Boolean>>() {
                    @Override
                    public void handle(ValueDTO<Boolean> result) {
                        if (result.getValue()) {
                            mUserListAdapter.getUsers().clear();
                            mUserListAdapter.notifyDataSetChanged();
                            fetchUserList();
                        }
                    }

                    @Override
                    public void handleError(ValueDTO<Boolean> result) {
                        switch (result.getResultCode()) {
                            case JewelryConstant.RESPONSE_USER_NOT_FOUND:
                                Toast.makeText(JewelryApplication.instance(), getResources().getString(R.string.error_user_not_found), Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                super.handleError(result);
                        }
                    }
                };
                DeleteUserAsyncTask deleteUserAsyncTask = new DeleteUserAsyncTask(asyncTaskHandler, userId);
                deleteUserAsyncTask.execute();
                return true;
            default:
                return super.onContextItemSelected(item);
        }


    }

    private void fetchUserList() {
        showProgress();
        GetUsersAsyncTask getUsersAsyncTask = new GetUsersAsyncTask(new GenericAsyncTaskHandler<ValueDTO<List<UserDTO>>>() {
            @Override
            public void handleError(ValueDTO<List<UserDTO>> result) {
                hideProgress();
                super.handleError(result);
            }

            @Override
            public void handle(ValueDTO<List<UserDTO>> result) {
                hideProgress();
                mUserListAdapter.setUser(result.getValue());
                mUserListAdapter.notifyDataSetChanged();
            }
        });
        getUsersAsyncTask.execute();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem item = menu.add(Menu.NONE, R.id.action_add, Menu.NONE, R.string.menu_action_add_product).setIcon(android.R.drawable.ic_menu_add);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent it = new Intent(getActivity(), AddEditUserActivity.class);
                startActivityForResult(it, 1);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {

            if (resultCode == Activity.RESULT_OK) {
                fetchUserList();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
