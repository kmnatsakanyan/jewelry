package com.jewelry.android.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.bitmap.util.ImageCache;
import com.jewelry.android.bitmap.util.ImageFetcher;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.domain.Photo;
import com.jewelry.android.domain.ProductTemp;
import com.jewelry.android.ui.component.ProductType;
import com.jewelry.android.util.BitmapHandler;
import com.jewelry.android.util.BitmapUtils;
import com.jewelry.android.util.JewelryMobileConstant;
import com.jewelry.dto.*;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.jewelry.android.util.JewelryMobileConstant.CAPTURE_PHOTO_SIZE_X;
import static com.jewelry.android.util.JewelryMobileConstant.CAPTURE_PHOTO_SIZE_Y;

/**
 * User: karen.mnatsakanyan
 */
public class EditProductActivity extends FragmentActivity {
    public static final String ARG_SESSION_ID = "session_id";
    public static final String ARG_PRODUCT_ID = "product_id";
    private static final String ARG_PRODUCT_PHOTO_KEY = "product_photo_key";
    public static final String ARG_PRODUCT_UPDATE_ID = "product_update_id";
    public static final String ARG_FILE_URI = "file_uri";
    public static final String ARG_PRODUCT_CATALOGS = "catalogs";
    public static final String ARG_PRODUCT_STONES = "stones";

    // Activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int GALLERY_SELECT_IMAGE_REQUEST_CODE = 200;
    public static final int MEDIA_TYPE_IMAGE = 1;

    protected ProductDAOHelper dao = JewelryApplication.DAO;

    private EditText mProductId;
    private EditText mWeight;
    private Spinner mSpinner;
    private Spinner catalogsSpinner;
    private LinearLayout catalogLayout;
    private EditText mDiameter;
    private EditText mStoneCount;
    private EditText mCatalogLabel;
    private Map<Integer, ProductStoneDTO> stoneMap;
    private Integer mapId = 0;


    private List<String> filePaths;
    private List<String> deleteFilePaths = new ArrayList<String>();
    private List<ProductCatalogDTO> productCatalogs = new ArrayList<ProductCatalogDTO>();
    private LinearLayout photoLayer;

    private Integer productId;
    private Integer tempProductId = null;
    private Integer productUpdateId = null;
    private long sessionId;
    private String photoKey;

    private ProductTemp productTemp;
    private Uri fileUri; // file url to store image

    private List<CatalogDTO> catalogs;

    protected ImageFetcher mImageFetcher;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        stoneMap = new HashMap<Integer, ProductStoneDTO>();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);
        //init views
        mProductId = (EditText) findViewById(R.id.productId);
        mWeight = (EditText) findViewById(R.id.weight);
        mSpinner = (Spinner) findViewById(R.id.type);
        catalogsSpinner = (Spinner) findViewById(R.id.catalog);
        photoLayer = (LinearLayout) findViewById(R.id.photosLayout);
        Button photo = (Button) findViewById(R.id.btnPhoto);
        Button gallery = (Button) findViewById(R.id.btnGallery);
        mDiameter = (EditText) findViewById(R.id.diameter);
        Button addStone = (Button) findViewById(R.id.add_stone);
        mStoneCount = (EditText) findViewById(R.id.stone_count);
        mCatalogLabel = (EditText) findViewById(R.id.catalog_label);
        catalogLayout = (LinearLayout) findViewById(R.id.catalogLayout);
        Button addCatalog = (Button) findViewById(R.id.add_catalog);
        int mImageThumbSize = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, JewelryMobileConstant.IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this, mImageThumbSize);
        mImageFetcher.setLoadingImage(R.drawable.empty_photo);
        mImageFetcher.addImageCache(this.getSupportFragmentManager(), cacheParams);
        mImageFetcher.setImageFadeIn(false);

        catalogs = dao.getCatalogs();
        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImage();
            }
        });
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImageFromGallery();
            }
        });
        mCatalogLabel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    mCatalogLabel.setError(null);
                }
            }
        });
        addStone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addStone();

            }
        });

        addCatalog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addProductCatalog();
            }
        });
        sessionId = getIntent().getLongExtra(ARG_SESSION_ID, 0);
        if (sessionId == 0) {
            sessionId = System.nanoTime();
        }

        photoKey = getIntent().getStringExtra(ARG_PRODUCT_PHOTO_KEY);
        if (photoKey == null) {
            //case new
            photoKey = System.nanoTime() + "";
        }
        onCreateProductTypeSpinner();
        productId = getIntent().getIntExtra(ARG_PRODUCT_ID, 0);
        if (productId != 0) {
            ProductDTO productDTO = dao.getProduct(productId);
            productTemp = dao.getTempProductByProductId(productId);
            fillProductData(productDTO, productTemp);
            fillProductImages(productDTO);
        }

        productUpdateId = getIntent().getIntExtra(ARG_PRODUCT_UPDATE_ID, 0);
        if (productUpdateId != 0) {
            productTemp = dao.getTempProductById(productUpdateId);
            ProductDTO productDTO = null;
            if (productTemp.getProductId() != null) {
                productDTO = dao.getProduct(productTemp.getProductId());
            }
            fillProductData(productDTO, productTemp);
            if (productId == 0)
                fillProductImages(productDTO);
        }


        onCreateCatalogSpinner();
    }


    private void fillProductData(ProductDTO productDTO, ProductTemp productTemp) {
        if (productTemp != null) {
            photoKey = productTemp.getProductPhotoKey();
            mWeight.setText(productTemp.getWeight() + "");
            mProductId.setText(productTemp.getProductName());
            mSpinner.setSelection(productTemp.getType());
            tempProductId = productTemp.getId();

        } else {

            productTemp = new ProductTemp();
            mWeight.setText(productDTO.getWeight() + "");
            mProductId.setText(productDTO.getProductId());
            mSpinner.setSelection(productDTO.getType());
            productTemp.setProductStones(productDTO.getProductStones());
            productTemp.setType(productDTO.getType());
            productTemp.setProductId(productDTO.getId());
            productTemp.setProductName(productDTO.getProductId());
            productTemp.setProductPhotoKey(photoKey);
            productTemp.setWeight(productDTO.getWeight());
            productTemp.setProductCatalogs(productDTO.getProductCatalogs());
        }

        fillProductStones(productTemp.getProductStones());
        fillProductCatalogs(productTemp.getProductCatalogs());
    }

    private void fillProductStones(List<ProductStoneDTO> stones) {
        LinearLayout ll = (LinearLayout) findViewById(R.id.stoneLayout);
        ll.removeAllViews();
        stoneMap.clear();
        if (stones != null) {
            for (ProductStoneDTO stone : stones) {
                addStoneLayout(stone);
            }
        }
    }

    private void fillProductCatalogs(List<ProductCatalogDTO> catalogs) {
        catalogLayout.removeAllViews();
        productCatalogs.clear();
        productCatalogs.addAll(catalogs);
        if (catalogs != null) {
            for (ProductCatalogDTO catalog : catalogs) {
                previewCatalog(catalog);
            }
        }
    }

    private void fillProductImages(ProductDTO productDTO) {
        filePaths = new ArrayList<String>();
        if (productDTO != null && productDTO.getProductPhotos() != null && productDTO.getProductPhotos().size() > 0) {
            for (ProductPhotoDTO productPhotoDTO : productDTO.getProductPhotos()) {
                filePaths.add(BitmapUtils.getAlbumStorageDir() + "/" + productPhotoDTO.getPhotoFile());
            }
        }
        filePaths.addAll(BitmapUtils.getProductPhotosIfExists(photoKey));//getting new photos;
        for (String file : filePaths) {
            previewCapturedImage(file);
        }
    }

    protected void onCreateProductTypeSpinner() {
        List<String> spinnerItems = new ArrayList<String>();
        spinnerItems.addAll(ProductType.getNameList());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, spinnerItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
    }

    protected void onCreateCatalogSpinner() {
        List<String> spinnerItems = new ArrayList<String>();

        spinnerItems.addAll(getCatalogNames());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, spinnerItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        catalogsSpinner.setAdapter(adapter);
    }

    private ArrayList<String> getCatalogNames() {
        ArrayList<String> strings = new ArrayList<String>();
        for (CatalogDTO catalogDTO : catalogs) {
            strings.add(catalogDTO.getName());
        }
        return strings;
    }

    /*
    * Capturing Camera Image will lauch camera app requrest image capture
    */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void selectImageFromGallery() {
        Intent i = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        startActivityForResult(i, GALLERY_SELECT_IMAGE_REQUEST_CODE);
    }

    private void addStoneLayout(ProductStoneDTO stone) {
        LinearLayout my_root = (LinearLayout) findViewById(R.id.stoneLayout);
        final LinearLayout stoneLayout = new LinearLayout(this);
        stoneLayout.setOrientation(LinearLayout.HORIZONTAL);
        stoneLayout.setGravity(Gravity.CENTER_VERTICAL);
        stoneLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        stoneMap.put(mapId, stone);
        TextView diameterView = new TextView(this);
        diameterView.setText(getResources().getText(R.string.stone_diameter) + " "
                + stone.getDiameter());
        TextView countView = new TextView(this);
        countView.setText(getResources().getText(R.string.count) + " " + stone.getCount().toString() + " ");

        Button deleteStone = new Button(this);
        deleteStone.setBackgroundDrawable(getResources().getDrawable(android.R.drawable.ic_delete));
        deleteStone.setLayoutParams(new LinearLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.delete_button), getResources().getDimensionPixelSize(R.dimen.delete_button)));
        final int stoneId = mapId;
        deleteStone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stoneMap.remove(stoneId);
                stoneLayout.removeAllViews();
            }
        });
        mapId++;

        stoneLayout.addView(deleteStone);
        stoneLayout.addView(countView);
        stoneLayout.addView(diameterView);
        my_root.addView(stoneLayout);
    }


    private void addStoneLayout() {
        Iterator<Integer> keySet = stoneMap.keySet().iterator();
        LinearLayout my_root = (LinearLayout) findViewById(R.id.stoneLayout);
        while (keySet.hasNext()) {
            Integer mapId = keySet.next();
            ProductStoneDTO stone = stoneMap.get(mapId);

            final LinearLayout stoneLayout = new LinearLayout(this);
            stoneLayout.setOrientation(LinearLayout.HORIZONTAL);
            stoneLayout.setGravity(Gravity.CENTER_VERTICAL);
            stoneLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            TextView diameterView = new TextView(this);
            diameterView.setText(getResources().getText(R.string.stone_diameter) + " " + stone.getDiameter());
            TextView countView = new TextView(this);
            countView.setText(getResources().getText(R.string.count) + " " + stone.getCount().toString() + " ");

            Button deleteStone = new Button(this);
            deleteStone.setBackgroundDrawable(getResources().getDrawable(android.R.drawable.ic_delete));
            deleteStone.setLayoutParams(new LinearLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.delete_button), getResources().getDimensionPixelSize(R.dimen.delete_button)));
            final int stoneId = mapId;
            deleteStone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    stoneMap.remove(stoneId);
                    stoneLayout.removeAllViews();
                }
            });

            stoneLayout.addView(deleteStone);
            stoneLayout.addView(countView);
            stoneLayout.addView(diameterView);
            my_root.addView(stoneLayout);
        }
    }

    private void addStone() {
        Boolean validate = true;
        String count = mStoneCount.getText().toString();
        String diameter = mDiameter.getText().toString();
        if (count.isEmpty() || count.equals("0")) {
            mStoneCount.setError(getString(R.string.validation_count));
            validate = false;
        }
        if (diameter.isEmpty() || Float.parseFloat(diameter) == 0) {
            mDiameter.setError(getString(R.string.enter_stone_diameter));
        }
        if (validate) {
            mStoneCount.setText("");
            mDiameter.setText("");
            ProductStoneDTO stone = new ProductStoneDTO();
            stone.setDiameter(Float.parseFloat(diameter));
            stone.setCount(Integer.parseInt(count));
            addStoneLayout(stone);
        }
    }

    private void addProductCatalog() {
        Boolean validate = true;
        String label = mCatalogLabel.getText().toString();
        final CatalogDTO catalog = getSelectedCatalogFromSpinner();
        if (label.isEmpty() || label.trim().equals("")) {
            mCatalogLabel.setError(getString(R.string.validation_product_catalog_label));
            validate = false;
        }
        for (ProductCatalogDTO productCatalogDTO : productCatalogs) {
            if (productCatalogDTO.getCatalog() != null && productCatalogDTO.getCatalog().getId().equals(catalog.getId())) {
                mCatalogLabel.setError(getString(R.string.validation_product_catalog_exists));
                validate = false;
            }
        }
        if (validate) {
            mCatalogLabel.setText("");
            catalogsSpinner.setSelection(0);
            mCatalogLabel.setError(null);
            ProductCatalogDTO productCatalogDTO = new ProductCatalogDTO();
            productCatalogDTO.setProductLabel(label);
            productCatalogDTO.setCatalog(catalog);
            productCatalogs.add(productCatalogDTO);
            previewCatalog(productCatalogDTO);
        }
    }

    private CatalogDTO getSelectedCatalogFromSpinner() {
        int position = catalogsSpinner.getSelectedItemPosition();
        return catalogs.get(position);
    }

    /**
     * Receiving activity result method will be called after closing the camera
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // display it in image view
                BitmapUtils.decodeBitmapAsync(new BitmapHandler() {
                    @Override
                    public void handleResponse(Bitmap bitmap) {
                        try {
                            File file = new File(fileUri.getPath());
                            FileOutputStream fOut = new FileOutputStream(file);

                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                            fOut.flush();
                            fOut.close();
                        } catch (Exception ex) {
                            Log.e("resize image", ex.getMessage());
                        }
                        previewCapturedImage(fileUri.getPath());
                    }
                }, getContentResolver(), fileUri, CAPTURE_PHOTO_SIZE_X, CAPTURE_PHOTO_SIZE_Y, fileUri.getPath());


            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        } else if (requestCode == GALLERY_SELECT_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Let's read picked image data - its URI
                Uri pickedImage = data.getData();
                // Let's read picked image path using content resolver
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
                cursor.moveToFirst();
                final String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

                // successfully captured the image
                // display it in image view
                BitmapUtils.decodeBitmapAsync(new BitmapHandler() {
                    @Override
                    public void handleResponse(Bitmap bitmap) {
                        try {
                            File file = new File(fileUri.getPath());
                            FileOutputStream fOut = new FileOutputStream(file);


                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                            fOut.flush();
                            fOut.close();
                        } catch (Exception ex) {
                            Log.e("resize image", ex.getMessage());
                        }
                        previewCapturedImage(fileUri.getPath());
                    }
                }, getContentResolver(), Uri.fromFile(new File(imagePath)), CAPTURE_PHOTO_SIZE_X, CAPTURE_PHOTO_SIZE_Y, imagePath);


            }
        } else if (resultCode == RESULT_CANCELED) {
            // user cancelled Image capture
            Toast.makeText(getApplicationContext(),
                    "User cancelled image capture", Toast.LENGTH_SHORT)
                    .show();
        } else {
            // failed to capture image
            Toast.makeText(getApplicationContext(),
                    "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                    .show();
        }
    }


    /*
     * Display image from a path to ImageView
	 */
    private void previewCapturedImage(final String fileName) {
        try {
            final View child = getLayoutInflater().inflate(R.layout.delete_photo, null);

            final ImageView imageView = (ImageView) child.findViewById(R.id.image);
            //delete image function
            Button deleteBtn = (Button) child.findViewById(R.id.btnDelete);
            deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteFilePaths.add(fileName);
                    ((ViewManager) child.getParent()).removeView(child);
                }
            });
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(EditProductActivity.this, FullScreenViewActivity.class);
                    ArrayList<Photo> arrayList = new ArrayList<Photo>();
                    Photo photo = new Photo();
                    photo.setFilePath(fileName);
                    arrayList.add(photo);
                    i.putExtra(FullScreenViewActivity.ARG_PRODUCT_PHOTOS, arrayList);
                    startActivity(i);
                }
            });

            mImageFetcher.loadImage(fileName, imageView);
            //BitmapUtils.loadPhotoToasdasdViewByFullPath(fileName.substring(fileName.lastIndexOf(("/")) + 1), imageView, fileName);
            photoLayer.addView(child);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    /*
 * Creating file uri to store image/video
 */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /*
     * returning image / video
     */
    private File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                BitmapUtils.IMAGE_DIRECTORY_NAME
        );

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(BitmapUtils.IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + BitmapUtils.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "product" + photoKey + "session"
                    + sessionId + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(ARG_SESSION_ID, sessionId);
        outState.putString(ARG_PRODUCT_PHOTO_KEY, photoKey);
        outState.putParcelable(ARG_FILE_URI, fileUri);
        outState.putSerializable(ARG_PRODUCT_CATALOGS, (ArrayList) productCatalogs);
        outState.putSerializable(ARG_PRODUCT_STONES, (HashMap) stoneMap);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState.containsKey(ARG_SESSION_ID)) {
            sessionId = savedInstanceState.getLong(ARG_SESSION_ID);
            filePaths = BitmapUtils.getProductPhotosIfExists(photoKey);//getting new photos;
        }

        if (savedInstanceState.containsKey(ARG_PRODUCT_PHOTO_KEY)) {
            photoKey = savedInstanceState.getString(ARG_PRODUCT_PHOTO_KEY);
        }

        if (savedInstanceState.containsKey(ARG_PRODUCT_CATALOGS)) {
            productCatalogs = (ArrayList) savedInstanceState.getSerializable(ARG_PRODUCT_CATALOGS);
            List<ProductCatalogDTO> copy = new ArrayList<ProductCatalogDTO>(productCatalogs.size());
            copy.addAll(productCatalogs);
            fillProductCatalogs(copy);
        }

        if (savedInstanceState.containsKey(ARG_PRODUCT_STONES)) {
            stoneMap = (HashMap) savedInstanceState.getSerializable(ARG_PRODUCT_STONES);
            LinearLayout my_root = (LinearLayout) findViewById(R.id.stoneLayout);
            my_root.removeAllViews();
            addStoneLayout();
        }

        if (savedInstanceState.containsKey(ARG_PRODUCT_ID)) {
            productId = savedInstanceState.getInt(ARG_PRODUCT_ID, 0);
            if (productId != 0) {
                ProductDTO productDTO = dao.getProduct(productId);
                productTemp = dao.getTempProductByProductId(productId);
                fillProductData(productDTO, productTemp);
            }
        }

        if (savedInstanceState.containsKey(ARG_PRODUCT_UPDATE_ID)) {
            productUpdateId = getIntent().getIntExtra(ARG_PRODUCT_UPDATE_ID, 0);
            if (productUpdateId != 0) {
                productTemp = dao.getTempProductById(productUpdateId);
                fillProductData(null, productTemp);
            }
        }
        if (savedInstanceState.containsKey(ARG_FILE_URI)) {
            fileUri = savedInstanceState.getParcelable(ARG_FILE_URI);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_save_product:
                saveProduct();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveProduct() {
        String productName = mProductId.getText().toString().trim();
        String productWeight = mWeight.getText().toString().trim();
        boolean validate = true;
        if (productName.isEmpty()) {
            mProductId.setError(getString(R.string.validation_product_name));
            validate = false;
        }

        if (productWeight.isEmpty() || productWeight.equals("0")) {
            mWeight.setError(getString(R.string.validation_weight));
            validate = false;
        }
        if (validate) {
            ProductTemp productTemp = new ProductTemp();
            productTemp.setWeight(Float.valueOf(productWeight));
            productTemp.setProductPhotoKey(photoKey);
            productTemp.setProductName(productName);
            List<ProductStoneDTO> list = new ArrayList<ProductStoneDTO>(stoneMap.values());
            productTemp.setProductStones(list);
            productTemp.setProductCatalogs(productCatalogs);
            if (ProductType.getNameList().size() > mSpinner.getSelectedItemPosition()) {
                String typeName = ProductType.getNameList().get(mSpinner.getSelectedItemPosition());
                Integer type = ProductType.findId(typeName);
                productTemp.setType(type);
            }
            if (productId != 0) {
                productTemp.setProductId(productId);
            }
            if (tempProductId != null) {
                productTemp.setId(tempProductId);
                dao.updateProductTemp(productTemp);
            } else {
                dao.saveTempProduct(productTemp);//new row
            }
            for (String fileName : deleteFilePaths) {
                File img = new File(fileName);
                if (fileName.contains(BitmapUtils.IMAGE_DIRECTORY_NAME)) {//check if image is temp
                    if (img.exists()) {
                        img.delete();
                    }
                } else if (productId != 0) {
                    String imageName = img.getName();
                    dao.deletePhotoFromProduct(imageName, productId);
                }


            }
            this.setResult(RESULT_OK);
            finish();
        }

    }

    @Override
    public void onBackPressed() {
        //removing unused files.
        File directory = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                BitmapUtils.IMAGE_DIRECTORY_NAME
        );
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.getName().contains(sessionId + "") && file.getName().contains(photoKey)) {
                    file.delete();
                }
            }
        }
        super.onBackPressed();
    }

    private void previewCatalog(final ProductCatalogDTO productCatalogDTO) {
        try {
            final View child = getLayoutInflater().inflate(R.layout.delete_catalog, null);

            if (child != null) {
                TextView catalog = (TextView) child.findViewById(R.id.catalog_lbl);
                catalog.setText(productCatalogDTO.getProductLabel());
                TextView label = (TextView) child.findViewById(R.id.label);
                if (productCatalogDTO.getCatalog() != null) {
                    label.setText(productCatalogDTO.getCatalog().getName());
                }
                //delete image function
                Button deleteBtn = (Button) child.findViewById(R.id.btnDelete);
                deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Iterator<ProductCatalogDTO> i = productCatalogs.iterator();
                        while (i.hasNext()) {
                            ProductCatalogDTO dto = i.next();
                            if (dto.getCatalog() != null && dto.getCatalog().getId().equals(productCatalogDTO.getCatalog().getId())) {
                                i.remove();
                            }
                        }
                        if (child.getParent() != null) {
                            ((ViewManager) child.getParent()).removeView(child);
                        }
                    }
                });
                catalogLayout.addView(child);
            }


        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}
