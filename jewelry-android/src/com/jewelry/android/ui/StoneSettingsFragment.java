package com.jewelry.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.task.*;
import com.jewelry.android.ui.component.StoneType;
import com.jewelry.dto.StoneDiameterDTO;
import com.jewelry.dto.StonePriceDTO;
import com.jewelry.dto.StoneQualityDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.util.JewelryConstant;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;


/**
 * User: mos;
 */
public class StoneSettingsFragment extends BaseFragment {
    ProductDAOHelper mProductDAOHelper = JewelryApplication.DAO;
    private Integer stoneId = 0;
    LinearLayout qualityListView ;
    LinearLayout diameterListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_stone_setings, container, false);
        final Spinner typeSpinner = (Spinner) rootView.findViewById(R.id.stone_types);
        createStoneTypeSpinner(typeSpinner);

       qualityListView  = (LinearLayout) rootView.findViewById(R.id.quality_list);
       diameterListView = (LinearLayout) rootView.findViewById(R.id.diameter_list);

        onCreateDiameter(rootView);
        onCreateQuality(rootView);

        onCreateDiameterList();
        onCreateQualityList();

        initProgressBar(rootView);
        return rootView;
    }

    private void onCreateQuality(View rootView) {
        final EditText mQualityText = (EditText) rootView.findViewById(R.id.quality);
        Button saveBtn = (Button) rootView.findViewById(R.id.quality_save);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String quality = mQualityText.getText().toString().trim();
                mQualityText.clearFocus();
                boolean valid = true;
                if (quality.equals("")) {
                    valid = false;
                    mQualityText.setError(getString(R.string.enter_stone_quality));
                } else {
                    if (mProductDAOHelper.checkQualityExist(quality, stoneId)) {
                        valid = false;
                        mQualityText.setError(getString(R.string.validation_unique));
                    }
                }
                if (valid) {

                    StoneQualityDTO stoneQuality = new StoneQualityDTO();
                    stoneQuality.setQuality(quality);
                    stoneQuality.setStoneId(stoneId);

                    AsyncTaskHandler<ValueDTO<StoneQualityDTO>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<StoneQualityDTO>>() {
                        @Override
                        public void handle(ValueDTO<StoneQualityDTO> result) {
                            List<StonePriceDTO> stonePrices = new ArrayList<StonePriceDTO>();
                            final StoneQualityDTO stoneQualityDTO = result.getValue();
                            List<StoneDiameterDTO> stoneDiameterList = mProductDAOHelper.getStoneDiameterList(stoneId);
                            for (StoneDiameterDTO diameter : stoneDiameterList) {
                                StonePriceDTO stonePrice = new StonePriceDTO();
                                stonePrice.setQualityID(stoneQualityDTO.getId());
                                stonePrice.setStoneID(stoneId);
                                stonePrice.setDiameterID(diameter.getId());
                                stonePrice.setUpdateDate(DateTime.now());
                                stonePrice.setCost(0f);
                                stonePrices.add(stonePrice);
                            }
                            AsyncTaskHandler<ValueDTO<List<StonePriceDTO>>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<List<StonePriceDTO>>>() {
                                @Override
                                public void handle(ValueDTO<List<StonePriceDTO>> result) {
                                    mProductDAOHelper.addStoneQuality(stoneQualityDTO);
                                    mProductDAOHelper.savePriceList(result.getValue());
                                    addQualityLayout(stoneQualityDTO);
                                    mQualityText.setText(null);
                                    hideProgress();
                                }

                                @Override
                                public void handleError(ValueDTO<List<StonePriceDTO>> result) {
                                    //todo delete StoneQualityDTO
                                    hideProgress();
                                    switch (result.getResultCode()) {
                                        case JewelryConstant.RESPONSE_UNAUTHORIZED:
                                            Toast.makeText(JewelryApplication.instance(), getResources()
                                                    .getString(R.string.error_unauthorized_user), Toast.LENGTH_SHORT).show();
                                            break;
                                        default:
                                            super.handleError(result);
                                    }
                                }
                            };

                            AddStonePriceAsyncTask addStonePriceAsyncTask = new AddStonePriceAsyncTask(asyncTaskHandler, stonePrices);
                            addStonePriceAsyncTask.execute();

                        }

                        @Override
                        public void handleError(ValueDTO<StoneQualityDTO> result) {
                            hideProgress();
                            switch (result.getResultCode()) {
                                case JewelryConstant.RESPONSE_UNAUTHORIZED:
                                    Toast.makeText(JewelryApplication.instance(), getResources()
                                            .getString(R.string.error_unauthorized_user), Toast.LENGTH_SHORT).show();
                                    break;
                                default:
                                    super.handleError(result);
                            }
                        }
                    };
                    UpdateQualityAsyncTask updateDiameterAsyncTask = new UpdateQualityAsyncTask(asyncTaskHandler, stoneQuality);
                    updateDiameterAsyncTask.execute();
                    showProgress();
                }

            }
        });

    }

    private void onCreateDiameter(View rootView) {
        final EditText mDiameterStart = (EditText) rootView.findViewById(R.id.stone_start);
        final EditText mDiameterEnd = (EditText) rootView.findViewById(R.id.stone_end);


        Button saveBtn = (Button) rootView.findViewById(R.id.stone_type_save);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String diameterStart = mDiameterStart.getText().toString().trim();
                String diameterEnd = mDiameterEnd.getText().toString().trim();
                StoneDiameterDTO diameterDTO = new StoneDiameterDTO();
                boolean valid = true;

                if (diameterStart.equals("")) {
                    valid = false;
                    mDiameterStart.setError(getString(R.string.enter_stone_diameter));
                } else {
                    Float diameterStartValue = Float.parseFloat(diameterStart);
                    Float diameterEndValue ;

                    if (mProductDAOHelper.checkDiameterExist(diameterStartValue, stoneId)) {
                        valid = false;
                        mDiameterStart.setError(getString(R.string.validation_unique));
                    } else {
                        diameterDTO.setStart(diameterStartValue);
                        diameterDTO.setStoneId(stoneId);

                        if (!diameterEnd.equals("")) {
                            diameterEndValue = Float.parseFloat(diameterEnd);
                            if (mProductDAOHelper.checkDiameterExist(diameterEndValue, stoneId)) {
                                valid = false;
                                mDiameterEnd.setError(getString(R.string.validation_unique));
                            } else {
                                if (diameterEndValue < diameterStartValue) {
                                    mDiameterEnd.setError(getString(R.string.validation_range));
                                    valid = false;
                                } else {
                                    if (mProductDAOHelper.checkDiameterRange(diameterStartValue, diameterEndValue, stoneId)) {
                                        mDiameterStart.setError(getString(R.string.validation_unique));
                                        mDiameterEnd.setError(getString(R.string.validation_unique));
                                    } else {
                                        diameterDTO.setEnd(diameterEndValue);
                                    }
                                }
                            }
                        }
                    }
                }
                if (valid) {
                    AsyncTaskHandler<ValueDTO<StoneDiameterDTO>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<StoneDiameterDTO>>() {
                        @Override
                        public void handle(ValueDTO<StoneDiameterDTO> result) {
                            List<StonePriceDTO> stonePrices = new ArrayList<StonePriceDTO>();
                            final StoneDiameterDTO stoneDiameterDTO = result.getValue();
                            List<StoneQualityDTO> stoneQualityList = mProductDAOHelper.getQualityByType(stoneId);
                            for (StoneQualityDTO quality : stoneQualityList) {
                                StonePriceDTO stonePrice = new StonePriceDTO();
                                stonePrice.setQualityID(quality.getId());
                                stonePrice.setStoneID(stoneId);
                                stonePrice.setDiameterID(stoneDiameterDTO.getId());
                                stonePrice.setUpdateDate(DateTime.now());
                                stonePrice.setCost(0f);
                                stonePrices.add(stonePrice);
                            }
                            AsyncTaskHandler<ValueDTO<List<StonePriceDTO>>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<List<StonePriceDTO>>>() {
                                @Override
                                public void handle(ValueDTO<List<StonePriceDTO>> result) {
                                    mProductDAOHelper.addStoneDiameter(stoneDiameterDTO);
                                    mProductDAOHelper.savePriceList(result.getValue());
                                    addDiameterLayout(stoneDiameterDTO);
                                    hideProgress();

                                }

                                @Override
                                public void handleError(ValueDTO<List<StonePriceDTO>> result) {
                                    //todo delete StoneDiameter
                                    hideProgress();
                                    switch (result.getResultCode()) {
                                        case JewelryConstant.RESPONSE_UNAUTHORIZED:
                                            Toast.makeText(JewelryApplication.instance(), getResources()
                                                    .getString(R.string.error_unauthorized_user), Toast.LENGTH_SHORT).show();
                                            break;
                                        default:
                                            super.handleError(result);
                                    }
                                }
                            };

                            AddStonePriceAsyncTask addStonePriceAsyncTask = new AddStonePriceAsyncTask(asyncTaskHandler, stonePrices);
                            addStonePriceAsyncTask.execute();

                        }

                        @Override
                        public void handleError(ValueDTO<StoneDiameterDTO> result) {
                            hideProgress();
                            switch (result.getResultCode()) {
                                case JewelryConstant.RESPONSE_UNAUTHORIZED:
                                    Toast.makeText(JewelryApplication.instance(), getResources()
                                            .getString(R.string.error_unauthorized_user), Toast.LENGTH_SHORT).show();
                                    break;
                                default:
                                    super.handleError(result);
                            }
                        }
                    };

                    UpdateDiameterAsyncTask updateDiameterAsyncTask = new UpdateDiameterAsyncTask(asyncTaskHandler, diameterDTO);
                    updateDiameterAsyncTask.execute();
                    showProgress();
                }
            }
        });

    }

    private void createStoneTypeSpinner(Spinner spinner) {
        List<String> typeItems = new ArrayList<String>();
        typeItems.addAll(StoneType.getNameList());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_spinner_item, typeItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                stoneId = pos;
                onCreateQualityList();
                onCreateDiameterList();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private void onCreateDiameterList() {
        diameterListView.removeAllViews();
        List<StoneDiameterDTO> diameterList = mProductDAOHelper.getStoneDiameterList(stoneId);
        for (StoneDiameterDTO diameter : diameterList) {
            addDiameterLayout(diameter);
        }

    }

    private void addDiameterLayout (final StoneDiameterDTO diameterDTO){
        final LinearLayout item = new LinearLayout(this.getActivity());
        Button delete = new Button(this.getActivity());
        item.setOrientation(LinearLayout.HORIZONTAL);

        item.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.FILL_PARENT));
        TextView diameterView = new TextView(this.getActivity());
        diameterView.setText(diameterDTO.getStart().toString());
        if((diameterDTO.getEnd()!= null)&&(!diameterDTO.getEnd().equals(0f))){
            diameterView.append("-" + diameterDTO.getEnd());
        }
        delete.setBackgroundDrawable(getResources().getDrawable(android.R.drawable.ic_delete));
        delete.setLayoutParams(new LinearLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.delete_button), getResources().getDimensionPixelSize(R.dimen.delete_button)));
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AsyncTaskHandler<ValueDTO<Boolean>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<Boolean>>() {
                    @Override
                    public void handle(ValueDTO<Boolean> result) {
                        if (result.getValue()) {
                            mProductDAOHelper.deleteStoneDiameter(diameterDTO);
                            item.removeAllViews();
                        }
                        hideProgress();
                    }

                    @Override
                    public void handleError(ValueDTO<Boolean> result) {
                        hideProgress();
                        switch (result.getResultCode()) {
                            case JewelryConstant.RESPONSE_USER_NOT_FOUND:
                                Toast.makeText(JewelryApplication.instance(),getResources().getString(R.string.error_quality_not_found), Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                super.handleError(result);
                        }
                    }
                };
                DeleteDiameterAsyncTask deleteQualityAsyncTask = new DeleteDiameterAsyncTask(asyncTaskHandler, diameterDTO.getId());
                deleteQualityAsyncTask.execute();
                showProgress();
            }
        });

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
        diameterView.setTextSize(20);
        item.addView(diameterView,param);
        item.addView(delete);
        diameterListView.addView(item);
    }

    private void onCreateQualityList() {
        qualityListView.removeAllViews();
        List<StoneQualityDTO> qualityList = mProductDAOHelper.getQualityByType(stoneId);
        for (StoneQualityDTO quality : qualityList) {
            addQualityLayout(quality);
        }
    }


    private void addQualityLayout (final StoneQualityDTO qualityDTO){
        final LinearLayout item = new LinearLayout(this.getActivity());
        Button delete = new Button(this.getActivity());
        item.setOrientation(LinearLayout.HORIZONTAL);

        item.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.FILL_PARENT));
        TextView qualityView = new TextView(this.getActivity());
        qualityView.setText(qualityDTO.getQuality());
        delete.setBackgroundDrawable(getResources().getDrawable(android.R.drawable.ic_delete));
        delete.setLayoutParams(new LinearLayout.LayoutParams(getResources().getDimensionPixelSize(R.dimen.delete_button), getResources().getDimensionPixelSize(R.dimen.delete_button)));
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AsyncTaskHandler<ValueDTO<Boolean>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<Boolean>>() {
                    @Override
                    public void handle(ValueDTO<Boolean> result) {
                        if (result.getValue()) {
                            item.removeAllViews();
                            mProductDAOHelper.deleteStoneQuality(qualityDTO);
                            hideProgress();
                        }
                    }

                    @Override
                    public void handleError(ValueDTO<Boolean> result) {
                        hideProgress();
                        switch (result.getResultCode()) {
                            case JewelryConstant.RESPONSE_USER_NOT_FOUND:
                                Toast.makeText(JewelryApplication.instance(),getResources().getString(R.string.error_quality_not_found), Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                super.handleError(result);
                        }

                    }
                };

                DeleteQualityAsyncTask deleteQualityAsyncTask = new DeleteQualityAsyncTask(asyncTaskHandler, qualityDTO.getId());
                deleteQualityAsyncTask.execute();
                showProgress();
            }
        });

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
        qualityView.setTextSize(20);
        item.addView(qualityView,param);
        item.addView(delete);
        qualityListView.addView(item);
    }
}