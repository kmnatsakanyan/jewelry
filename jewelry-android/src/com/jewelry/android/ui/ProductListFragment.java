package com.jewelry.android.ui;

/**
 * User: karen.mnatsakanyan
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.*;
import android.widget.*;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.domain.ProductTemp;
import com.jewelry.android.task.AsyncTaskHandler;
import com.jewelry.android.task.AsyncTaskProgressHandler;
import com.jewelry.android.task.GenericAsyncTaskHandler;
import com.jewelry.android.task.ProductAsyncTask;
import com.jewelry.android.ui.component.ProductListAdapter;
import com.jewelry.android.ui.component.ProductType;
import com.jewelry.android.util.JewelryMobileConstant;
import com.jewelry.dto.CatalogDTO;
import com.jewelry.dto.ProductDTO;
import com.jewelry.dto.UserRole;
import com.jewelry.dto.ValueDTO;

import java.util.ArrayList;
import java.util.List;


public class ProductListFragment extends BaseFragment implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {

    private static final int DELETE_PRODUCT = 0;

    private SearchView mSearchView;
    private ProductDAOHelper productDAOHelper = JewelryApplication.DAO;
    private ProductListAdapter mAdapter;
    private ListView mListView;
    private Spinner catalogsSpinner;
    private List<CatalogDTO> catalogs;
    private Spinner typeSpinner;
    private boolean firstTime;

    public ProductListFragment() {
    }

    public ProductListFragment(boolean firstTime) {
        this.firstTime = firstTime;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_product_list, container, false);
        mListView = (ListView) rootView.findViewById(R.id.product_list);
        setHasOptionsMenu(true);

        List<ProductDTO> rowItems = productDAOHelper.getProductList();
        mAdapter = new ProductListAdapter((FragmentActivity) this.getActivity());
        mAdapter.setRowItems(rowItems);
        mListView.setAdapter(mAdapter);
        mListView.setFastScrollEnabled(true);
        registerForContextMenu(mListView);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent intent = new Intent(getActivity(), ProductDetailsListFragment.class);
                intent.putExtra(ProductDetailsListFragment.ARG_PRODUCT_IDS, mAdapter.getProductIds());
                intent.putExtra(ProductDetailsListFragment.ARG_POSITION, position);
                startActivity(intent);
            }
        });
        initProgressBar(rootView);
        catalogsSpinner = (Spinner) rootView.findViewById(R.id.catalogs);
        catalogsSpinner.setVisibility(View.VISIBLE);
        catalogsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                searchProduct();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        if (firstTime) {
            startSync();
        }

        catalogs = productDAOHelper.getCatalogs();
        onCreateCatalogSpinner();
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.product_list_actions, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) searchItem.getActionView();
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);
        onCreateSpinner(menu);

        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                return onClose();
            }
        });
        if (JewelryApplication.instance().getLoggedInUser().getRole() != UserRole.ROLE_CANDY) {
            MenuItem item = menu.add(Menu.NONE, R.id.action_add_product, Menu.NONE, R.string.add).setIcon(android.R.drawable.ic_menu_add);
            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    protected void onCreateSpinner(Menu menu) {

        List<String> spinnerItems = new ArrayList<String>();
        spinnerItems.add(getString(R.string.menu_action_all));
        spinnerItems.addAll(ProductType.getNameList());

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_spinner_item, spinnerItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        MenuItem spinnerItem = menu.findItem(R.id.action_spinner);
        View view = spinnerItem.getActionView();
        if (view instanceof Spinner) {
            typeSpinner = (Spinner) view;
            typeSpinner.setAdapter(adapter);

            typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View arg1,
                                           int pos, long arg3) {
                    searchProduct();
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {


                }

            });
        }
    }

    @Override
    public boolean onClose() {
        searchProduct();
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        mSearchView.clearFocus();
        searchProduct(s);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                startSync();
                return true;
            case R.id.action_add_product:
                Intent intent = new Intent(getActivity(), EditProductActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void startSync() {
        AsyncTaskHandler<ValueDTO<List<ProductDTO>>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<List<ProductDTO>>>() {

            @Override
            public void handleError(ValueDTO<List<ProductDTO>> result) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                hideProgress();
                super.handleError(result);
            }

            @Override
            public void handle(ValueDTO<List<ProductDTO>> result) {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                hideProgress();
                if (result != null && result.getValue() != null && result.getValue().size() > 0) {
                    mAdapter.setGoldCost(getGoldCost());
                    catalogs = productDAOHelper.getCatalogs();
                    onCreateCatalogSpinner();
                    searchProduct();
                }

            }
        };
        ProductAsyncTask productAsyncTask = new ProductAsyncTask(asyncTaskHandler, (FragmentActivity) this.getActivity(), new AsyncTaskProgressHandler() {
            @Override
            public void onProgressUpdate(Integer... progress) {
                ProductListFragment.this.onProgressUpdate(progress[0]);
            }
        });
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        productAsyncTask.execute();
        showProgress();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (JewelryApplication.instance().isLoggedInUserAdmin() && v.getId() == R.id.product_list) {
            menu.add(0, DELETE_PRODUCT, 0, getResources().getString(R.string.menu_delete));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case DELETE_PRODUCT:
                AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                int position = acmi.position;
                deleteProduct((ProductDTO) mAdapter.getItem(position));
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void deleteProduct(ProductDTO item) {
        ProductTemp productTemp = productDAOHelper.getTempProductByProductId(item.getId());
        if (productTemp == null) {
            productTemp = new ProductTemp();
        }
        if (productTemp.getProductName() == null)
            productTemp.setProductName(item.getProductId());
        if (productTemp.getType() == null)
            productTemp.setType(item.getType());
        if (productTemp.getWeight() == null)
            productTemp.setWeight(item.getWeight());
        if (productTemp.getProductId() == null)
            productTemp.setProductId(item.getId());
        productTemp.setIsDeleted(true);
        if (productTemp.getId() == null) {
            productDAOHelper.saveTempProduct(productTemp);
        } else {
            productDAOHelper.updateProductTemp(productTemp);
        }
    }

    protected void onCreateCatalogSpinner() {
        List<String> spinnerItems = new ArrayList<String>();
        spinnerItems.addAll(getCatalogNames());
        spinnerItems.add(getString(R.string.menu_action_all));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, spinnerItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        catalogsSpinner.setAdapter(adapter);
    }

    private ArrayList<String> getCatalogNames() {
        ArrayList<String> strings = new ArrayList<String>();
        for (CatalogDTO catalogDTO : catalogs) {
            strings.add(catalogDTO.getName());
        }
        return strings;
    }

    private CatalogDTO getSelectedCatalogFromSpinner() {
        int position = catalogsSpinner.getSelectedItemPosition();
        return catalogs.get(position);
    }

    private void searchProduct(String query) {
        List<ProductDTO> rowItems;
        String selectedItem = typeSpinner.getSelectedItem().toString();
        Integer catalogId = null;
        Integer id = null;
        if (catalogs!=null && catalogsSpinner.getSelectedItemPosition() < catalogs.size() && catalogs.size()>0) {
            CatalogDTO catalogDTO = getSelectedCatalogFromSpinner();
            catalogId = catalogDTO.getId();
        }

        if (!selectedItem.equals(getString(R.string.menu_action_all))) {
            id = ProductType.findId(selectedItem);
        }

        rowItems = productDAOHelper.searchProductByTypeAndCatalog(id, catalogId, query);
        mAdapter.setRowItems(rowItems);
        mAdapter.notifyDataSetChanged();
    }

    private void searchProduct() {
        searchProduct(null);
    }

    private Double getGoldCost(){
       return ( JewelryApplication.instance().getGoldCost() / JewelryMobileConstant.GOLD_UNIT
                + JewelryMobileConstant.GOLD_TRANSPORT_PRICE) / JewelryMobileConstant.GOLD_TYPE_CARAT_0 + JewelryApplication.instance().getWholesale();
    }
}
