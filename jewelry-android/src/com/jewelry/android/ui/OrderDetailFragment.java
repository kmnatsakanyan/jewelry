package com.jewelry.android.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.bitmap.util.ImageCache;
import com.jewelry.android.bitmap.util.ImageFetcher;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.domain.OrderTemp;
import com.jewelry.android.domain.Photo;
import com.jewelry.android.task.AddOrderAsyncTask;
import com.jewelry.android.task.AsyncTaskHandler;
import com.jewelry.android.task.GenericAsyncTaskHandler;
import com.jewelry.android.task.UpdateOrderAsyncTask;
import com.jewelry.android.ui.component.GoldType;
import com.jewelry.android.ui.component.OrderStoneAdapter;
import com.jewelry.android.ui.component.OrderType;
import com.jewelry.android.util.BitmapUtils;
import com.jewelry.android.util.JewelryMobileConstant;
import com.jewelry.dto.*;
import org.joda.time.DateTime;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class OrderDetailFragment extends FragmentActivity {

    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_ORDER = "order";
    public static final String ARG_TEMP_ID = "temp_id";
    private static final String IMAGE_CACHE_DIR = "images";

    private Context globalContext = null;

    private  OrderStoneAdapter adapter;

    private OrderDTO mOrder;

    private int mTempId;

    private EditText doneText;

    private EditText mDescription;

    private ProductDAOHelper mProductDAOHelper;
    protected ImageFetcher mImageFetcher;
    /*private LinearLayout progressBarLayout;*/
    private ResultDialog positive = new ResultDialog(R.string.order_added);
    private ResultDialog negative = new ResultDialog(R.string.order_saved_to_draft);
    private LinearLayout progressBarLayout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_order_details);

        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        // For this sample we'll use half of the longest width to resize our images. As the
        // image scaling ensures the image is larger than this, we should be left with a
        // resolution that is appropriate for both portrait and landscape. For best image quality
        // we shouldn't divide by 2, but this will use more memory and require a larger memory
        // cache.
        final int longest = (height > width ? height : width) / 2;

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this, longest);
        mImageFetcher.addImageCache((this).getSupportFragmentManager(), cacheParams);
        progressBarLayout = (LinearLayout) findViewById(R.id.progressLayout);
        mOrder = (OrderDTO)getIntent().getSerializableExtra(ARG_ORDER);
        mTempId = getIntent().getIntExtra(ARG_TEMP_ID,0);
        mProductDAOHelper = new ProductDAOHelper(this);
        adapter = new OrderStoneAdapter(this);
        globalContext = this.getApplicationContext();

        TextView textView = (TextView)findViewById(R.id.productId);
        TextView mTextWeight = (TextView) findViewById(R.id.weight);
        TextView mTextGoldPrice = (TextView) findViewById(R.id.price);
        TextView mTextGold = (TextView) findViewById(R.id.gold_type);
        TextView mOrderType = (TextView)findViewById(R.id.order_type);
        mDescription = (EditText) findViewById(R.id.description);
        TextView mOrderCount = (TextView) findViewById(R.id.count);
        TextView priceSum = (TextView) findViewById(R.id.price_sum);
        doneText = (EditText) findViewById(R.id.done_count);
        LinearLayout stoneListView = (LinearLayout)findViewById(R.id.product_stone_list);
        /*progressBarLayout = (LinearLayout) findViewById(R.id.progressLayout);*/



        Integer goldTypeId = mOrder.getGoldType().ordinal();
        Integer orderTypeId = mOrder.getOrderType().ordinal();
        final ProductDTO productDTO = mOrder.getProduct();
        Float weight = productDTO.getWeight();


        if(goldTypeId == 1){
            weight *= JewelryMobileConstant.GOLD_TYPE_RATE;
        }

        textView.setText(productDTO.getProductId());
        mOrderType.setText(OrderType.findName(orderTypeId));
        mTextGold.setText(GoldType.findName(goldTypeId));
        mTextWeight.setText(weight.toString());
        if(JewelryApplication.instance().getLoggedInUser().getRole()!=UserRole.ROLE_CANDY){
            mTextGoldPrice.setText(String.format("%.2f", getProductGoldPrice(goldTypeId, weight, orderTypeId )));
        }else {
            mTextGoldPrice.setVisibility(View.INVISIBLE);
        }
        mOrderCount.setText(mOrder.getCount().toString());
        mDescription.setText(mOrder.getDescription());
        doneText.setText(mOrder.getDone().toString());
        onCreateStoneList(stoneListView, mOrder.getStones());

        Float sum = 0f;
        List<Float> priceList = adapter.getPriceList();
        for(Float price:priceList ){
            sum += price;
        }

        String gold = mTextGoldPrice.getText().toString();
        Float goldPrice = Float.parseFloat(gold);
        sum += goldPrice;
        if (JewelryApplication.instance().getLoggedInUser().getRole() == UserRole.ROLE_SHOP) {
            sum += Float.parseFloat(JewelryApplication.instance().getShop().toString());
        }
        if(JewelryApplication.instance().getLoggedInUser().getRole()!=UserRole.ROLE_CANDY){
            priceSum.setText(sum.toString());
        }else {
            priceSum.setVisibility(View.INVISIBLE);
        }
        ImageView productImage = (ImageView)findViewById(R.id.productImage);

        productImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(globalContext, FullScreenViewActivity.class);
                ArrayList<Photo> photos = new ArrayList<Photo>();
                for (ProductPhotoDTO productPhotoDTO : productDTO.getProductPhotos()) {
                    Photo photo = new Photo();
                    photo.setFilePath(BitmapUtils.getAlbumStorageDir() + File.separator + productPhotoDTO.getPhotoFile());
                    photos.add(photo);
                }
                i.putExtra(FullScreenViewActivity.ARG_PRODUCT_PHOTOS, photos);
                startActivity(i);
            }
        });

        if (productDTO.getProductPhotos() != null && productDTO.getProductPhotos().size() > 0) {
            mImageFetcher.loadImage(productDTO.getProductPhotos().get(0).getPhotoFile(), productImage);
        }


    }


    protected Double getProductGoldPrice(Integer goldTypeId, Float weight, Integer orderTypeId) {
        Double cost;
        Double goldCost = JewelryApplication.instance().getGoldCost() / JewelryMobileConstant.GOLD_UNIT
                + JewelryMobileConstant.GOLD_TRANSPORT_PRICE;

        if (goldTypeId == 1) {
            cost = goldCost / JewelryMobileConstant.GOLD_TYPE_CARAT_1;
        } else {
            cost = goldCost / JewelryMobileConstant.GOLD_TYPE_CARAT_0;
        }
        if (orderTypeId == 0) {
            return (cost + JewelryApplication.instance().getWholesale()) * weight;
        } else {
            return (cost + JewelryApplication.instance().getRetail()) * weight;
        }
    }

    protected void onCreateStoneList(LinearLayout stoneList,List<StoneOrderDTO> stoneOrders) {
        adapter.setRowItems(stoneOrders);
        final int adapterCount = adapter.getCount();
        for (int i = 0; i < adapterCount; i++) {
            View item = adapter.getView(i, null, null);
            stoneList.addView(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.edit_actions, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_save_product:
                saveOrder();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void saveOrder(){
        if(JewelryApplication.instance().isLoggedInUserAdmin()){
            mOrder.setDone(Integer.parseInt(doneText.getText().toString()));
            mOrder.setDescription(mDescription.getText().toString());
            mOrder.setUpdateDate(DateTime.now());
            if(mOrder.getId()!= null){
                List<OrderDTO> orders = new ArrayList<OrderDTO>();
                orders.add(mOrder);

                AsyncTaskHandler<ValueDTO<List<OrderDTO>>> asyncUpdateTaskHandler = new GenericAsyncTaskHandler<ValueDTO<List<OrderDTO>>>() {

                    @Override
                    public void handleError(ValueDTO<List<OrderDTO>> result) {
                        OrderTemp orderTemp = new OrderTemp();
                        orderTemp.setOrderDTO(mOrder);
                        if(mTempId != 0 ){
                            orderTemp.setId(mTempId);
                        }

                        mProductDAOHelper.saveTempOrder(orderTemp);
                       // hideProgress();
                        negative.show(getFragmentManager(), "Tag");

                    }

                    @Override
                    public void handle(ValueDTO<List<OrderDTO>> result) {
                        mProductDAOHelper.saveOrderList(result.getValue());
                        if(mTempId != 0 ){
                            mProductDAOHelper.deleteOrderTemp(mTempId);
                            //hideProgress();
                            positive.show(getFragmentManager(), "Tag");
                        }else{
                          hideProgress();
                            OrderDetailFragment.this.setResult(RESULT_OK);
                            OrderDetailFragment.this.finish();
                        }
                    }
                };
                UpdateOrderAsyncTask orderUpdateAsyncTask = new UpdateOrderAsyncTask(asyncUpdateTaskHandler,orders);
                orderUpdateAsyncTask.execute();
                showProgress();

            }else{
                AsyncTaskHandler<ValueDTO<OrderDTO>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<OrderDTO>>() {
                    @Override
                    public void handle(ValueDTO<OrderDTO> result) {
                        hideProgress();
                        mProductDAOHelper.saveOrder(result.getValue());
                        if(mTempId != 0 ){
                            mProductDAOHelper.deleteOrderTemp(mTempId);
                            positive.show(getFragmentManager(), "Tag");
                        }
                    }
                    @Override
                    public void handleError(ValueDTO<OrderDTO> result) {
                        hideProgress();
                        OrderTemp orderTemp = new OrderTemp();
                        orderTemp.setOrderDTO(mOrder);
                        orderTemp.setId(mTempId);
                        mProductDAOHelper.saveTempOrder(orderTemp);
                        negative.show(getFragmentManager(), "Tag");
                    }
                };
                AddOrderAsyncTask addOrderAsyncTask = new AddOrderAsyncTask(asyncTaskHandler, mOrder);
                addOrderAsyncTask.execute();
            //    showProgress();
            }


        }
    }

    private class ResultDialog extends DialogFragment {
        int mRes;

        public ResultDialog(int res) {
            mRes = res;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setIcon(android.R.drawable.ic_dialog_info).setTitle(mRes).
                    setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            OrderDetailFragment.this.setResult(RESULT_OK);
                            OrderDetailFragment.this.finish();
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }

    public void hideProgress() {
        progressBarLayout.setVisibility(View.GONE);
    }

    public void showProgress() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }
}