package com.jewelry.android.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class ProductDetailsListFragment extends FragmentActivity {
    public static final String ARG_PRODUCT_IDS = "productIds";
    public static final String ARG_POSITION = "position";
    /**
     * The number of pages (wizard steps) to show in this demo.
     */

    private List<Integer> mProductIds = new ArrayList<Integer>();
    private int mPosition;
    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_slide);


        if (getIntent().getSerializableExtra(ARG_PRODUCT_IDS) != null) {
            mProductIds = (List<Integer>) getIntent().getSerializableExtra(ARG_PRODUCT_IDS);
        }
        mPosition = getIntent().getIntExtra(ARG_POSITION, 0);

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When changing pages, reset the action bar actions since they are dependent
                // on which page is currently active. An alternative approach is to have each
                // fragment expose actions itself (rather than the activity exposing actions),
                // but for simplicity, the activity provides the actions in this sample.
                invalidateOptionsMenu();
            }
        });


        mPager.setCurrentItem(mPosition);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.activity_screen_slide, menu);

        menu.findItem(R.id.action_previous).setEnabled(mPager.getCurrentItem() > 0);

        // Add either a "next" or "finish" button to the action bar, depending on which page
        // is currently selected.
        MenuItem item = menu.add(Menu.NONE, R.id.action_next, Menu.NONE, R.string.menu_action_next);
        item.setEnabled(mPager.getCurrentItem() != mPagerAdapter.getCount() - 1);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        if (JewelryApplication.instance().isLoggedInUserAdmin()) {
            item = menu.add(Menu.NONE, R.id.action_edit, Menu.NONE, R.string.menu_action_edit);
            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;

            case R.id.action_previous:
                // Go to the previous step in the wizard. If there is no previous step,
                // setCurrentItem will do nothing.
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                return true;

            case R.id.action_next:
                // Advance to the next step in the wizard. If there is no next step, setCurrentItem
                // will do nothing.
                mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                return true;
            case R.id.action_edit:
                Intent intent = new Intent(this, EditProductActivity.class);
                intent.putExtra(EditProductActivity.ARG_PRODUCT_ID, mProductIds.get(mPager.getCurrentItem()));
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A simple pager adapter that represents 5 {@link ProductDetailFragment} objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ProductDetailFragment.create(mProductIds.get(position));
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ProductDetailFragment view = (ProductDetailFragment) object;
            (container).removeView(view.getProductImage());
            view = null;
            super.destroyItem(container, position, object);
        }

        @Override
        public int getCount() {
            return mProductIds.size();
        }
    }
}
