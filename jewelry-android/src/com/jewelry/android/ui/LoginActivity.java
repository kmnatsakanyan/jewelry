package com.jewelry.android.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.client.Client;
import com.jewelry.android.service.NotificationService;
import com.jewelry.android.task.AsyncTaskHandler;
import com.jewelry.android.task.GenericAsyncTaskHandler;
import com.jewelry.android.task.LoginAsyncTask;
import com.jewelry.android.util.IOUtils;
import com.jewelry.android.util.JewelryMobileConstant;
import com.jewelry.dto.UserDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.util.JewelryConstant;

public class LoginActivity extends Activity {

    EditText mEmail;

    EditText mPassword;

    Button loginBtn;

    ProgressBar progressBar;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        if (!JewelryApplication.instance().isMyServiceRunning(NotificationService.class)) {
            startService(new Intent(this, NotificationService.class));
        }

        //check if user logged in than start main activity
        if (JewelryApplication.instance().getLoggedInUser() != null) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }
        super.onCreate(savedInstanceState);
        //Setting up location for armenian language
        setContentView(R.layout.login);


        mEmail = (EditText) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);
        loginBtn = (Button) findViewById(R.id.login);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AsyncTaskHandler<ValueDTO<UserDTO>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<UserDTO>>() {
                    @Override
                    public void handle(ValueDTO<UserDTO> loggedInUser) {
                        progressBar.setVisibility(View.GONE);
                        loginBtn.setVisibility(View.VISIBLE);
                        IOUtils.storeObjectToPreferences(JewelryApplication.instance(), JewelryMobileConstant.KEY_PREF_LOGGED_IN_USER, loggedInUser.getValue());
                        JewelryApplication.instance().setLoggedInUser(loggedInUser.getValue());
                        Client.reconfigure(loggedInUser.getValue());
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void handleError(ValueDTO<UserDTO> result) {
                        progressBar.setVisibility(View.GONE);
                        loginBtn.setVisibility(View.VISIBLE);
                        switch (result.getResultCode()) {
                            case JewelryConstant.RESPONSE_UNAUTHORIZED:
                                Toast.makeText(JewelryApplication.instance(), getResources().getString(R.string.error_unauthorized_user), Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                super.handleError(result);
                        }
                    }
                };
                LoginAsyncTask loginAsyncTask = new LoginAsyncTask(mEmail.getText().toString(), mPassword.getText().toString(), asyncTaskHandler);
                loginAsyncTask.execute();
                progressBar.setVisibility(View.VISIBLE);
                loginBtn.setVisibility(View.GONE);

            }
        });
    }
}
