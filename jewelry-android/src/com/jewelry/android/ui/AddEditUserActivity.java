package com.jewelry.android.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.jewelry.android.R;
import com.jewelry.android.task.*;
import com.jewelry.dto.UserDTO;
import com.jewelry.dto.UserRole;
import com.jewelry.dto.ValueDTO;
import com.jewelry.util.JewelryConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class AddEditUserActivity extends FragmentActivity {

    public static final String ARG_USER = "user";

    LinearLayout progressBarLayout;
    TextView mLoginName;
    TextView mPassword;
    TextView mfName;
    TextView mlName;
    private Spinner mSpinner;
    private UserDTO mUser = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        mLoginName = (TextView) findViewById(R.id.login);
        mPassword = (TextView) findViewById(R.id.password);
        mSpinner = (Spinner) findViewById(R.id.role);
        mfName = (TextView) findViewById(R.id.fName);
        mlName = (TextView) findViewById(R.id.lName);
        progressBarLayout = (LinearLayout) findViewById(R.id.progressLayout);
        onCreateRoleSpinner();
        mUser = (UserDTO)getIntent().getSerializableExtra(ARG_USER);
      if(mUser!=null){
          mLoginName.setText(mUser.getEmail());
          mfName.setText(mUser.getFirstname());
          mlName.setText(mUser.getLastname());
          mPassword.setText(mUser.getPassword());
          setRoleToSpinner(mUser.getRole());
      }
    }

    private void onCreateRoleSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, getRolesStr());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
    }

    private void saveUser() {
        String login = mLoginName.getText().toString().trim();
        String password = mPassword.getText().toString().trim();
        String fName = mfName.getText().toString().trim();
        String lName = mlName.getText().toString().trim();
        boolean validate = true;

        if (login.isEmpty()) {
            mLoginName.setError(getString(R.string.validation_login));
            validate = false;
        }

        if (password.isEmpty()) {
            mPassword.setError(getString(R.string.validation_password));
            validate = false;
        }

        if (fName.isEmpty()) {
            mfName.setError(getString(R.string.validation_first_name));
            validate = false;
        }

        if (lName.isEmpty()) {
            mlName.setError(getString(R.string.validation_last_name));
            validate = false;
        }
        if (validate) {
            UserDTO userDTO;
            if(mUser != null){
                userDTO = mUser;
            }else{
                userDTO = new UserDTO();
            }
            userDTO.setEmail(login);
            userDTO.setFirstname(fName);
            userDTO.setLastname(lName);
            userDTO.setPassword(password);
            userDTO.setRole(getRoleFromSpinner());
            AsyncTaskHandler<ValueDTO<UserDTO>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<UserDTO>>() {

                @Override
                public void handle(ValueDTO<UserDTO> result) {
                    hideProgress();
                    AddEditUserActivity.this.setResult(RESULT_OK);
                    AddEditUserActivity.this.finish();
                }

                @Override
                public void handleError(ValueDTO<UserDTO> result) {
                    hideProgress();
                    switch (result.getResultCode()) {
                        case JewelryConstant.RESPONSE_USER_EXIST:
                            mLoginName.setError(getString(R.string.validation_login_exist));
                            break;
                        default:
                            super.handleError(result);
                    }
                }
            };
            AddUserAsyncTask addUserAsyncTask = new AddUserAsyncTask(asyncTaskHandler, userDTO);
            addUserAsyncTask.execute();
            showProgress();
        }
    }

    private List<String> getRolesStr() {
        List<String> spinnerItems = new ArrayList<String>();
        for (UserRole role : UserRole.values()) {
            if (role == UserRole.ROLE_ADMIN) {
                spinnerItems.add(getString(R.string.admin));
            } else if (role == UserRole.ROLE_SELLER) {
                spinnerItems.add(getString(R.string.seller));
            }else if (role == UserRole.ROLE_SHOP) {
                spinnerItems.add(getString(R.string.shop));
            }else if (role == UserRole.ROLE_CANDY) {
                spinnerItems.add(getString(R.string.candy));
            }
        }
        return spinnerItems;
    }

    private UserRole getRoleFromSpinner() {
        UserRole role = UserRole.ROLE_SELLER;
        if (mSpinner.getSelectedItemPosition() == 0) {
            role = UserRole.ROLE_ADMIN;
        }
        if (mSpinner.getSelectedItemPosition() == 1) {
            role = UserRole.ROLE_SELLER;
        }
        if (mSpinner.getSelectedItemPosition() == 2) {
            role = UserRole.ROLE_SHOP;
        }
        if (mSpinner.getSelectedItemPosition() == 3) {
            role = UserRole.ROLE_CANDY;
        }
        return role;
    }

    private void setRoleToSpinner(UserRole role){
        if (role == UserRole.ROLE_ADMIN) {
            mSpinner.setSelection(0);
        } else if (role == UserRole.ROLE_SELLER) {
            mSpinner.setSelection(1);
        }else if (role == UserRole.ROLE_SHOP) {
            mSpinner.setSelection(2);
        }else if (role == UserRole.ROLE_CANDY) {
            mSpinner.setSelection(3);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_save_product:
                saveUser();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void hideProgress() {
        progressBarLayout.setVisibility(View.GONE);
    }

    public void showProgress() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }
}
