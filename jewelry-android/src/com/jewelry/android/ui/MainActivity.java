package com.jewelry.android.ui;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.ui.component.NavDrawerItem;
import com.jewelry.android.ui.component.NavDrawerListAdapter;
import com.jewelry.android.util.JewelryMobileConstant;
import com.jewelry.dto.UserRole;

import java.util.ArrayList;

/**
 * User: karen.mnatsakanyan
 */
public class MainActivity extends FragmentActivity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private ProductDAOHelper dao = JewelryApplication.DAO;
    private Long lastUpdate;
    // used to store app title
    private CharSequence mTitle;

    // slide menu items
    private String[] navMenuTitles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        if (JewelryApplication.instance().getLoggedInUser() != null) {

            lastUpdate =  JewelryApplication.instance().getPrefs().getLong(JewelryMobileConstant.PREFS_LAST_UPDATE_TIMESTAMP, 0);
            boolean shouldUpdate= (System.currentTimeMillis()-lastUpdate)>JewelryMobileConstant.ONE_DAY_IN_MILLISECONDS;
            mTitle = getTitle();

            // load slide menu items
            navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

            // nav drawer icons from resources
            TypedArray navMenuIcons = getResources()
                    .obtainTypedArray(R.array.nav_drawer_icons);

            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

            ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<NavDrawerItem>(10);

            // adding nav drawer items to array
            final NavDrawerItem mDrawerTitle = new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1), true, dao.getTempProductListCount() + "");

            navDrawerItems.add(0, new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
            navDrawerItems.add(1, new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
            if (JewelryApplication.instance().getLoggedInUser().getRole() != UserRole.ROLE_CANDY) {
                navDrawerItems.add(2, new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
                navDrawerItems.add(3, new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
                navDrawerItems.add(4, mDrawerTitle);
            } else {
                navDrawerItems.add(2, null);
                navDrawerItems.add(3, null);
                navDrawerItems.add(4, null);
            }

            if (JewelryApplication.instance().isLoggedInUserAdmin()) {
                navDrawerItems.add(5, new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
                navDrawerItems.add(6, new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));
            } else {
                navDrawerItems.add(5, null);
                navDrawerItems.add(6, null);
            }

            navDrawerItems.add(7, new NavDrawerItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1)));

            navDrawerItems.add(8, new NavDrawerItem(navMenuTitles[8], navMenuIcons.getResourceId(8, -1)));

            mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

            // Recycle the typed array
            navMenuIcons.recycle();

            // setting the nav drawer list mAdapter
            NavDrawerListAdapter adapter = new NavDrawerListAdapter(getApplicationContext(),
                    navDrawerItems);
            mDrawerList.setAdapter(adapter);

            // enabling action bar app icon and behaving it as toggle button
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setHomeButtonEnabled(true);

            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                    R.drawable.ic_drawer, //nav menu toggle icon
                    R.string.empty, // nav drawer open - description for accessibility
                    R.string.empty // nav drawer close - description for accessibility
            ) {
                public void onDrawerClosed(View view) {
                    getActionBar().setTitle(mTitle);
                    // calling onPrepareOptionsMenu() to show action bar icons
                    invalidateOptionsMenu();
                    mDrawerTitle.setCount(dao.getTempProductListCount() + "");
                }

                public void onDrawerOpened(View drawerView) {
                    getActionBar().setTitle(mTitle);
                    // calling onPrepareOptionsMenu() to hide action bar icons
                    invalidateOptionsMenu();
                    mDrawerTitle.setCount(dao.getTempProductListCount() + "");
                }
            };
            mDrawerLayout.setDrawerListener(mDrawerToggle);

            int notificationId = getIntent().getIntExtra("notificationId", Integer.MAX_VALUE);
            if (notificationId != Integer.MAX_VALUE) {
                displayView(notificationId);
            } else if (savedInstanceState == null && shouldUpdate) {
                displayView(9);
            } else {
                // on first time display view for first nav item
                displayView(0);
            }
        } else {
            finish();
        }
    }


    /**
     * Slide menu item click listener
     */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     */
    private void displayView(int position) {
        // update the main content by replacing fragments
        Log.d("displayView", position + "");
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new ProductListFragment();
                break;
            case 1:
                fragment = new OrderListFragment();
                break;
            case 2:
                if (JewelryApplication.instance().getLoggedInUser().getRole() != UserRole.ROLE_CANDY) {
                    fragment = new GoldFragment();
                } else {
                    fragment = new UserListFragment();

                }
                break;
            case 3:
                if (JewelryApplication.instance().getLoggedInUser().getRole() != UserRole.ROLE_CANDY) {
                    fragment = new StonePriceFragment();
                } else {
                    mDrawerList.setItemChecked(position, true);
                    mDrawerList.setSelection(position);
                    logOutAndEraseAllData();
                }
                break;
            case 4:
                fragment = new ProductUpdatesFragment();
                break;
            case 5:
                if (JewelryApplication.instance().isLoggedInUserAdmin()) {
                    fragment = new StoneSettingsFragment();
                } else {
                    mDrawerList.setItemChecked(position, true);
                    mDrawerList.setSelection(position);
                    logOutAndEraseAllData();
                }
                break;
            case 6:
                if (JewelryApplication.instance().isLoggedInUserAdmin()) {
                    fragment = new CatalogsFragment();
                }
                break;
            case 7:
                fragment = new UserListFragment();
                break;
            case 8:
                mDrawerList.setItemChecked(position, true);
                mDrawerList.setSelection(position);
                logOutAndEraseAllData();
                break;
            case 9:
                fragment = new ProductListFragment(true);
                position = 0;
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(navMenuTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    private void logOutAndEraseAllData() {
        SharedPreferences.Editor editor = this.getSharedPreferences(JewelryMobileConstant.PREFS_CONFIG_FILE_NANE, Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();

        editor = this.getSharedPreferences(getApplicationContext().getPackageName(), Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();

        JewelryApplication.instance().setLoggedInUser(null);

        dao.clearAllData();
        Intent it = new Intent(this, LoginActivity.class);
        startActivity(it);
        finish();
    }


    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }


    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}