package com.jewelry.android.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.domain.OrderTemp;
import com.jewelry.android.domain.Photo;
import com.jewelry.android.task.AddOrderAsyncTask;
import com.jewelry.android.task.AsyncTaskHandler;
import com.jewelry.android.task.GenericAsyncTaskHandler;
import com.jewelry.android.ui.component.GoldType;
import com.jewelry.android.ui.component.OrderType;
import com.jewelry.android.ui.component.StoneAdapter;
import com.jewelry.android.util.BitmapUtils;
import com.jewelry.android.util.JewelryMobileConstant;
import com.jewelry.dto.*;
import com.jewelry.util.JewelryConstant;
import org.joda.time.DateTime;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProductDetailFragment extends BaseFragment {
    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PRODUCT_ID = "productId";

    private TextView mTextWeight;

    private TextView mTextGoldPrice;

    private Spinner mSpinnerGold;

    private Spinner mOrderType;

    private EditText mDescription;

    private Button mOrder;

    private EditText mOrderCount;

    private final ProductDAOHelper mProductDAOHelper = JewelryApplication.DAO;

    private ProductDTO productDTO;

    private StoneAdapter adapter;

    private TextView stoneDetail;

    private ImageView productImage;

    private RelativeLayout content;

    private ResultDialog positive = new ResultDialog(R.string.order_added);
    private ResultDialog negative = new ResultDialog(R.string.order_saved_to_draft);

    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     */
    public static ProductDetailFragment create(int productId) {
        ProductDetailFragment fragment = new ProductDetailFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PRODUCT_ID, productId);
        fragment.setArguments(args);
        return fragment;
    }

    public ProductDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int mProductId = getArguments().getInt(ARG_PRODUCT_ID);
        productDTO = mProductDAOHelper.getProduct(mProductId);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout containing a title and body text.
        ViewGroup rootView = (ViewGroup) inflater
                .inflate(R.layout.fragment_product_details, container, false);

        TextView textView = (TextView) rootView.findViewById(R.id.productId);
        textView.requestFocus();
        mTextWeight = ((TextView) rootView.findViewById(R.id.weight));
        mTextGoldPrice = ((TextView) rootView.findViewById(R.id.price));
        mSpinnerGold = (Spinner) rootView.findViewById(R.id.gold_type);
        mOrderType = (Spinner) rootView.findViewById(R.id.order_type);
        mOrder = (Button) rootView.findViewById(R.id.order);
        Button mSum = (Button) rootView.findViewById(R.id.sum);
        mDescription = (EditText) rootView.findViewById(R.id.description);
        mOrderCount = (EditText) rootView.findViewById(R.id.count);

        final TextView priceSum = (TextView) rootView.findViewById(R.id.price_sum);
        LinearLayout stoneListView = (LinearLayout) rootView.findViewById(R.id.product_stone_list);
        stoneDetail = (TextView) rootView.findViewById(R.id.product_stone_detail);
        content = (RelativeLayout) rootView.findViewById(R.id.content);

        onCreateOrderClickListener();

        mSum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<Integer, Float> priceMap = adapter.getPriceMap();
                Float sum = 0f;
                if (priceMap.size() != 0) {
                    List<Float> priceList = new ArrayList<Float>(priceMap.values());
                    for (Float price : priceList) {
                        sum += price;
                    }
                }
                String gold = mTextGoldPrice.getText().toString();
                Float goldPrice = Float.parseFloat(gold);
                sum += goldPrice;
                if (JewelryApplication.instance().getLoggedInUser().getRole() == UserRole.ROLE_SHOP) {
                    sum += Float.parseFloat(JewelryApplication.instance().getShop().toString());
                }
                String count = mOrderCount.getText().toString();
                if (!count.equals("")) {
                    sum*=Integer.parseInt(count);

                }
                priceSum.setText(sum.toString());
            }
        });

        textView.setText(productDTO.getProductId());
        mTextWeight.setText(productDTO.getWeight().toString());
        onCreateGoldSpinner();
        onCreateOrderTypeSpinner();
        if(JewelryApplication.instance().getLoggedInUser().getRole()!= UserRole.ROLE_CANDY){
            mTextGoldPrice.setText(String.format("%.2f", getProductGoldPrice()));
        }
        onCreateStoneList(stoneListView);
        productImage = (ImageView) rootView.findViewById(R.id.productImage);
        productImage.setImageDrawable(getResources().getDrawable(R.drawable.empty_photo));
        productImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), FullScreenViewActivity.class);
                ArrayList<Photo> photos = new ArrayList<Photo>();
                for (ProductPhotoDTO productPhotoDTO : productDTO.getProductPhotos()) {
                    Photo photo = new Photo();
                    photo.setFilePath(BitmapUtils.getAlbumStorageDir() + File.separator + productPhotoDTO.getPhotoFile());
                    photos.add(photo);
                }
                i.putExtra(FullScreenViewActivity.ARG_PRODUCT_PHOTOS, photos);
                startActivity(i);
            }
        });

        if (productDTO.getProductPhotos() != null && productDTO.getProductPhotos().size() > 0) {
            mImageFetcher.loadImage(productDTO.getProductPhotos().get(0).getPhotoFile(), productImage);
        }

        if (JewelryApplication.instance().getLoggedInUser().getRole() == UserRole.ROLE_CANDY) {
            rootView.findViewById(R.id.product_price_layout).setVisibility(View.INVISIBLE);
        }

        initProgressBar(rootView);
        return rootView;
    }

    protected void onCreateOrderTypeSpinner() {
        List<String> spinnerItems = new ArrayList<String>();
        spinnerItems.addAll(OrderType.getNameList());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_spinner_item, spinnerItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mOrderType.setAdapter(adapter);

        mOrderType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                                       int pos, long arg3) {
                mTextGoldPrice.setText(String.format("%.2f", getProductGoldPrice()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }


    protected void onCreateGoldSpinner() {

        List<String> spinnerItems = new ArrayList<String>();
        spinnerItems.addAll(GoldType.getNameList());

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_spinner_item, spinnerItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerGold.setAdapter(adapter);


        mSpinnerGold.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View arg1,
                                       int pos, long arg3) {
                Float weight;
                if (pos == 1) {
                    weight = productDTO.getWeight() * JewelryMobileConstant.GOLD_TYPE_RATE;
                } else {
                    weight = productDTO.getWeight();
                }
                mTextWeight.setText(String.format("%.2f", weight) + " " + getString(R.string.weight_unit));
                mTextGoldPrice.setText(String.format("%.2f", getProductGoldPrice()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    protected Double getProductGoldPrice() {
        Double cost;
        Float weight;

        //Float workPriceForGram =
        Double goldCost = JewelryApplication.instance().getGoldCost() / JewelryMobileConstant.GOLD_UNIT
                + JewelryMobileConstant.GOLD_TRANSPORT_PRICE;

        if (mSpinnerGold.getSelectedItemPosition() == 1) {
            cost = goldCost / JewelryMobileConstant.GOLD_TYPE_CARAT_1;
            weight = productDTO.getWeight() * JewelryMobileConstant.GOLD_TYPE_RATE;
        } else {
            cost = goldCost / JewelryMobileConstant.GOLD_TYPE_CARAT_0;
            weight = productDTO.getWeight();
        }

        if (mOrderType.getSelectedItemPosition() == 0) {
            return (cost + JewelryApplication.instance().getWholesale()) * weight;
        } else {
            return (cost + JewelryApplication.instance().getRetail()) * weight;
        }
    }

    protected void onCreateStoneList(LinearLayout stoneList) {
        adapter = new StoneAdapter(this.getActivity());
        adapter.setRowItems(productDTO.getProductStones());
        final int adapterCount = adapter.getCount();
        if (adapterCount > 0) {
            stoneDetail.setVisibility(View.VISIBLE);
        }
        for (int i = 0; i < adapterCount; i++) {
            View item = adapter.getView(i, null, null);
            stoneList.addView(item);
        }
    }

    protected void onCreateOrderClickListener() {
        mOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<StoneOrderDTO> stoneOrders = adapter.getStoneOrders();
                final OrderDTO order = new OrderDTO();
                order.setProduct(productDTO);
                order.setGoldType(com.jewelry.dto.GoldType.values()[mSpinnerGold.getSelectedItemPosition()]);
                order.setStones(stoneOrders);
                order.setUpdateDate(DateTime.now());
                order.setDate(DateTime.now());
                order.setDone(0);
                order.setUser(JewelryApplication.instance().getLoggedInUser());
                order.setOrderType(com.jewelry.dto.OrderType.values()[mOrderType.getSelectedItemPosition()]);
                order.setDescription(mDescription.getText().toString());

                String count = mOrderCount.getText().toString();
                if (count.equals("")) {
                    order.setCount(1);
                } else {
                    order.setCount(Integer.parseInt(count));
                }

                AsyncTaskHandler<ValueDTO<OrderDTO>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<OrderDTO>>() {
                    @Override
                    public void handle(ValueDTO<OrderDTO> result) {
                        List<OrderDTO> orderList = new ArrayList<OrderDTO>();
                        orderList.add(result.getValue());
                        mProductDAOHelper.saveOrderList(orderList);
                        hideProgress();
                        content.setVisibility(View.VISIBLE);
                        positive.show(getFragmentManager(), "Tag");
                    }

                    @Override
                    public void handleError(ValueDTO<OrderDTO> result) {
                        OrderTemp orderTemp = new OrderTemp();
                        orderTemp.setOrderDTO(order);
                        mProductDAOHelper.saveTempOrder(orderTemp);
                        hideProgress();

                        content.setVisibility(View.VISIBLE);
                        switch (result.getResultCode()) {
                            case JewelryConstant.RESPONSE_UNAUTHORIZED:
                                Toast.makeText(JewelryApplication.instance(), getResources().getString(R.string.error_unauthorized_user), Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                super.handleError(result);
                        }
                        negative.show(getFragmentManager(), "Tag");
                    }
                };
                AddOrderAsyncTask addOrderAsyncTask = new AddOrderAsyncTask(asyncTaskHandler, order);
                addOrderAsyncTask.execute();
                content.setVisibility(View.GONE);
                showProgress();

            }
        });
    }

    public ImageView getProductImage() {
        return productImage;
    }


    private class ResultDialog extends DialogFragment {
        int mRes;

        public ResultDialog(int res) {
            mRes = res;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setIcon(android.R.drawable.ic_dialog_info).setTitle(mRes).
                    setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }
}