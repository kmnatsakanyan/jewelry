package com.jewelry.android.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.*;
import android.widget.*;
import com.jewelry.android.task.*;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.domain.OrderTemp;
import com.jewelry.android.ui.component.OrderListAdapter;
import com.jewelry.android.ui.component.OrderTempListAdapter;
import com.jewelry.dto.OrderDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.util.JewelryConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * User:aramayis.antonyan
 */
public class OrderListFragment extends BaseFragment {

    private static final int DELETE_ORDER_MENU_ITEM = 0;
    ProductDAOHelper productDAOHelper= JewelryApplication.DAO;
    OrderListAdapter mAdapter;
    OrderTempListAdapter mTempAdapter;
    ListView mListView;
    Spinner mSpinnerStatus;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_order_list, container, false);
        mListView = (ListView) rootView.findViewById(R.id.order_list);
        setHasOptionsMenu(true);
        registerForContextMenu(mListView);


        List<OrderDTO> rowItems = productDAOHelper.getOrders();
        mAdapter = new OrderListAdapter((FragmentActivity)this.getActivity());
        mTempAdapter = new OrderTempListAdapter((FragmentActivity)this.getActivity());

        mAdapter.setRowItems(rowItems);
        initProgressBar(rootView);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.order_list_actions, menu);
        onCreateSpinner(menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    protected void onCreateSpinner(Menu menu) {

        List<String> spinnerItems = new ArrayList<String>();
        spinnerItems.add(getString(R.string.order_submit));
        spinnerItems.add(getString(R.string.order_draft));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(),
                android.R.layout.simple_spinner_item, spinnerItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        MenuItem spinnerItem = menu.findItem(R.id.action_spinner);
        View view = spinnerItem.getActionView();
        if (view instanceof Spinner) {
            mSpinnerStatus = (Spinner) view;
            mSpinnerStatus.setAdapter(adapter);

            mSpinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View arg1,
                                           int pos, long arg3) {
                    if (pos == 0) {
                        mAdapter.setRowItems(productDAOHelper.getOrders());
                        mListView.setAdapter(mAdapter);
                        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position,
                                                    long id) {
                                Intent intent = new Intent(getActivity(), OrderDetailFragment.class);
                                Bundle mBundle = new Bundle();
                                mBundle.putSerializable(OrderDetailFragment.ARG_ORDER,(OrderDTO)mAdapter.getItem(position));
                                intent.putExtras(mBundle);
                                startActivityForResult(intent, 0);
                            }
                        });
                    } else {
                        mTempAdapter.setRowItems(productDAOHelper.getTempOrderList());
                        mListView.setAdapter(mTempAdapter);
                        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position,
                                                    long id) {
                                Intent intent = new Intent(getActivity(), OrderDetailFragment.class);
                                Bundle mBundle = new Bundle();
                                OrderTemp orderTemp =  (OrderTemp) mTempAdapter.getItem(position);
                                mBundle.putSerializable(OrderDetailFragment.ARG_ORDER, orderTemp.getOrderDTO());
                                mBundle.putInt(OrderDetailFragment.ARG_TEMP_ID,orderTemp.getId());
                                intent.putExtras(mBundle);
                                startActivityForResult(intent, 0);
                            }
                        });

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }

            });
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.order_list) {
            menu.add(0, DELETE_ORDER_MENU_ITEM, 0, getResources().getString(R.string.menu_delete));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case DELETE_ORDER_MENU_ITEM:
                AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                int position = acmi.position;
                if(mSpinnerStatus.getSelectedItemPosition()==0){
                    if(JewelryApplication.instance().isLoggedInUserAdmin()){
                    OrderDTO order = (OrderDTO)mAdapter.getItem(position);
                    final Integer orderId = order.getId();
                    AsyncTaskHandler<ValueDTO<Boolean>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<Boolean>>() {
                        @Override
                        public void handle(ValueDTO<Boolean> result) {
                            if (result.getValue()) {
                               productDAOHelper.deleteOrder(orderId);
                               mAdapter.setRowItems(productDAOHelper.getOrders());
                               mAdapter.notifyDataSetChanged();
                            }
                        }
                        @Override
                        public void handleError(ValueDTO<Boolean> result) {
                            switch (result.getResultCode()) {
                                case JewelryConstant.RESPONSE_CATALOG_NOT_FOUND:
                                    Toast.makeText(JewelryApplication.instance(), getResources().getString(R.string.error_user_not_found), Toast.LENGTH_SHORT).show();
                                    break;
                                default:
                                    super.handleError(result);
                            }
                        }
                    };
                    DeleteOrderAsyncTask deleteUserAsyncTask = new DeleteOrderAsyncTask(asyncTaskHandler, orderId);
                    deleteUserAsyncTask.execute();
                    return true;
                    }
                }
                else{
                    OrderTemp orderTemp = (OrderTemp)mTempAdapter.getItem(position);
                    productDAOHelper.deleteOrderTemp(orderTemp.getId());
                    Integer orderId = orderTemp.getOrderDTO().getId();
                    if(orderId != null){
                        productDAOHelper.setOrderIsDeleted(orderId,false);
                    }
                    mTempAdapter.setRowItems(productDAOHelper.getTempOrderList());
                    mTempAdapter.notifyDataSetChanged();
                }


        default:
        return super.onContextItemSelected(item);

        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==0){
            if(resultCode == Activity.RESULT_OK){
                mAdapter.setRowItems(productDAOHelper.getOrders());
                mAdapter.notifyDataSetChanged();
                mTempAdapter.setRowItems(productDAOHelper.getTempOrderList());
                mTempAdapter.notifyDataSetChanged();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:

                if(mSpinnerStatus.getSelectedItemPosition()==0){
                    AsyncTaskHandler<ValueDTO<List<OrderDTO>>> asyncTaskHandler = new GenericAsyncTaskHandler<ValueDTO<List<OrderDTO>>>() {

                        @Override
                        public void handleError(ValueDTO<List<OrderDTO>> result) {
                            hideProgress();
                            super.handleError(result);
                        }

                        @Override
                        public void handle(ValueDTO<List<OrderDTO>> result) {
                            if (result != null && result.getValue() != null && result.getValue().size() > 0) {
                                List<OrderDTO> rowItems = productDAOHelper.getOrders();
                                mAdapter.setRowItems(rowItems);
                                mAdapter.notifyDataSetChanged();
                            }
                            hideProgress();
                        }
                    };
                    GetOrdersAsyncTask productAsyncTask = new GetOrdersAsyncTask(asyncTaskHandler);
                    productAsyncTask.execute();
                    showProgress();

                }
                else{

                    AsyncTaskHandler<ValueDTO<List<OrderDTO>>> asyncUpdateTaskHandler = new GenericAsyncTaskHandler<ValueDTO<List<OrderDTO>>>() {

                    @Override
                    public void handleError(ValueDTO<List<OrderDTO>> result) {
                        hideProgress();
                        super.handleError(result);
                    }

                    @Override
                    public void handle(ValueDTO<List<OrderDTO>> result) {
                        productDAOHelper.saveOrderList(result.getValue());
                        productDAOHelper.deleteAllTempOrder();
                        hideProgress();
                    }
                    };
                    UpdateOrderAsyncTask orderUpdateAsyncTask = new UpdateOrderAsyncTask(asyncUpdateTaskHandler,mTempAdapter.getOrderRowItems());
                    orderUpdateAsyncTask.execute();
                    showProgress();
                }
                 return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
