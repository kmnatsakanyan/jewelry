package com.jewelry.android.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.jewelry.android.R;
import com.jewelry.android.bitmap.util.ImageCache;
import com.jewelry.android.bitmap.util.ImageFetcher;

/**
 * User: karen.mnatsakanyan
 */
public class BaseFragment extends Fragment {
    private static final String IMAGE_CACHE_DIR = "images";

    protected ImageFetcher mImageFetcher;

    LinearLayout progressBarLayout;
    TextView progress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        // For this sample we'll use half of the longest width to resize our images. As the
        // image scaling ensures the image is larger than this, we should be left with a
        // resolution that is appropriate for both portrait and landscape. For best image quality
        // we shouldn't divide by 2, but this will use more memory and require a larger memory
        // cache.
        final int longest = (height > width ? height : width) / 2;

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this.getActivity(), IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this.getActivity(), longest);
        mImageFetcher.addImageCache(((FragmentActivity) this.getActivity()).getSupportFragmentManager(), cacheParams);


    }

    public void initProgressBar(View rootView) {
        progressBarLayout = (LinearLayout) rootView.findViewById(R.id.progressLayout);
        progress = (TextView) rootView.findViewById(R.id.percent);
    }

    public void hideProgress() {
        progressBarLayout.setVisibility(View.GONE);
    }

    public void showProgress() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    public void onProgressUpdate(Integer percent) {
        if (progress.getVisibility() != View.VISIBLE) {
            progress.setVisibility(View.VISIBLE);
        }
        progress.setText(percent + "");
    }
}
