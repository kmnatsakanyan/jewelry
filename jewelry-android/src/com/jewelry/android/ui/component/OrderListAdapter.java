package com.jewelry.android.ui.component;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.bitmap.ui.RecyclingImageView;
import com.jewelry.android.bitmap.util.ImageCache;
import com.jewelry.android.bitmap.util.ImageFetcher;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.util.JewelryMobileConstant;
import com.jewelry.dto.OrderDTO;
import com.jewelry.dto.ProductPhotoDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class OrderListAdapter extends BaseAdapter {
    Context context;
    List<OrderDTO> rowItems;
    ProductDAOHelper productDAOHelper = JewelryApplication.DAO;

    private ImageFetcher mImageFetcher;
    private int mImageThumbSize;

    public OrderListAdapter(FragmentActivity context) {
        this.context = context;
        rowItems = new ArrayList<OrderDTO>();

        mImageThumbSize = context.getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(context, JewelryMobileConstant.IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(context, mImageThumbSize);
        mImageFetcher.setLoadingImage(R.drawable.empty_photo);
        mImageFetcher.addImageCache(context.getSupportFragmentManager(), cacheParams);
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }

    public void setRowItems(List<OrderDTO> rowItems) {
        this.rowItems = rowItems;
    }


    public List<OrderDTO> getRowItems() {
        return this.rowItems;
    }


    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView readyCount;
        TextView toBeDoneCount;
        TextView orderOwnerName;
        TextView orderDate;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.order_list_item, null);
            holder = new ViewHolder();
            holder.toBeDoneCount = (TextView) convertView.findViewById(R.id.to_be_done_count);
            holder.readyCount = (TextView) convertView.findViewById(R.id.ready_count);
            holder.orderOwnerName = (TextView) convertView.findViewById(R.id.user_name);
            holder.orderDate = (TextView) convertView.findViewById(R.id.order_date);
            holder.imageView = (RecyclingImageView) convertView.findViewById(R.id.icon);
            holder.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        OrderDTO rowItem = (OrderDTO) getItem(position);


        Integer beDone = rowItem.getCount() - rowItem.getDone();

        holder.toBeDoneCount.setText(beDone.toString());
        holder.readyCount.setText(rowItem.getDone().toString());
        holder.orderOwnerName.setText(rowItem.getUser().getEmail());
        holder.orderDate.setText(rowItem.getDate().toString("dd/MM/yyyy"));
        holder.imageView.setImageBitmap(null);
        if (rowItem.getProduct() != null && rowItem.getProduct().getProductPhotos() != null && rowItem.getProduct().getProductPhotos().size() > 0) {
            String fileName = rowItem.getProduct().getProductPhotos().get(0).getPhotoThumbnailFile();
            mImageFetcher.loadImage(fileName, holder.imageView);
        } else {
            List<ProductPhotoDTO> productPhotoDTOs = productDAOHelper.loadProductPhotosByProductId(rowItem.getId());
            if (productPhotoDTOs != null && productPhotoDTOs.size() > 0 && rowItem.getProduct() != null) {
                rowItem.getProduct().setProductPhotos(productPhotoDTOs);
                String fileName = rowItem.getProduct().getProductPhotos().get(0).getPhotoThumbnailFile();
                mImageFetcher.loadImage(fileName, holder.imageView);
            } else {
                holder.imageView.setImageResource(R.drawable.empty_photo);
            }
        }
        return convertView;
    }
}
