package com.jewelry.android.ui.component;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.jewelry.dto.UserDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class UserListAdapter extends BaseAdapter {
    List<UserDTO> mUsers = new ArrayList<UserDTO>();
    Context mContext;

    public UserListAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return mUsers.size();
    }

    @Override
    public Object getItem(int i) {
        return mUsers.size() > i ? mUsers.get(i) : null;
    }

    @Override
    public long getItemId(int i) {
        return mUsers.get(i).getId();
    }

    public List<UserDTO> getUsers() {
        return mUsers;
    }

    public void setUser(List<UserDTO> mUsers) {
        this.mUsers = mUsers;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(android.R.layout.simple_list_item_1, null);
        }

        TextView name = (TextView) convertView.findViewById(android.R.id.text1);
        UserDTO rowItem = (UserDTO) getItem(position);
        name.setText(rowItem.getEmail());
        name.setTag(rowItem.getId());
        return convertView;
    }
}
