package com.jewelry.android.ui.component;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.jewelry.android.bitmap.ui.RecyclingImageView;
import com.jewelry.android.bitmap.util.ImageCache;
import com.jewelry.android.bitmap.util.ImageFetcher;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.util.JewelryMobileConstant;
import com.jewelry.dto.ProductDTO;
import com.jewelry.dto.ProductPhotoDTO;
import com.jewelry.dto.UserRole;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mos on 18.03.14.
 */
public class ProductListAdapter extends BaseAdapter {

    Context context;
    List<ProductDTO> rowItems;
    ProductDAOHelper productDAOHelper = JewelryApplication.DAO;
    Double goldCost;

    private ImageFetcher mImageFetcher;

    public ProductListAdapter(FragmentActivity context) {
        this.context = context;
        goldCost = (JewelryApplication.instance().getGoldCost() / JewelryMobileConstant.GOLD_UNIT
                + JewelryMobileConstant.GOLD_TRANSPORT_PRICE) / JewelryMobileConstant.GOLD_TYPE_CARAT_0 + JewelryApplication.instance().getWholesale();
        rowItems = new ArrayList<ProductDTO>();
        int mImageThumbSize = context.getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(context, JewelryMobileConstant.IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(context, mImageThumbSize);
        mImageFetcher.setLoadingImage(R.drawable.empty_photo);
        mImageFetcher.addImageCache(context.getSupportFragmentManager(), cacheParams);
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }

    public void setRowItems(List<ProductDTO> rowItems) {
        this.rowItems = rowItems;
    }

    public void setGoldCost(Double value){
        this.goldCost = value;
    }


    public ArrayList<Integer> getProductIds() {
        ArrayList<Integer> productIds = new ArrayList<Integer>(rowItems.size());
        for (ProductDTO productDTO : rowItems) {
            productIds.add(productDTO.getId());
        }
        return productIds;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtProduct;
        TextView txtType;
        TextView txtWeight;
        TextView txtPrice;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.product_list_item, null);
            holder = new ViewHolder();
            holder.txtType = (TextView) convertView.findViewById(R.id.type);
            holder.txtProduct = (TextView) convertView.findViewById(R.id.product_id);
            holder.imageView = (RecyclingImageView) convertView.findViewById(R.id.icon);
            holder.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            holder.txtWeight = (TextView) convertView.findViewById(R.id.weight);
            holder.txtPrice = (TextView) convertView.findViewById(R.id.price);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ProductDTO rowItem = (ProductDTO) getItem(position);

        holder.txtType.setText(ProductType.findName(rowItem.getType()));
        holder.txtProduct.setText(rowItem.getProductId() + "");
        holder.txtWeight.setText(rowItem.getWeight() + "");
        if (rowItem.getWeight() != null && JewelryApplication.instance().getLoggedInUser().getRole()!= UserRole.ROLE_CANDY ) {
            holder.txtPrice.setText(String.format("%.2f", goldCost * rowItem.getWeight()));
        }
        holder.imageView.setImageBitmap(null);
        if (rowItem.getProductPhotos() != null && rowItem.getProductPhotos().size() > 0) {
            String fileName = rowItem.getProductPhotos().get(0).getPhotoThumbnailFile();
            mImageFetcher.loadImage(fileName, holder.imageView);
        } else {
            List<ProductPhotoDTO> productPhotoDTOs = productDAOHelper.loadProductPhotosByProductId(rowItem.getId());
            if (productPhotoDTOs != null && productPhotoDTOs.size() > 0) {
                rowItem.setProductPhotos(productPhotoDTOs);
                String fileName = rowItem.getProductPhotos().get(0).getPhotoThumbnailFile();
                mImageFetcher.loadImage(fileName, holder.imageView);
            } else {
                holder.imageView.setImageResource(R.drawable.empty_photo);
            }
        }
        return convertView;
    }

}