package com.jewelry.android.ui.component;

import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public enum ProductType {
    NECKLACE(0, JewelryApplication.instance().getString(R.string.type_0)),
    RING(1, JewelryApplication.instance().getString(R.string.type_1)),
    WATCH(2, JewelryApplication.instance().getString(R.string.type_2)),
    BRACELET(3, JewelryApplication.instance().getString(R.string.type_3)),
    CHOKER(4, JewelryApplication.instance().getString(R.string.type_4)),
    EARRING(5, JewelryApplication.instance().getString(R.string.type_5)),
    RING_MEN(6, JewelryApplication.instance().getString(R.string.type_6)),
    CROSS(7, JewelryApplication.instance().getString(R.string.type_7)),
    ORIGIN(8, JewelryApplication.instance().getString(R.string.type_8)),
    RING_WEEDING(9, JewelryApplication.instance().getString(R.string.type_9)),
    COULOMB(10, JewelryApplication.instance().getString(R.string.type_10)),
    STAMMERING(11, JewelryApplication.instance().getString(R.string.type_11)),
    CHILD(12, JewelryApplication.instance().getString(R.string.type_12)),
    HEADSET(13, JewelryApplication.instance().getString(R.string.type_13)),
    SETS(14, JewelryApplication.instance().getString(R.string.type_14));



    private ProductType(Integer id, String name) {
        mId = id;
        mName = name;
    }

    private Integer mId;
    private String mName;

    public Integer getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public static String findName(Integer id) {
        for (ProductType productType : values()) {
            if (productType.getId().equals(id)) {
                return productType.getName();
            }
        }
        return "";
    }

    public static List<String> getNameList() {
        List<String> names = new ArrayList<String>();
        for (ProductType productType : values()) {
            names.add(productType.getName());
        }
        return names;
    }

    public static Integer findId(String name) {
        for (ProductType productType : values()) {
            if (productType.getName().equals(name)) {
                return productType.getId();
            }
        }
        return null;
    }


}
