package com.jewelry.android.ui.component;

import com.jewelry.dto.StoneQualityDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mos on 15.04.14.
 */
public class QualityTypes {
    private List<StoneQualityDTO> qualityList;

    public void setQualityList(List<StoneQualityDTO> qualityList){
        this.qualityList = qualityList;
    }



    public  List<String> getNameList() {
         List<String> names = new ArrayList<String>();
            for (StoneQualityDTO quality : qualityList) {
                names.add(quality.getQuality());
            }
         return names;
    }

    public Integer findId(String name) {
        for (StoneQualityDTO stoneQuality : qualityList) {
            if (stoneQuality.getQuality().equals(name)) {
                return stoneQuality.getId();
            }
        }
        return null;
    }
}
