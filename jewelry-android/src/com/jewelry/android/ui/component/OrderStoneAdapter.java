package com.jewelry.android.ui.component;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.dto.*;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Mos .
 */
public class OrderStoneAdapter extends BaseAdapter {
    private Context context;
    private List<StoneOrderDTO> rowItems;
    private ProductDAOHelper productDAOHelper= JewelryApplication.DAO;
    private List<Float> priceList = new ArrayList<Float>();

    public OrderStoneAdapter(Context context) {
        this.context = context;
        rowItems = new ArrayList<StoneOrderDTO>();
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<Float> getPriceList(){
        return this.priceList;
    }


    public void setRowItems(List<StoneOrderDTO> rowItems) {
        this.rowItems = rowItems;
    }

    /*private view holder class*/
    private static class ViewHolder {
        TextView txtCount;
        TextView txtDiameter;
        TextView priceText;
        TextView stoneText;
        TextView qualityText;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.order_stone_item, null);
            holder = new ViewHolder();
            holder.txtCount = (TextView) convertView.findViewById(R.id.stone_count);
	        holder.txtDiameter = (TextView) convertView.findViewById(R.id.stone_diameter);
            holder.stoneText = (TextView)convertView.findViewById(R.id.stone_type);
            holder.qualityText = (TextView)convertView.findViewById(R.id.stone_quality);
            holder.priceText = (TextView) convertView.findViewById(R.id.stone_price);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

	    final StoneOrderDTO rowItem = (StoneOrderDTO) getItem(position);
        Integer qualityId = rowItem.getQualityId();
        StoneQualityDTO qualityDTO = productDAOHelper.getQualityByID(qualityId);
        Integer stoneId = qualityDTO.getStoneId();
        Float diameter = rowItem.getDiameter();
        Integer diameterId = productDAOHelper.getStoneDiameterIdByDiameter(diameter,stoneId);
        StonePriceDTO stonePriceDTO = productDAOHelper.getPrice(stoneId, diameterId, qualityId);
        Float price = stonePriceDTO.getCost();

        if(stoneId == 0){
            DiamondWeightDTO diamondWeightDTO = productDAOHelper.getDiamondWeight(diameter);
            if(diamondWeightDTO != null){
                Float weight = diamondWeightDTO.getMax() + diamondWeightDTO.getMin()/2;
                price = price * weight;
            }
        }
        price =(price + 0.2f)* rowItem.getCount();
        priceList.add(price);

        holder.priceText.setText(price.toString());
        holder.txtCount.append(" " + rowItem.getCount().toString());
        holder.txtDiameter.setText(diameter.toString());
        holder.qualityText.setText(qualityDTO.getQuality());
        holder.stoneText.setText(StoneType.findName(stoneId));

        return convertView;
    }
}