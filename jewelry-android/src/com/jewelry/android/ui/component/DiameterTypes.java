package com.jewelry.android.ui.component;

import com.jewelry.dto.StoneDiameterDTO;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mos on 15.04.14.
 */
public class DiameterTypes {
    private List<StoneDiameterDTO> diameterList;

    public void setDiameterList(List<StoneDiameterDTO> diameterList){
        this.diameterList = diameterList;
    }

    public List<StoneDiameterDTO> getDiameterList(){
        return this.diameterList;
    }

    public  List<String> getNameList() {
         List<String> names = new ArrayList<String>();
            for (StoneDiameterDTO diameter : diameterList) {
                //names.add(diameter.getDiameter().toString());
            }
         return names;
    }

    public Integer findId(String name) {
        for (StoneDiameterDTO diameter : diameterList) {
           /* if (diameter.getDiameter().toString().equals(name)) {
                return diameter.getId();
            }*/
        }
        return null;
    }

    public String getName(Integer id){
        for (StoneDiameterDTO diameter : diameterList) {
            if (diameter.getId().equals(id)) {
               // return diameter.getDiameter().toString();
            }
        }
        return null;
    }
}
