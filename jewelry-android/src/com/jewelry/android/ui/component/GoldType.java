package com.jewelry.android.ui.component;

import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * User: mos
 */
public enum GoldType {
    GOLD_585(0, JewelryApplication.instance().getString(R.string.gold_585)),
	GOLD_750(1, JewelryApplication.instance().getString(R.string.gold_750));



    private GoldType(Integer id, String name) {
        mId = id;
        mName = name;
    }

    private Integer mId;
    private String mName;

    public Integer getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public static String findName(Integer id) {
        for (GoldType goldType : values()) {
            if (goldType.getId().equals(id)) {
                return goldType.getName();
            }
        }
        return "";
    }

    public static List<String> getNameList() {
        List<String> names = new ArrayList<String>();
        for (GoldType goldType : values()) {
            names.add(goldType.getName());
        }
        return names;
    }

    public static Integer findId(String name) {
        for (GoldType goldType : values()) {
            if (goldType.getName().equals(name)) {
                return goldType.getId();
            }
        }
        return null;
    }


}
