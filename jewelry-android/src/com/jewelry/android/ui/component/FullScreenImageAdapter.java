package com.jewelry.android.ui.component;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.jewelry.android.R;
import com.jewelry.android.bitmap.util.ImageCache;
import com.jewelry.android.bitmap.util.ImageFetcher;
import com.jewelry.android.domain.Photo;
import com.jewelry.android.util.JewelryMobileConstant;

import java.util.List;

public class FullScreenImageAdapter extends PagerAdapter {

    private FragmentActivity _activity;
    private List<Photo> _imagePaths;
    private ImageFetcher mImageFetcher;

    // constructor
    public FullScreenImageAdapter(FragmentActivity activity,
                                  List<Photo> imagePaths) {
        this._activity = activity;
        this._imagePaths = imagePaths;

        final DisplayMetrics displayMetrics = new DisplayMetrics();
        _activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;
        final int longest = (height > width ? height : width) / 2;

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(_activity, JewelryMobileConstant.IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(_activity, longest);
        mImageFetcher.addImageCache(_activity.getSupportFragmentManager(), cacheParams);
    }

    @Override
    public int getCount() {
        return this._imagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imgDisplay;

        LayoutInflater inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);

        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
        String fileName = _imagePaths.get(position).getFilePath();
        mImageFetcher.loadImage(fileName, imgDisplay);

        container.addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);

    }

}
