package com.jewelry.android.ui.component;

import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * User: mos
 */
public enum StoneType {
    STONE_DIAMOND(0, JewelryApplication.instance().getString(R.string.stone_diamond)),
    STONE_ZIRCON(1, JewelryApplication.instance().getString(R.string.stone_zircon));



    private StoneType(Integer id, String name) {
        mId = id;
        mName = name;
    }

    private Integer mId;
    private String mName;

    public Integer getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public static String findName(Integer id) {
        for (StoneType stoneType : values()) {
            if (stoneType.getId().equals(id)) {
                return stoneType.getName();
            }
        }
        return "";
    }

    public static List<String> getNameList() {
        List<String> names = new ArrayList<String>();
        for (StoneType stoneType : values()) {
            names.add(stoneType.getName());
        }
        return names;
    }

    public static Integer findId(String name) {
        for (StoneType stoneType : values()) {
            if (stoneType.getName().equals(name)) {
                return stoneType.getId();
            }
        }
        return null;
    }


}
