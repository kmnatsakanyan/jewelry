package com.jewelry.android.ui.component;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.bitmap.ui.RecyclingImageView;
import com.jewelry.android.bitmap.util.ImageCache;
import com.jewelry.android.bitmap.util.ImageFetcher;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.domain.OrderTemp;
import com.jewelry.android.util.JewelryMobileConstant;
import com.jewelry.dto.OrderDTO;
import com.jewelry.dto.ProductPhotoDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class OrderTempListAdapter extends BaseAdapter {
    Context context;
    List<OrderTemp> rowItems;
    ProductDAOHelper productDAOHelper;
    private ImageFetcher mImageFetcher;

    public OrderTempListAdapter(FragmentActivity context) {
        this.context = context;
        rowItems = new ArrayList<OrderTemp>();
        productDAOHelper = JewelryApplication.DAO;

        int mImageThumbSize = context.getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(context, JewelryMobileConstant.IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(context, mImageThumbSize);
        mImageFetcher.setLoadingImage(R.drawable.empty_photo);
        mImageFetcher.addImageCache(context.getSupportFragmentManager(), cacheParams);
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }

    public void setRowItems(List<OrderTemp> rowItems) {
        this.rowItems = rowItems;
    }


    public List<OrderDTO> getOrderRowItems() {
        List <OrderDTO> orders = new ArrayList<OrderDTO>();
        for(OrderTemp orderTemp : rowItems){
            orders.add(orderTemp.getOrderDTO());
        }
        return orders;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView readyCount;
        TextView toBeDoneCount;
        TextView orderOwnerName;
        TextView orderDate;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.order_list_item, null);
            holder = new ViewHolder();
            holder.toBeDoneCount = (TextView) convertView.findViewById(R.id.to_be_done_count);
            holder.readyCount = (TextView) convertView.findViewById(R.id.ready_count);
            holder.orderOwnerName = (TextView) convertView.findViewById(R.id.user_name);
            holder.orderDate = (TextView) convertView.findViewById(R.id.order_date);
            holder.imageView = (RecyclingImageView) convertView.findViewById(R.id.icon);
            holder.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        OrderTemp rowItem = (OrderTemp) getItem(position);
        OrderDTO orderDTO = rowItem.getOrderDTO();

        Integer beDone =orderDTO.getCount() - orderDTO.getDone();

        holder.toBeDoneCount.setText(beDone.toString());
        holder.readyCount.setText(orderDTO.getDone().toString());
        holder.orderOwnerName.setText(orderDTO.getUser().getEmail());
        holder.orderDate.setText(orderDTO.getDate().toString("yyyy.MM.dd"));
        holder.imageView.setImageBitmap(null);
        if (orderDTO.getProduct().getProductPhotos() != null && orderDTO.getProduct().getProductPhotos().size() > 0) {
            String fileName = orderDTO.getProduct().getProductPhotos().get(0).getPhotoThumbnailFile();
            mImageFetcher.loadImage(fileName, holder.imageView);
        } else {
            List<ProductPhotoDTO> productPhotoDTOs = productDAOHelper.loadProductPhotosByProductId(rowItem.getId());
            if (productPhotoDTOs != null && productPhotoDTOs.size() > 0) {
                orderDTO.getProduct().setProductPhotos(productPhotoDTOs);
                String fileName = orderDTO.getProduct().getProductPhotos().get(0).getPhotoThumbnailFile();
                mImageFetcher.loadImage(fileName, holder.imageView);
            } else {
                holder.imageView.setImageResource(R.drawable.empty_photo);
            }
        }
        return convertView;
    }
}
