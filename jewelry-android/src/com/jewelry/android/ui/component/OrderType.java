package com.jewelry.android.ui.component;

import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * User: mos
 */
public enum OrderType {

    WHOLESALE(0,JewelryApplication.instance().getString(R.string.wholesale)),
    RETAIL(1,JewelryApplication.instance().getString(R.string.retail));




    private OrderType(Integer id, String name) {
        mId = id;
        mName = name;
    }

    private Integer mId;
    private String mName;

    public Integer getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public static String findName(Integer id) {
        for (OrderType goldType : values()) {
            if (goldType.getId().equals(id)) {
                return goldType.getName();
            }
        }
        return "";
    }

    public static List<String> getNameList() {
        List<String> names = new ArrayList<String>();
        for (OrderType goldType : values()) {
            names.add(goldType.getName());
        }
        return names;
    }

    public static Integer findId(String name) {
        for (OrderType goldType : values()) {
            if (goldType.getName().equals(name)) {
                return goldType.getId();
            }
        }
        return null;
    }


}
