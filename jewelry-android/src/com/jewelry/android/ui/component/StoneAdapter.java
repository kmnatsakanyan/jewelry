package com.jewelry.android.ui.component;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.dto.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mos .
 */
public class StoneAdapter extends BaseAdapter {
    private Context context;
    private List<ProductStoneDTO> rowItems;
    private ProductDAOHelper productDAOHelper = JewelryApplication.DAO;
    private QualityTypes qualityTypes = new QualityTypes();
    private Map<Integer, Float> priceMap = new HashMap<Integer, Float>();
    private Map<Integer, StoneOrderDTO> stoneOrders = new HashMap<Integer, StoneOrderDTO>();

    public StoneAdapter(Context context) {
        this.context = context;
        rowItems = new ArrayList<ProductStoneDTO>();
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public Map<Integer, Float> getPriceMap() {
        return this.priceMap;
    }

    public List<StoneOrderDTO> getStoneOrders() {
        return new ArrayList<StoneOrderDTO>(this.stoneOrders.values());
    }

    public void setRowItems(List<ProductStoneDTO> rowItems) {
        this.rowItems = rowItems;
    }

    /*private view holder class*/
    private static class ViewHolder {
        TextView txtCount;
        EditText txtDiameter;
        TextView priceText;
        Button addButton;
        Button deleteButton;
        Spinner stoneSpinner;
        Spinner qualitySpinner;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.product_stone_item, null);
            holder = new ViewHolder();
            holder.txtCount = (TextView) convertView.findViewById(R.id.stone_count);
            holder.txtDiameter = (EditText) convertView.findViewById(R.id.stone_diameter);
            holder.addButton = (Button) convertView.findViewById(R.id.add);
            holder.stoneSpinner = (Spinner) convertView.findViewById(R.id.stone_type);
            holder.qualitySpinner = (Spinner) convertView.findViewById(R.id.stone_quality);
            holder.priceText = (TextView) convertView.findViewById(R.id.stone_price);
            holder.deleteButton = (Button) convertView.findViewById(R.id.btnDelete);
            holder.txtDiameter.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if (!b) {
                        holder.txtDiameter.setError(null);
                    }
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final ProductStoneDTO rowItem = (ProductStoneDTO) getItem(position);
        final Integer mapId = position;
        holder.txtCount.setText(rowItem.getCount().toString());
        holder.txtDiameter.setText(rowItem.getDiameter().toString());

        holder.txtDiameter.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                deleteStone(mapId, holder);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        List<String> spinnerItems = new ArrayList<String>();
        spinnerItems.addAll(StoneType.getNameList());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, spinnerItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.stoneSpinner.setAdapter(adapter);
        final View.OnClickListener addListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Integer stoneId = holder.stoneSpinner.getSelectedItemPosition();
                String diameterText = holder.txtDiameter.getText().toString().trim();
                holder.txtDiameter.setError(null);

                if ((diameterText.equals("")) || (Float.parseFloat(diameterText) == 0f)) {
                    holder.txtDiameter.setError(context.getString(R.string.enter_stone_diameter));
                } else {
                    Float diameter = Float.parseFloat(diameterText);
                    Integer diameterId = productDAOHelper.getStoneDiameterIdByDiameter(diameter, stoneId);
                    if (diameterId != null) {
                        StoneOrderDTO stoneOrder = new StoneOrderDTO();
                        Integer qualityId = qualityTypes.findId(holder.qualitySpinner.getSelectedItem().toString());
                        StonePriceDTO stonePriceDTO = productDAOHelper.getPrice(stoneId, diameterId, qualityId);
                        Float price = stonePriceDTO.getCost();

                        if(price == 0 ){
                            holder.txtDiameter.setError(context.getString(R.string.stone_not_exist));
                            return;
                        }

                        if (stoneId == 0) {
                            DiamondWeightDTO diamondWeightDTO = productDAOHelper.getDiamondWeight(diameter);
                            if (diamondWeightDTO != null) {
                                Float weight = (diamondWeightDTO.getMax() + diamondWeightDTO.getMin()) / 2;
                                price = price * weight;
                            }else{
                                holder.txtDiameter.setError(context.getString(R.string.stone_not_exist));
                                return;
                            }
                        }
                        price = (price + 0.008f)* rowItem.getCount();
                        holder.priceText.setText(price.toString());
                        holder.priceText.setVisibility(View.VISIBLE);
                        priceMap.put(mapId, price);
                        stoneOrder.setCount(rowItem.getCount());
                        stoneOrder.setDiameter(diameter);
                        stoneOrder.setQualityId(qualityId);
                        stoneOrders.put(mapId, stoneOrder);
                        holder.deleteButton.setVisibility(View.VISIBLE);
                    } else {
                        holder.txtDiameter.setError(context.getString(R.string.validation_diameter));
                    }
                }
            }
        };

        final View.OnClickListener deleteListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteStone(mapId, holder);
            }
        };
        holder.deleteButton.setOnClickListener(deleteListener);
        holder.addButton.setOnClickListener(addListener);


        final AdapterView.OnItemSelectedListener typeListener = new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                deleteStone(mapId, holder);
                List<StoneQualityDTO> qualityList = productDAOHelper.getQualityByType(pos);
                qualityTypes.setQualityList(qualityList);
                List<String> qualityNames = qualityTypes.getNameList();
                List<String> spinnerItems = new ArrayList<String>();
                spinnerItems.addAll(qualityNames);

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                        android.R.layout.simple_spinner_item, spinnerItems);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                holder.qualitySpinner.setAdapter(adapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        };

        final AdapterView.OnItemSelectedListener qualityListener = new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                deleteStone(mapId, holder);
                holder.txtDiameter.setError(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        };


        holder.stoneSpinner.setOnItemSelectedListener(typeListener);
        holder.qualitySpinner.setOnItemSelectedListener(qualityListener);
        return convertView;
    }


    private void deleteStone(Integer mapId, ViewHolder holder) {
        priceMap.remove(mapId);
        stoneOrders.remove(mapId);
        holder.priceText.setVisibility(View.INVISIBLE);
        holder.deleteButton.setVisibility(View.INVISIBLE);
    }

}