package com.jewelry.android.ui.component;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.jewelry.android.R;
import com.jewelry.android.app.JewelryApplication;
import com.jewelry.android.bitmap.ui.RecyclingImageView;
import com.jewelry.android.bitmap.util.ImageCache;
import com.jewelry.android.bitmap.util.ImageFetcher;
import com.jewelry.android.dao.helper.ProductDAOHelper;
import com.jewelry.android.domain.ProductTemp;
import com.jewelry.android.util.BitmapUtils;
import com.jewelry.android.util.JewelryMobileConstant;
import com.jewelry.dto.ProductPhotoDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class ProductUpdatesListAdapter extends BaseAdapter {
    Context context;
    List<ProductTemp> rowItems;
    ProductDAOHelper productDAOHelper = JewelryApplication.DAO;
    private ImageFetcher mImageFetcher;

    public ProductUpdatesListAdapter(FragmentActivity context) {
        this.context = context;
        rowItems = new ArrayList<ProductTemp>();
        int mImageThumbSize = context.getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(context, JewelryMobileConstant.IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(context, mImageThumbSize);
        mImageFetcher.setLoadingImage(R.drawable.empty_photo);
        mImageFetcher.addImageCache(context.getSupportFragmentManager(), cacheParams);
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public ProductTemp getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }

    public void setRowItems(List<ProductTemp> rowItems) {
        this.rowItems = rowItems;
    }

    public void removeItem(ProductTemp temp) {
        rowItems.remove(temp);

    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtProduct;
        TextView txtType;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.product_list_item, null);
            holder = new ViewHolder();
            holder.txtType = (TextView) convertView.findViewById(R.id.type);
            holder.txtProduct = (TextView) convertView.findViewById(R.id.product_id);
            holder.imageView = (RecyclingImageView) convertView.findViewById(R.id.icon);
            holder.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ProductTemp rowItem = getItem(position);

        holder.txtType.setText(ProductType.findName(rowItem.getType()));
        holder.txtProduct.setText(rowItem.getProductName() + "");
        boolean isPhotoSet = false;
        if (rowItem.getProductPhotoKey() != null) {
            List<String> productPhotos = BitmapUtils.getProductPhotosIfExists(rowItem.getProductPhotoKey());
            if (productPhotos != null && productPhotos.size() > 0) {
                String filePath = productPhotos.get(0);
                mImageFetcher.loadImage(filePath, holder.imageView);
                isPhotoSet = true;
            }
        }
        if (!isPhotoSet && rowItem.getProductId() != null && rowItem.getProductId() != 0) {
            List<ProductPhotoDTO> productPhotoDTOs = productDAOHelper.loadProductPhotosByProductId(rowItem.getProductId());
            if (productPhotoDTOs != null && productPhotoDTOs.size() > 0) {
                String fileName = productPhotoDTOs.get(0).getPhotoThumbnailFile();
                mImageFetcher.loadImage(fileName, holder.imageView);
            }
        }

        return convertView;
    }

}