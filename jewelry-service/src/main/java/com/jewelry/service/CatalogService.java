package com.jewelry.service;

import com.jewelry.dto.CatalogDTO;
import org.joda.time.DateTime;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public interface CatalogService {

    public CatalogDTO saveCatalog(CatalogDTO productDTO);

    public CatalogDTO updateCatalog(CatalogDTO product);

    List<CatalogDTO> getUpdates(DateTime startDate);

    List<CatalogDTO> getAll();

    Boolean delete(Integer userId);
}
