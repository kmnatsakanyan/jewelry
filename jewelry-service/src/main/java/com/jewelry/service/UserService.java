package com.jewelry.service;


import com.jewelry.dto.UserDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    public void saveUser(UserDTO employee);

    public UserDTO login(String loginName, String password);

    public List<UserDTO> getAll();

    public UserDTO getUser(Integer userId);

    Boolean delete(Integer userId);
}
