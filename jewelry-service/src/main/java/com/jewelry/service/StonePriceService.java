package com.jewelry.service;

import com.jewelry.dto.StonePriceDTO;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by aramayis.antonyan on 2/28/14.
 */
public interface StonePriceService {
	public List<StonePriceDTO> getPrices();
	public boolean updatePrice(List<StonePriceDTO> stonePrices);
	public List<StonePriceDTO> addPrice(List<StonePriceDTO> priceList);
    public List<StonePriceDTO> getUpdates(DateTime startDate);
}
