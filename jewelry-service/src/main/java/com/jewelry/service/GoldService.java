package com.jewelry.service;


import com.jewelry.dto.GoldDTO;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by aramayis.antonyan on 2/26/14.
 */
public interface GoldService {
    public boolean updateCost(Double cost);

    public Double getCost();

    public List<GoldDTO> getUpdates(DateTime startDate);
}
