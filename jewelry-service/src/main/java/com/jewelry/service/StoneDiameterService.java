package com.jewelry.service;

import com.jewelry.dto.StoneDiameterDTO;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
public interface StoneDiameterService {
	public List<StoneDiameterDTO> getDiametersList();
    public List<StoneDiameterDTO> getUpdates(DateTime startDate);
    public Integer saveDiameter(StoneDiameterDTO diameterDTO);
    public Boolean delete(Integer diameterId);
}
