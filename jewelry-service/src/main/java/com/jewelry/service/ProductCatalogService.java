package com.jewelry.service;

import com.jewelry.dto.ProductCatalogDTO;
import org.joda.time.DateTime;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public interface ProductCatalogService {

    public ProductCatalogDTO saveProductCatalog(ProductCatalogDTO productCatalogDTO);

    public ProductCatalogDTO updateProductCatalog(ProductCatalogDTO productCatalogDTO);

    List<ProductCatalogDTO> getUpdates(DateTime startDate);
}
