package com.jewelry.service;

/**
 * User: karen.mnatsakanyan
 */
public interface PhotoService {

    public boolean save(Integer productId, byte[] bytes);
}
