package com.jewelry.service;

import com.jewelry.entity.DiamondWeightEntity;

/**
 * Created by Mos on 10.05.14.
 */
public interface DBPopulatorService {
    void loadDiamondWeightFromCSV();
    DiamondWeightEntity readSCVRow(String row);
}
