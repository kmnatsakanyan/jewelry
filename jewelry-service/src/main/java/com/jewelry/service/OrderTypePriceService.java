package com.jewelry.service;


import com.jewelry.dto.OrderType;
import com.jewelry.dto.OrderTypePriceDTO;
import com.jewelry.entity.OrderTypePriceEntity;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by aramayis.antonyan .
 */
public interface OrderTypePriceService {

    public boolean updatePrice(OrderType type, Double price);

    public List<OrderTypePriceDTO> getOrderTypePrices();
}
