package com.jewelry.service;

import com.jewelry.dto.StoneDiameterDTO;
import com.jewelry.dto.StoneQualityDTO;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by aramayis.antonyan on 4/16/14.
 */
public interface StoneQualityService {
	public List<StoneQualityDTO> getQualityList();
    public List<StoneQualityDTO> getUpdates(DateTime startDate);
    public Integer saveQuality(StoneQualityDTO qualityDTO);
    public Boolean delete(Integer qualityId);
}
