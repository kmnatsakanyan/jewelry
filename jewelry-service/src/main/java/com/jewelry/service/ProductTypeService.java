package com.jewelry.service;


import com.jewelry.dto.ProductTypeDTO;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
public interface ProductTypeService {
    public List<ProductTypeDTO> getTypeList();
    public ProductTypeDTO getType(Integer id);
    public List<ProductTypeDTO> getUpdates(DateTime startDate);
}
