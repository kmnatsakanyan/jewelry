package com.jewelry.service;


import com.jewelry.dto.ProductDTO;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by Mos on 25.02.14.
 */
public interface ProductService {

    public ProductDTO saveProduct(ProductDTO productDTO);

    public ProductDTO updateProduct(ProductDTO product);

    List<ProductDTO> getUpdates(DateTime startDate);

    public void updateProductList(List<ProductDTO> productDTOs);
}
