package com.jewelry.service;

import com.jewelry.dto.OrderDTO;
import org.joda.time.DateTime;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public interface OrderService {
    void updateOrder(OrderDTO orderDTO);
    OrderDTO addOrder(OrderDTO orderDTO);
    List<OrderDTO> updateOrders(List<OrderDTO> orderDTO);
    List<OrderDTO> getUpdates(DateTime startDate,Integer userId);
    Boolean delete(Integer orderId);
}
