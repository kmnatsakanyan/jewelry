package com.jewelry.service.impl;


import com.jewelry.dao.GenericDAO;
import com.jewelry.dao.ProductTypeDAO;
import com.jewelry.dto.ProductTypeDTO;
import com.jewelry.entity.ProductTypeEntity;
import com.jewelry.service.ProductTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
@Service("productTypeService")
public class ProductTypeServiceImpl extends GenericService<ProductTypeDTO, ProductTypeEntity> implements ProductTypeService {

    @Autowired
    private ProductTypeDAO productTypeDao;

    @Override
    @Transactional
    public List<ProductTypeDTO> getTypeList() {
        List<ProductTypeEntity> typeEntities = productTypeDao.findAll();
        List<ProductTypeDTO> typeDTOs = new ArrayList<ProductTypeDTO>();
        for (ProductTypeEntity typeEntity : typeEntities) {
            typeDTOs.add(dozerMapper.map(typeEntity, ProductTypeDTO.class));
        }
        return typeDTOs;
    }

    @Override
    @Transactional
    public ProductTypeDTO getType(Integer id) {
        return dozerMapper.map(productTypeDao.find(id), ProductTypeDTO.class);
    }

    @Override
    protected GenericDAO getDAO() {
        return productTypeDao;
    }
}
