package com.jewelry.service.impl;

import com.jewelry.dao.GenericDAO;
import com.jewelry.dao.OrderDAO;
import com.jewelry.dao.UserDAO;
import com.jewelry.dto.OrderDTO;
import com.jewelry.dto.UserRole;
import com.jewelry.entity.OrderEntity;
import com.jewelry.entity.StoneOrderEntity;
import com.jewelry.entity.StoneQualityEntity;
import com.jewelry.entity.UserEntity;
import com.jewelry.exception.UserNotFoundException;
import com.jewelry.service.OrderService;
import com.jewelry.util.SystemLogger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
@Service
public class OrderServiceImpl extends GenericService<OrderDTO, OrderEntity> implements OrderService {

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private UserDAO userDAO;

    @Override
    @Transactional
    public void updateOrder(OrderDTO orderDTO) {
        OrderEntity orderEntity = dozerMapper.map(orderDTO, OrderEntity.class);
        orderDAO.merge(orderEntity);
    }

    @Override
    @Transactional
    public OrderDTO addOrder(OrderDTO orderDTO) {
        OrderEntity orderEntity = dozerMapper.map(orderDTO, OrderEntity.class);
        List<StoneOrderEntity> stones = orderEntity.getStones();
        for (int i = 0; i < stones.size(); i++) {
            StoneOrderEntity stoneOrderEntity = stones.get(i);
            StoneQualityEntity stoneQualityEntity = new StoneQualityEntity();
            stoneQualityEntity.setId(orderDTO.getStones().get(i).getQualityId());
            stoneOrderEntity.setQualityId(stoneQualityEntity);
        }
        orderDAO.persist(orderEntity);
        orderDTO.setId(orderEntity.getId());
        return orderDTO;
    }

    @Override
    @Transactional
    public List<OrderDTO> updateOrders(List<OrderDTO> orders) {
        List<OrderDTO> result = new ArrayList<OrderDTO>();
        for (OrderDTO orderDTO : orders) {
            OrderEntity orderEntity = dozerMapper.map(orderDTO, OrderEntity.class);
            if (orderEntity.getId() != null) {
                orderDAO.merge(orderEntity);
            } else {
                orderDAO.persist(orderEntity);
            }
            result.add(dozerMapper.map(orderEntity, OrderDTO.class));
        }
        return result;
    }

    @Override
    @Transactional
    public List<OrderDTO> getUpdates(DateTime startDate, Integer userId) {
        UserEntity userEntity = userDAO.find(userId);
        if (userEntity.getRole() == UserRole.ROLE_ADMIN || userEntity.getRole() == UserRole.ROLE_CANDY) {
            return super.getUpdates(startDate);
        }
        List<OrderEntity> orderEntities = orderDAO.findUpdatedRecords(startDate, userId);
        List<OrderDTO> productDTOs = new ArrayList<OrderDTO>();
        for (OrderEntity productEntity : orderEntities) {
            try {
                productDTOs.add(dozerMapper.map(productEntity, OrderDTO.class));
            } catch (Exception ex) {
                SystemLogger.e(ex.getMessage());
            }
        }
        return productDTOs;
    }

    @Override
    @Transactional
    public Boolean delete(Integer orderId) {

        OrderEntity userEntity = orderDAO.find(orderId);
        if (userEntity != null) {
            userEntity.setDeleted(true);
            orderDAO.merge(userEntity);
            return true;
        }
        throw new UserNotFoundException("Cant find user to delete");
    }

    @Override
    protected GenericDAO getDAO() {
        return orderDAO;
    }


}
