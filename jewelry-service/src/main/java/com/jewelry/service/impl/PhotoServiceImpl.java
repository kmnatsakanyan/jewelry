package com.jewelry.service.impl;

import com.jewelry.dao.ProductPhotoDAO;
import com.jewelry.entity.ProductPhotoEntity;
import com.jewelry.service.PhotoService;
import com.jewelry.util.MimeTypes;
import com.jewelry.util.ServiceConfigs;
import com.jewelry.util.SystemLogger;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.net.URLConnection;

import static com.jewelry.util.JewelryConstant.*;

/**
 * User: karen.mnatsakanyan
 */
@Service
public class PhotoServiceImpl implements PhotoService {

    @Autowired
    ServiceConfigs serviceConf;

    @Autowired
    ProductPhotoDAO productPhotoDAO;

    @Override
    @Transactional
    public boolean save(Integer productId, byte[] bytes) {
        if (productId != null && bytes != null && bytes.length > 0) {
            try {
                InputStream is = new BufferedInputStream(new ByteArrayInputStream(bytes));
                String mimeType = URLConnection.guessContentTypeFromStream(is);
                String extension = MimeTypes.lookupMimeType(mimeType);

                String photoName = productId + "_" + System.currentTimeMillis();
                String photoFullPath = serviceConf.getPhotosURL() + photoName + "." + extension;

                String photoFullName = photoName + "." + extension;
                String photoThumbnailName = photoName + THUMBNAIL_PREFIX + "." + extension;

                writeToFile(bytes, photoFullPath);
                createThumbnail(photoThumbnailName, photoFullPath);
                //Creating productPhoto entity
                ProductPhotoEntity productPhotoEntity = new ProductPhotoEntity();
                productPhotoEntity.setPhotoFile(photoFullName);
                productPhotoEntity.setProductId(productId);
                productPhotoEntity.setPhotoThumbnailFile(photoThumbnailName);
                productPhotoDAO.persist(productPhotoEntity);
                return true;
            } catch (IOException e) {
                SystemLogger.e("Error while saving photo");
                return false;
            }
        }
        return false;
    }

    // save uploaded file to new location
    private void writeToFile(byte[] bytes,
                             String uploadedFileLocation) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(uploadedFileLocation));
        bos.write(bytes);
        bos.flush();
        bos.close();
    }

    private void createThumbnail(String photoThumbnailName, String photoFullPath) throws IOException {
        Thumbnails.of(new File(photoFullPath))
                .size(THUMBNAIL_X, THUMBNAIL_Y)
                .toFile(new File(serviceConf.getPhotosURL() + photoThumbnailName));
    }
}
