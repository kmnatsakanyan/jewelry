package com.jewelry.service.impl;

import com.jewelry.dao.GenericDAO;
import com.jewelry.dao.UserDAO;
import com.jewelry.dto.UserDTO;
import com.jewelry.entity.UserEntity;
import com.jewelry.exception.UnauthorizedException;
import com.jewelry.exception.UserExistException;
import com.jewelry.exception.UserNotFoundException;
import com.jewelry.security.User;
import com.jewelry.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl extends GenericService<UserDTO, UserEntity> implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    @Transactional
    public void saveUser(UserDTO userDTO) {
        UserEntity userEntity = dozerMapper.map(userDTO, UserEntity.class);

        UserEntity checkEntity = userDAO.getUserByEmail(userDTO.getEmail());
        if (checkEntity != null) {
            if(userEntity.getId()==checkEntity.getId()){
                userDAO.merge(userEntity);
            }else{
                throw new UserExistException("Such user exist");
            }
        } else {
            if(userEntity.getId()!= null){
                userDAO.merge(userEntity);
            }else{
                userDAO.persist(userEntity);
            }
        }

    }

    @Override
    @Transactional
    public UserDTO login(String loginName, String password) {
        UserEntity userEntity = userDAO.getUserByEmailAndPassword(loginName, password);
        if (userEntity == null)
            throw new UnauthorizedException("User not found");
        return dozerMapper.map(userEntity, UserDTO.class);
    }

    @Override
    public List<UserDTO> getAll() {
        List<UserDTO> userDTOs = new ArrayList<UserDTO>();
        List<UserEntity> userEntities = userDAO.findAll();
        for (UserEntity userEntity : userEntities) {
            userDTOs.add(dozerMapper.map(userEntity, UserDTO.class));
        }
        return userDTOs;
    }

    @Override
    public UserDTO getUser(Integer userId) {
       UserEntity userEntity = userDAO.find(userId);
        if(userEntity!= null){
            return dozerMapper.map(userEntity, UserDTO.class);
        }
        else{
            return null;
        }
    }

    @Override
    @Transactional
    public Boolean delete(Integer userId) {
        UserEntity userEntity = userDAO.find(userId);
        if (userEntity != null) {
            userEntity.setDeleted(true);
            userDAO.merge(userEntity);
            return true;
        }
        throw new UserNotFoundException("Cant find user to delete");
    }

    @Override
    protected GenericDAO getDAO() {
        return userDAO;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userDAO.getUserByEmail(username);
        if (userEntity != null) {
            return new User(userEntity);
        } else {
            return null;
        }
    }
}
