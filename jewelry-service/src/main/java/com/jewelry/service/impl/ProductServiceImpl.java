package com.jewelry.service.impl;

import com.jewelry.dao.GenericDAO;
import com.jewelry.dao.ProductCatalogDAO;
import com.jewelry.dao.ProductDAO;
import com.jewelry.dto.ProductCatalogDTO;
import com.jewelry.dto.ProductDTO;
import com.jewelry.entity.ProductCatalogEntity;
import com.jewelry.entity.ProductEntity;
import com.jewelry.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl extends GenericService<ProductDTO, ProductEntity> implements ProductService {

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private ProductCatalogDAO productCatalogDAO;

    @Override
    @Transactional
    public ProductDTO saveProduct(ProductDTO productDTO) {
        ProductEntity productEntity = dozerMapper.map(productDTO, ProductEntity.class);
        if (productDTO.getProductCatalogs() != null) {
            List<ProductCatalogEntity> productCatalogEntities = new ArrayList<ProductCatalogEntity>();
            for (ProductCatalogDTO productCatalogDTO : productDTO.getProductCatalogs()) {
                ProductCatalogEntity productCatalogEntity = dozerMapper.map(productCatalogDTO, ProductCatalogEntity.class);
                productCatalogEntity.setProduct(productEntity);
                productCatalogEntities.add(productCatalogEntity);
            }
            productEntity.setProductCatalogs(productCatalogEntities);
        }
        productDAO.persist(productEntity);
        productDTO.setId(productEntity.getId());
        return dozerMapper.map(productEntity, ProductDTO.class);
    }

    @Override
    @Transactional
    public ProductDTO updateProduct(ProductDTO productDTO) {
        ProductEntity productEntity = dozerMapper.map(productDTO, ProductEntity.class);
        if (productDTO.getProductCatalogs() != null) {
            List<ProductCatalogEntity> productCatalogEntities = new ArrayList<ProductCatalogEntity>();
            for (ProductCatalogDTO productCatalogDTO : productDTO.getProductCatalogs()) {
                productCatalogEntities.add(dozerMapper.map(productCatalogDTO, ProductCatalogEntity.class));
            }

            ProductEntity productEntity1 = productDAO.find(productEntity.getId());
            for (ProductCatalogEntity productCatalogEntity : productEntity1.getProductCatalogs()) {
                boolean deleted = true;
                for (ProductCatalogEntity entity : productCatalogEntities) {
                    if (productCatalogEntity.getCatalog().getId().equals(entity.getCatalog().getId())
                            && productCatalogEntity.getProductLabel().equals(entity.getProductLabel())) {
                        deleted = false;
                        productCatalogEntities.remove(entity);
                        break;
                    }
                }
                if (deleted) {
                    productCatalogEntity.setDeleted(true);
                    productCatalogDAO.merge(productCatalogEntity);
                }
            }
            productEntity.setProductCatalogs(productCatalogEntities);
        }
        productDAO.merge(productEntity);
        productDTO.setId(productEntity.getId());
        return dozerMapper.map(productEntity, ProductDTO.class);
    }

    @Override
    @Transactional
    public void updateProductList(List<ProductDTO> productDTOs) {
        for (ProductDTO productDTO : productDTOs) {
            ProductEntity productEntity = dozerMapper.map(productDTO, ProductEntity.class);
            if (productEntity.getId() != null) {
                productDAO.merge(productEntity);
            } else {
                productDAO.persist(productEntity);
            }
        }
    }

    @Override
    protected GenericDAO getDAO() {
        return productDAO;
    }
}
