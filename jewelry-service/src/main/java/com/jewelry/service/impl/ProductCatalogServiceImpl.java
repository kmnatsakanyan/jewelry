package com.jewelry.service.impl;

import com.jewelry.dao.GenericDAO;
import com.jewelry.dao.ProductCatalogDAO;
import com.jewelry.dto.ProductCatalogDTO;
import com.jewelry.entity.ProductCatalogEntity;
import com.jewelry.service.ProductCatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * User: karen.mnatsakanyan
 */
@Service
public class ProductCatalogServiceImpl extends GenericService<ProductCatalogDTO, ProductCatalogEntity> implements ProductCatalogService {

    @Autowired
    ProductCatalogDAO productCatalogDAO;

    @Override
    public ProductCatalogDTO saveProductCatalog(ProductCatalogDTO productCatalogDTO) {
        ProductCatalogEntity catalogEntity = dozerMapper.map(productCatalogDTO, ProductCatalogEntity.class);
        productCatalogDAO.persist(catalogEntity);
        return productCatalogDTO;
    }

    @Override
    public ProductCatalogDTO updateProductCatalog(ProductCatalogDTO productCatalogDTO) {
        ProductCatalogEntity catalogEntity = dozerMapper.map(productCatalogDTO, ProductCatalogEntity.class);
        productCatalogDAO.merge(catalogEntity);
        return productCatalogDTO;
    }

    @Override
    protected GenericDAO getDAO() {
        return productCatalogDAO;
    }
}
