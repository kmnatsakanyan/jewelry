package com.jewelry.service.impl;

import com.jewelry.dao.CatalogDAO;
import com.jewelry.dao.GenericDAO;
import com.jewelry.dto.CatalogDTO;
import com.jewelry.entity.CatalogEntity;
import com.jewelry.exception.UserNotFoundException;
import com.jewelry.service.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
@Service
public class CatalogServiceImpl extends GenericService<CatalogDTO, CatalogEntity> implements CatalogService {

    @Autowired
    CatalogDAO catalogDAO;

    @Override
    @Transactional
    public CatalogDTO saveCatalog(CatalogDTO catalogDTO) {
        CatalogEntity catalogEntity = dozerMapper.map(catalogDTO, CatalogEntity.class);
        catalogDAO.persist(catalogEntity);
        return catalogDTO;
    }

    @Override
    @Transactional
    public CatalogDTO updateCatalog(CatalogDTO catalogDTO) {
        CatalogEntity catalogEntity = dozerMapper.map(catalogDTO, CatalogEntity.class);
        catalogDAO.merge(catalogEntity);
        return catalogDTO;
    }

    @Override
    public List<CatalogDTO> getAll() {
        List<CatalogDTO> catalogDTOs = new ArrayList<CatalogDTO>();
        List<CatalogEntity> all = catalogDAO.findAll();
        for (CatalogEntity userEntity : all) {
            catalogDTOs.add(dozerMapper.map(userEntity, CatalogDTO.class));
        }
        return catalogDTOs;
    }


    @Override
    @Transactional
    public Boolean delete(Integer userId) {
        CatalogEntity userEntity = catalogDAO.find(userId);
        if (userEntity != null) {
            userEntity.setDeleted(true);
            catalogDAO.remove(userEntity);
            return true;
        }
        throw new UserNotFoundException("Cant find catalog to delete");
    }
    @Override
    protected GenericDAO getDAO() {
        return catalogDAO;
    }
}
