package com.jewelry.service.impl;

import com.jewelry.dao.GenericDAO;
import com.jewelry.dao.GoldDAO;
import com.jewelry.dto.GoldDTO;
import com.jewelry.entity.GoldEntity;
import com.jewelry.service.GoldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Created by aramayis.antonyan on 2/26/14.
 */
@Service
public class GoldServiceImpl extends GenericService<GoldDTO, GoldEntity> implements GoldService {


    @Autowired
    private GoldDAO goldDAO;

    @Override
    @Transactional
    public boolean updateCost(Double cost) {
        if (cost != null) {
            List<GoldEntity> goldDTOs = goldDAO.findAll();
            if (goldDTOs != null && goldDTOs.size() > 0) {
                GoldEntity goldEntity = goldDTOs.get(0);
                goldEntity.setCost(cost);
                goldDAO.merge(goldEntity);
            }else {
                GoldEntity goldEntity = new GoldEntity();
                goldEntity.setCost(cost);
                goldDAO.persist(goldEntity);
            }
        }
        return true;
    }

    @Override
    @Transactional
    public Double getCost() {
        GoldEntity goldEntity = goldDAO.findAll().get(0);
        return dozerMapper.map(goldEntity, GoldDTO.class).getCost();
    }

    @Override
    protected GenericDAO getDAO() {
        return goldDAO;
    }
}
