package com.jewelry.service.impl;

import com.jewelry.dao.GenericDAO;
import com.jewelry.dao.StonePriceDAO;
import com.jewelry.dto.StonePriceDTO;
import com.jewelry.entity.StonePriceEntity;
import com.jewelry.service.StonePriceService;
import com.jewelry.util.SystemLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aramayis.antonyan on 3/3/14.
 */
@Service
public class StonePriceServiceImpl extends GenericService<StonePriceDTO, StonePriceEntity> implements StonePriceService {

    @Autowired
    private StonePriceDAO stonePriceDAO;

    @Override
    @Transactional
    public List<StonePriceDTO> getPrices() {
        List<StonePriceEntity> typeEntities = stonePriceDAO.findAll();
        List<StonePriceDTO> priceDTOs = new ArrayList<StonePriceDTO>();
        for (StonePriceEntity typeEntity : typeEntities) {
            priceDTOs.add(dozerMapper.map(typeEntity, StonePriceDTO.class));
        }
        return priceDTOs;
    }

    /*if (productEntity.getId() != null) {
        productDAO.merge(productEntity);
    } else {
        productDAO.persist(productEntity);
    }*/

    @Override
    @Transactional
    public boolean updatePrice(List<StonePriceDTO> stonePrices) {
        for (StonePriceDTO stonePrice : stonePrices) {
            try {
                stonePriceDAO.merge(dozerMapper.map(stonePrice, StonePriceEntity.class));
            } catch (Exception e) {
                SystemLogger.e("Error while update price");
                return false;
            }
        }
        return true;
    }

    @Override
    @Transactional
    public List<StonePriceDTO> addPrice(List<StonePriceDTO> stonePrices) {
        for (StonePriceDTO stonePrice : stonePrices) {
            try {
                StonePriceEntity stonePriceEntity = dozerMapper.map(stonePrice, StonePriceEntity.class);
                stonePriceDAO.persist(stonePriceEntity);
                stonePrice.setId(stonePriceEntity.getId());
            } catch (Exception e) {
                SystemLogger.e("Error while insert price");
                return null;
            }
        }
        return stonePrices;
    }

   /* @Override
    @Transactional
    public StonePriceDTO getPrice(Integer stoneId) {
        return dozerMapper.map(stonePriceDAO.find(stoneId), StonePriceDTO.class);
    }*/

    @Override
    protected GenericDAO getDAO() {
        return stonePriceDAO;
    }
}
