package com.jewelry.service.impl;

import com.jewelry.dao.GenericDAO;
import com.jewelry.dao.StoneQualityDAO;
import com.jewelry.dto.StoneQualityDTO;
import com.jewelry.entity.StoneQualityEntity;
import com.jewelry.exception.QualityNotFoundException;
import com.jewelry.service.StoneQualityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

;

/**
 * Created by aramayis.antonyan on 4/16/14.
 */
@Service
public class StoneQualityServiceImpl extends GenericService<StoneQualityDTO, StoneQualityEntity> implements StoneQualityService {

    @Autowired
    private StoneQualityDAO stoneQualityDao;

    @Override
    protected GenericDAO getDAO() {
        return stoneQualityDao;
    }

    @Override
    public List<StoneQualityDTO> getQualityList() {
        List<StoneQualityEntity> qualityEntity = stoneQualityDao.findAll();
        List<StoneQualityDTO> qualityList = new ArrayList<StoneQualityDTO>();
        for (StoneQualityEntity quality : qualityEntity) {
            qualityList.add(dozerMapper.map(quality, StoneQualityDTO.class));
        }
        return qualityList;
    }

    @Override
    @Transactional
    public Integer saveQuality(StoneQualityDTO qualityDTO) {
        StoneQualityEntity qualityEntity = dozerMapper.map(qualityDTO, StoneQualityEntity.class);
        stoneQualityDao.persist(qualityEntity);
        return qualityEntity.getId();
    }

    @Override
    @Transactional
    public Boolean delete(Integer qualityId) {
         StoneQualityEntity qualityEntity = stoneQualityDao.find(qualityId);
            if (qualityEntity != null) {
                qualityEntity.setDeleted(true);
                stoneQualityDao.merge(qualityEntity);
                return true;
            }
            throw new QualityNotFoundException("Cant find quality to delete");
    }

}
