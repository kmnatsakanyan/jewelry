package com.jewelry.service.impl;

import com.jewelry.dao.GenericDAO;
import com.jewelry.dao.OrderTypePriceDAO;
import com.jewelry.dto.OrderType;
import com.jewelry.dto.OrderTypePriceDTO;
import com.jewelry.entity.OrderTypePriceEntity;
import com.jewelry.service.OrderTypePriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by aramayis.antonyan .
 */
@Service
public class OrderTypePriceServiceImpl extends GenericService<OrderTypePriceDTO, OrderTypePriceEntity> implements OrderTypePriceService {


   @Autowired
    private OrderTypePriceDAO orderTypePriceDAO;

    @Override
    @Transactional
    public boolean updatePrice(OrderType type , Double price) {
        orderTypePriceDAO.updatePrice(type, price);
       return true;
    }

    @Override
    public List<OrderTypePriceDTO> getOrderTypePrices() {
        List<OrderTypePriceDTO> orderTypePriceDTOs = new ArrayList<OrderTypePriceDTO>();
        List<OrderTypePriceEntity> orderTypePrices = orderTypePriceDAO.findAll();

        for (OrderTypePriceEntity orderPriceEntity : orderTypePrices) {
            OrderTypePriceDTO orderPriceDTO = dozerMapper.map(orderPriceEntity, OrderTypePriceDTO.class);
            orderTypePriceDTOs.add(orderPriceDTO);
        }
        return orderTypePriceDTOs;
    }

    @Override
    protected GenericDAO getDAO() {
        return orderTypePriceDAO;
    }
}
