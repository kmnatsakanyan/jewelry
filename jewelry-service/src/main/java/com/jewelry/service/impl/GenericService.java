package com.jewelry.service.impl;

import com.jewelry.dao.GenericDAO;
import com.jewelry.util.SystemLogger;
import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public abstract class GenericService<DTO, ENTITY> {

    private Class<DTO> type;

    @SuppressWarnings("unchecked")
    public GenericService() {
        type = (Class<DTO>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Autowired
    protected Mapper dozerMapper;

    @SuppressWarnings("unchecked")
    @Transactional
    public List<DTO> getUpdates(DateTime startDate) {
        List<ENTITY> productEntities;
        if (startDate == null) {
            productEntities = getDAO().findAll();
        } else {
            productEntities = getDAO().findUpdatedRecords(startDate);
        }
        List<DTO> productDTOs = new ArrayList<DTO>();
        for (ENTITY productEntity : productEntities) {
            try {
                productDTOs.add(dozerMapper.map(productEntity, type));
            }catch (Exception ex){
                SystemLogger.e(ex.getMessage());
            }
        }
        return productDTOs;
    }

    protected abstract GenericDAO getDAO();
}
