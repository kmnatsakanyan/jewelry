package com.jewelry.service.impl;

import com.jewelry.dao.DiamondWeightDAO;
import com.jewelry.entity.DiamondWeightEntity;
import com.jewelry.service.DBPopulatorService;
import com.jewelry.util.ServiceConfigs;
import com.jewelry.util.SystemLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mos on 12.05.14.
 */
@Service
public class DBPopulatorServiceImpl implements DBPopulatorService {

    @Autowired
    private ServiceConfigs serviceConfigs;

    @Autowired
    DiamondWeightDAO diamondWeightDAO;

    @Override
    @Transactional
    public void loadDiamondWeightFromCSV() {
        try {
            InputStream is = this.getClass().getResourceAsStream(serviceConfigs.getDiametersFile());
            Reader reader = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(reader);
            List<DiamondWeightEntity> diamondList = getDiamondList(br);
            saveDiamondWeights(diamondList);
        } catch (IOException e) {
            SystemLogger.e("was not able to complete file import for " + serviceConfigs.getDiametersFile());
        }
    }

    @Override
    public DiamondWeightEntity readSCVRow(String row) {
        String[] r = row.split(",");
        DiamondWeightEntity diamondWeightEntity = new DiamondWeightEntity();
        diamondWeightEntity.setDiameter(Float.valueOf(r[0]));
        diamondWeightEntity.setMax(Float.valueOf(r[1]));
        diamondWeightEntity.setMin(Float.valueOf(r[2]));
        return diamondWeightEntity;
    }

    public List<DiamondWeightEntity> getDiamondList(BufferedReader reader) throws IOException {
        List<DiamondWeightEntity> diamondWeightEntities = new ArrayList<DiamondWeightEntity>();
        String line = "";
        while ((line = reader.readLine()) != null) {
            diamondWeightEntities.add(readSCVRow(line));
        }

        return diamondWeightEntities;
    }


    private void saveDiamondWeights(List<DiamondWeightEntity> diamondWeightEntities) {
        diamondWeightDAO.deleteAllRows();
        for (DiamondWeightEntity diamondWeightEntity : diamondWeightEntities) {
            diamondWeightDAO.persist(diamondWeightEntity);
        }
    }

}
