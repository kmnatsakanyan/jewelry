package com.jewelry.service.impl;

import com.jewelry.dao.GenericDAO;
import com.jewelry.dao.StoneDiameterDAO;
import com.jewelry.dto.StoneDiameterDTO;
import com.jewelry.entity.StoneDiameterEntity;
import com.jewelry.exception.DiameterNotFoundException;
import com.jewelry.service.StoneDiameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
@Service
public class StoneDiametersServiceImpl extends GenericService<StoneDiameterDTO, StoneDiameterEntity> implements StoneDiameterService {

    @Autowired
    private StoneDiameterDAO stoneDiameterDao;

    @Override
    @Transactional
    public List<StoneDiameterDTO> getDiametersList() {
        List<StoneDiameterEntity> diametersEntity = stoneDiameterDao.findAll();
        List<StoneDiameterDTO> diameters = new ArrayList<StoneDiameterDTO>();
        for (StoneDiameterEntity diameter : diametersEntity) {
            diameters.add(dozerMapper.map(diameter, StoneDiameterDTO.class));
        }
        return diameters;
    }

    @Override
    protected GenericDAO getDAO() {
        return stoneDiameterDao;
    }

    @Override
    @Transactional
    public Integer saveDiameter(StoneDiameterDTO diameterDTO) {
        StoneDiameterEntity diameterEntity = dozerMapper.map(diameterDTO, StoneDiameterEntity.class);
        stoneDiameterDao.persist(diameterEntity);
        stoneDiameterDao.flush();
        return diameterEntity.getId();

    }

    @Override
    @Transactional
    public Boolean delete(Integer qualityId) {
        StoneDiameterEntity diameterEntity = stoneDiameterDao.find(qualityId);
        if (diameterEntity != null) {
            diameterEntity.setDeleted(true);
            stoneDiameterDao.merge(diameterEntity);
            return true;
        }
        throw new DiameterNotFoundException("Cant find diameter to delete");
    }
}
