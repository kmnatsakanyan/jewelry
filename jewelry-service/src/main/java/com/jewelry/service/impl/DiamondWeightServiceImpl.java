package com.jewelry.service.impl;

import com.jewelry.dao.DiamondWeightDAO;
import com.jewelry.dao.GenericDAO;
import com.jewelry.dto.DiamondWeightDTO;
import com.jewelry.entity.DiamondWeightEntity;
import com.jewelry.service.DBPopulatorService;
import com.jewelry.service.DiamondWeightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aramayis.antonyan on 5/10/14.
 */
@Service
public class DiamondWeightServiceImpl extends GenericService<DiamondWeightDTO, DiamondWeightEntity> implements DiamondWeightService {

    @Autowired
    private DiamondWeightDAO diamondWeightDAO;

    @Autowired
    private DBPopulatorService dbPopulatorService;

    @Override
    @Transactional
    public List<DiamondWeightDTO> getWeightList() {
        List<DiamondWeightEntity> weightEntity = diamondWeightDAO.findAll();
        List<DiamondWeightDTO> weightList = new ArrayList<DiamondWeightDTO>();
        for (DiamondWeightEntity weight : weightEntity) {
            weightList.add(dozerMapper.map(weight, DiamondWeightDTO.class));
        }
        return weightList;
    }

    @Override
    public void loadDiamondWeightFromCSV() {
        dbPopulatorService.loadDiamondWeightFromCSV();
    }

    @Override
    protected GenericDAO getDAO() {
        return diamondWeightDAO;
    }
}
