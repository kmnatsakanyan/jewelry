package com.jewelry.service;

import com.jewelry.dto.DiamondWeightDTO;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
public interface DiamondWeightService {
	public List<DiamondWeightDTO> getWeightList();
    public List<DiamondWeightDTO> getUpdates(DateTime startDate);

    void loadDiamondWeightFromCSV();
}
