package com.jewelry.entity;

import javax.persistence.*;

/**
 * Created by aramayis.antonyan on 2/26/14.
 */
@Entity
@Table(name = "GOLD")
public class GoldEntity extends BaseEntity {

    @Column(name = "COST")
    private Double cost;

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }
}
