package com.jewelry.entity;

import javax.persistence.*;

/**
 * Created by aramayis.antonyan on 2/28/14.
 */
@Entity
@Table(name = "STONE_QUALITY")
public class StoneQualityEntity extends BaseEntity {

    @Column(name = "QUALITY")
    private String quality;

    @Column(name = "STONE_ID")
    private Integer stoneId;


    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public Integer getStoneId() {
        return stoneId;
    }

    public void setStoneId(Integer stoneId) {
        this.stoneId = stoneId;
    }
}
