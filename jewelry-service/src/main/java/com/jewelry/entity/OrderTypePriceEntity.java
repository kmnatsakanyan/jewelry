package com.jewelry.entity;

import com.jewelry.dto.OrderType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by aramayis.antonyan on 2/26/14.
 */
@Entity
@Table(name = "OrderTypePrice")
public class OrderTypePriceEntity extends BaseEntity {

    @Column(name = "COST")
    private Double cost;

    @Column(name = "TYPE")
    private OrderType type;



    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public OrderType getType() {
        return type;
    }

    public void setType(OrderType type) {
        this.type = type;
    }
}
