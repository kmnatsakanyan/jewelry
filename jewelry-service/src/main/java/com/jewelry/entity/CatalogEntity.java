package com.jewelry.entity;

import org.codehaus.jackson.annotate.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
@Entity
@Table(name = "CATALOG")
public class CatalogEntity extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "catalog", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<ProductCatalogEntity> productCatalogs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductCatalogEntity> getProductCatalogs() {
        return productCatalogs;
    }

    public void setProductCatalogs(List<ProductCatalogEntity> productCatalogs) {
        this.productCatalogs = productCatalogs;
    }
}
