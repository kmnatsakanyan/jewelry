package com.jewelry.entity;


import javax.persistence.*;

/**
 * Created by Mos on 27.02.14.
 */
@Entity
@Table(name = "PRODUCT_TYPE")
public class ProductTypeEntity extends BaseEntity{

    @Column(name = "NAME")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}