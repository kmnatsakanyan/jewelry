package com.jewelry.entity;

import javax.persistence.*;

/**
 * Created by aramayis.antonyan on 2/28/14.
 */
@Entity
@Table(name = "STONE_PRICE")
public class StonePriceEntity extends BaseEntity {

    @Column(name = "STONE_ID")
    private Integer stoneID;

    @Column(name = "DIAMETER_ID")
    private Integer diameterID;

    @Column(name = "QUALITY_ID")
    private Integer qualityID;

    @Column(name = "COST")
    private Float cost;

    public Integer getStoneID() {
        return stoneID;
    }

    public void setStoneID(Integer stoneID) {
        this.stoneID = stoneID;
    }

    public Integer getDiameterID() {
        return diameterID;
    }

    public void setDiameterID(Integer diameterID) {
        this.diameterID = diameterID;
    }

    public Integer getQualityID() {
        return qualityID;
    }

    public void setQualityID(Integer qualityID) {
        this.qualityID = qualityID;
    }

    public Float getCost() {
        return cost;
    }

    public void setCost(Float cost) {
        this.cost = cost;
    }
}
