package com.jewelry.entity;

import javax.persistence.*;

/**
 * Created by Mos on 04.03.14.
 */
@Entity
@Table(name = "STONE_ORDER")
public class StoneOrderEntity extends BaseEntity {

    @Column(name = "ORDER_ID")
    private Integer orderId;

   /* @OneToOne
    @JoinColumn(name = "TYPE_ID", referencedColumnName = "ID")
    private StoneTypeEntity type;*/

    @OneToOne
    @JoinColumn(name = "QUALITY_ID", referencedColumnName = "ID")
    private StoneQualityEntity qualityId;


    @Column(name = "DIAMETER")
    private Float diameter;


    @Column(name = "COUNT")
    private Integer count;


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }


   /* public StoneTypeEntity getType() {
        return type;
    }

    public void setType(StoneTypeEntity type) {
        this.type = type;
    }*/


    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Float getDiameter() {
        return diameter;
    }

    public void setDiameter(Float diameter) {
        this.diameter = diameter;
    }

    public StoneQualityEntity getQualityId() {
        return qualityId;
    }

    public void setQualityId(StoneQualityEntity qualityId) {
        this.qualityId = qualityId;
    }
}
