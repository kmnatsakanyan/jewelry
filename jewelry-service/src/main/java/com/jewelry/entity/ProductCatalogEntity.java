package com.jewelry.entity;

import javax.persistence.*;

/**
 * User: karen.mnatsakanyan
 */
@Entity
@Table(name = "PRODUCT_CATALOG")
public class ProductCatalogEntity extends BaseEntity {

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "PRODUCT_ID")
    private ProductEntity product;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "CATALOG_ID")
    private CatalogEntity catalog;

    @Column
    private String productLabel;

    public ProductEntity getProduct() {
        return product;
    }

    public void setProduct(ProductEntity product) {
        this.product = product;
    }

    public CatalogEntity getCatalog() {
        return catalog;
    }

    public void setCatalog(CatalogEntity catalog) {
        this.catalog = catalog;
    }

    public String getProductLabel() {
        return productLabel;
    }

    public void setProductLabel(String productLabel) {
        this.productLabel = productLabel;
    }
}
