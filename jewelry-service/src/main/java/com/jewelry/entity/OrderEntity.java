package com.jewelry.entity;

import com.jewelry.dto.GoldType;
import com.jewelry.dto.OrderType;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
@Entity
@Table(name = "PRODUCT_ORDER")
public class OrderEntity extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID")
    private ProductEntity product;

    @Column(name = "COUNT")
    private Integer count;


    @Column(name = "DONE_COUNT")
    private Integer done;

    @Column(name = "DATE")
    @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTime")
    private DateTime date;

    @OneToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    private UserEntity user;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ORDER_ID")
    private List<StoneOrderEntity> stones;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "GOLD_TYPE", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private GoldType goldType;


    @Column(name = "ORDER_TYPE", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private OrderType orderType;


    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public ProductEntity getProduct() {
        return product;
    }

    public void setProduct(ProductEntity productEntity) {
        this.product = productEntity;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public List<StoneOrderEntity> getStones() {
        return stones;
    }

    public void setStones(List<StoneOrderEntity> stones) {
        this.stones = stones;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GoldType getGoldType() {
        return goldType;
    }

    public void setGoldType(GoldType goldType) {
        this.goldType = goldType;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public Integer getDone() {
        return done;
    }

    public void setDone(Integer done) {
        this.done = done;
    }
}
