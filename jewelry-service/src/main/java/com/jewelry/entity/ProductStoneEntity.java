package com.jewelry.entity;

import javax.persistence.*;

/**
 * Created by aramayis.antonyan on 2/26/14.
 */
@Entity
@Table(name = "PRODUCT_STONE")
public class ProductStoneEntity extends BaseEntity{
    @Column(name = "PRODUCT_ID")
    private Integer productId;

    @Column(name = "DIAMETER")
    private Float diameter;

    @Column(name = "COUNT")
    private Integer count;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }


    public Float getDiameter() {
        return diameter;
    }

    public void setDiameter(Float diameter) {
        this.diameter = diameter;
    }
}
