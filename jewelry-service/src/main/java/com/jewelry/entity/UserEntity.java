package com.jewelry.entity;

import com.jewelry.dto.UserRole;

import javax.persistence.*;

@Entity
@Table(name = "USER")
public class UserEntity extends BaseEntity {
    public static final String EMAIL = "EMAIL";
    public static final String FIRSTNAME = "FIRSTNAME";
    public static final String LASTNAME = "LASTNAME";
    public static final String TELEPHONE = "TELEPHONE";
    public static final String ROLE = "ROLE";
    public static final String PASSWORD = "PASSWORD";

    @Column(name = FIRSTNAME)
    private String firstname;

    @Column(name = LASTNAME)
    private String lastname;

    @Column(name = EMAIL)
    private String email;

    @Column(name = TELEPHONE)
    private String telephone;

    @Column(name = ROLE, nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private UserRole role;

    @Column(name = PASSWORD)
    private String password;

    public String getEmail() {
        return email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}