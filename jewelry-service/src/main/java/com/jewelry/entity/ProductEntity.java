package com.jewelry.entity;


import javax.persistence.*;
import java.util.List;


/**
 * Created by Mos on 25.02.14.
 */
@Entity
@Table(name = "PRODUCT")
public class ProductEntity extends BaseEntity {

    @Column(name = "PRODUCT_ID")
    private String productId;

    @Column(name = "TYPE")
    private Integer type;

    @Column(name = "WEIGHT")
    private Float weight;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "PRODUCT_ID")
    private List<ProductPhotoEntity> productPhotos;


    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "PRODUCT_ID")
    private List<ProductStoneEntity> productStones;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<ProductCatalogEntity> productCatalogs;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public List<ProductPhotoEntity> getProductPhotos() {
        return productPhotos;
    }

    public void setProductPhotos(List<ProductPhotoEntity> productPhotos) {
        this.productPhotos = productPhotos;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public List<ProductStoneEntity> getProductStones() {
        return productStones;
    }

    public void setProductStones(List<ProductStoneEntity> productStones) {
        this.productStones = productStones;
    }

    public List<ProductCatalogEntity> getProductCatalogs() {
        return productCatalogs;
    }

    public void setProductCatalogs(List<ProductCatalogEntity> productCatalogs) {
        this.productCatalogs = productCatalogs;
    }
}
