package com.jewelry.entity;


import javax.persistence.*;

/**
 * Created by aramayis.antonyan on 2/28/14.
 */
@Entity
@Table(name = "STONE")
public class StoneTypeEntity extends BaseEntity {

    @Column(name = "NAME")
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
