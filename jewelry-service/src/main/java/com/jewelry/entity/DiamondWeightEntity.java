package com.jewelry.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by aramayis.antonyan on 5/10/14.
 */
@Entity
@Table(name = "DIAMOND_WEIGHT")
public class DiamondWeightEntity extends BaseEntity{

    @Column(name = "DIAMETER")
    private Float diameter;

    @Column(name = "MAX")
    private Float max;

    @Column(name = "MIN")
    private Float min;


    public Float getDiameter() {
        return diameter;
    }

    public void setDiameter(Float diameter) {
        this.diameter = diameter;
    }

    public Float getMax() {
        return max;
    }

    public void setMax(Float max) {
        this.max = max;
    }

    public Float getMin() {
        return min;
    }

    public void setMin(Float min) {
        this.min = min;
    }
}
