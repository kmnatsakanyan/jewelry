package com.jewelry.entity;

import javax.persistence.*;

/**
 * User: karen.mnatsakanyan
 */
@Entity
@Table(name = "PRODUCT_PHOTO")
public class ProductPhotoEntity extends BaseEntity{

    @Column(name = "PRODUCT_ID")
    private Integer productId;

    @Column(name = "PHOTO_FILE")
    private String photoFile;

    @Column(name = "PHOTO_THUMBNAIL_FILE")
    private String photoThumbnailFile;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getPhotoFile() {
        return photoFile;
    }

    public void setPhotoFile(String photoFile) {
        this.photoFile = photoFile;
    }

    public String getPhotoThumbnailFile() {
        return photoThumbnailFile;
    }

    public void setPhotoThumbnailFile(String photoThumbnailFile) {
        this.photoThumbnailFile = photoThumbnailFile;
    }
}
