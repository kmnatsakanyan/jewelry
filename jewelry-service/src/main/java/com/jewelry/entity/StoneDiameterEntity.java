package com.jewelry.entity;

import javax.persistence.*;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
@Entity
@Table(name = "STONE_DIAMETER")
public class StoneDiameterEntity extends BaseEntity {

    @Column(name = "START")
    private Float start;

    @Column(name = "END")
    private Float end;

    @Column(name = "STONE_ID")
    private Integer stoneId;


    public Float getStart() {
        return start;
    }

    public void setStart(Float start) {
        this.start = start;
    }

    public Float getEnd() {
        return end;
    }

    public void setEnd(Float end) {
        this.end = end;
    }

    public Integer getStoneId() {
        return stoneId;
    }

    public void setStoneId(Integer stoneId) {
        this.stoneId = stoneId;
    }
}
