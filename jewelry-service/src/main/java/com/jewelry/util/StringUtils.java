package com.jewelry.util;

import java.lang.reflect.Method;

/**
 * User: karen.mnatsakanyan
 */
public class StringUtils {

    public static String objectToString(Object obj) {
        if(!SystemLogger.isDebug()){
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        // Get the public methods associated with this class.
        Method[] methods = obj.getClass().getMethods();
        Method[] superMethods  = obj.getClass().getSuperclass().getMethods();

        for (Method method : methods) {
            if (method.getName().startsWith("get")) {
                try {
                    Object result = method.invoke(obj);
                    stringBuilder.append(method.getName()).append(":").append(result).append("\n");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        for (Method method : superMethods) {
            if (method.getName().startsWith("get")) {
                try {
                    Object result = method.invoke(obj);
                    stringBuilder.append(method.getName()).append(":").append(result).append("\n");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return stringBuilder.toString();
    }
}
