package com.jewelry.util;

/**
 * User: karen.mnatsakanyan
 */

import java.util.HashMap;

/**
 * Map file extensions to MIME types. Based on the Apache mime.types file.
 * http://www.iana.org/assignments/media-types/
 */
public class MimeTypes {
    public static final String MIME_IMAGE_BMP = "image/bmp";
    public static final String MIME_IMAGE_CGM = "image/cgm";
    public static final String MIME_IMAGE_GIF = "image/gif";
    public static final String MIME_IMAGE_IEF = "image/ief";
    public static final String MIME_IMAGE_JPEG = "image/jpeg";
    public static final String MIME_IMAGE_TIFF = "image/tiff";
    public static final String MIME_IMAGE_PNG = "image/png";
    public static final String MIME_IMAGE_SVG_XML = "image/svg+xml";
    public static final String MIME_IMAGE_VND_DJVU = "image/vnd.djvu";
    public static final String MIME_IMAGE_WAP_WBMP = "image/vnd.wap.wbmp";
    public static final String MIME_IMAGE_X_CMU_RASTER = "image/x-cmu-raster";
    public static final String MIME_IMAGE_X_ICON = "image/x-icon";
    public static final String MIME_IMAGE_X_PORTABLE_ANYMAP = "image/x-portable-anymap";
    public static final String MIME_IMAGE_X_PORTABLE_BITMAP = "image/x-portable-bitmap";
    public static final String MIME_IMAGE_X_PORTABLE_GRAYMAP = "image/x-portable-graymap";
    public static final String MIME_IMAGE_X_PORTABLE_PIXMAP = "image/x-portable-pixmap";
    public static final String MIME_IMAGE_X_RGB = "image/x-rgb";

    private static HashMap<String, String> mimeTypeMapping;

    static {
        mimeTypeMapping = new HashMap<String, String>(18);
        mimeTypeMapping.put(MIME_IMAGE_X_RGB, "rgb");
        mimeTypeMapping.put(MIME_IMAGE_X_PORTABLE_PIXMAP, "ppm");
        mimeTypeMapping.put(MIME_IMAGE_X_PORTABLE_GRAYMAP, "pgm");
        mimeTypeMapping.put(MIME_IMAGE_X_PORTABLE_BITMAP, "pbm");
        mimeTypeMapping.put(MIME_IMAGE_X_PORTABLE_ANYMAP, "pnm");
        mimeTypeMapping.put(MIME_IMAGE_X_ICON, "ico");
        mimeTypeMapping.put(MIME_IMAGE_X_CMU_RASTER, "ras");
        mimeTypeMapping.put(MIME_IMAGE_WAP_WBMP, "wbmp");
        mimeTypeMapping.put(MIME_IMAGE_VND_DJVU, "djv");
        mimeTypeMapping.put(MIME_IMAGE_VND_DJVU, "djvu");
        mimeTypeMapping.put(MIME_IMAGE_SVG_XML, "svg");
        mimeTypeMapping.put(MIME_IMAGE_IEF, "ief");
        mimeTypeMapping.put(MIME_IMAGE_CGM, "cgm");
        mimeTypeMapping.put(MIME_IMAGE_BMP, "bmp");
        mimeTypeMapping.put(MIME_IMAGE_GIF, "gif");
        mimeTypeMapping.put(MIME_IMAGE_JPEG, "jpg");
        mimeTypeMapping.put(MIME_IMAGE_TIFF, "tiff");
        mimeTypeMapping.put(MIME_IMAGE_TIFF, "tif");
        mimeTypeMapping.put(MIME_IMAGE_PNG, "png");
    }

    /**
     * Registers MIME type for provided extension. Existing extension type will be overriden.
     */
    public static void registerMimeType(String ext, String mimeType) {
        mimeTypeMapping.put(ext, mimeType);
    }

    /**
     * Simply returns MIME type or <code>null</code> if no type is found.
     */
    public static String lookupMimeType(String ext) {
        return mimeTypeMapping.get(ext.toLowerCase());
    }
}

