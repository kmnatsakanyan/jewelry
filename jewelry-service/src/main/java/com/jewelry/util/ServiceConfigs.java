package com.jewelry.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * User: karen.mnatsakanyan
 */
@Component("serviceConf")
public class ServiceConfigs {

    @Value("${photosURL}")
    private String photosURL;

    @Value("${diametersFile}")
    private String diametersFile;

    public String getPhotosURL() {
        return photosURL;
    }

    public void setPhotosURL(String photosURL) {
        this.photosURL = photosURL;
    }

    public String getDiametersFile() {
        return diametersFile;
    }

    public void setDiametersFile(String diametersFile) {
        this.diametersFile = diametersFile;
    }
}
