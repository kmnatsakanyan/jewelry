package com.jewelry.dao;

import com.jewelry.entity.StoneDiameterEntity;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
public interface StoneDiameterDAO extends GenericDAO<StoneDiameterEntity, Integer>{
}
