package com.jewelry.dao;

import com.jewelry.entity.OrderEntity;
import org.joda.time.DateTime;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public interface OrderDAO extends GenericDAO<OrderEntity, Integer> {
    List<OrderEntity> findUpdatedRecords(DateTime startDate, Integer userId);
}
