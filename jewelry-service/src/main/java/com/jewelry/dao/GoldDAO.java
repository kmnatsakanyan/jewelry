package com.jewelry.dao;

import com.jewelry.entity.GoldEntity;

/**
 * Created by aramayis.antonyan on 2/26/14.
 */

public interface GoldDAO extends GenericDAO<GoldEntity, Integer> {

}
