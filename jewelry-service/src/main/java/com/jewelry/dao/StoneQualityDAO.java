package com.jewelry.dao;


import com.jewelry.entity.StoneQualityEntity;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
public interface StoneQualityDAO extends GenericDAO<StoneQualityEntity, Integer>{
}
