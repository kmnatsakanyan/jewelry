package com.jewelry.dao;

import com.jewelry.entity.CatalogEntity;

/**
 * User: karen.mnatsakanyan
 */
public interface CatalogDAO extends GenericDAO<CatalogEntity, Integer> {

}
