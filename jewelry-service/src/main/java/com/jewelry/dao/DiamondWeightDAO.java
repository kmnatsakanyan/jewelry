package com.jewelry.dao;

import com.jewelry.entity.DiamondWeightEntity;


/**
 * Created by aramayis.antonyan on 5/10/14.
 */
public interface DiamondWeightDAO extends GenericDAO<DiamondWeightEntity, Integer>{

    public void deleteAllRows();

}
