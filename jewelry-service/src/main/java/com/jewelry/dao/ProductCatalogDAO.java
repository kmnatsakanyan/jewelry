package com.jewelry.dao;

import com.jewelry.entity.ProductCatalogEntity;

/**
 * User: karen.mnatsakanyan
 */
public interface ProductCatalogDAO extends GenericDAO<ProductCatalogEntity, Integer> {
}
