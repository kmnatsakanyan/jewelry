package com.jewelry.dao;

import org.joda.time.DateTime;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public interface GenericDAO<T, ID> {

    public T find(ID id);

    public List<T> findAll();

    public void persist(T entity);

    public void merge(T entity);

    public void remove(T entity);

    public long count();

    public List<T> findUpdatedRecords(DateTime startingFrom);

    void flush();

    void clear();

    void setEntityManager(EntityManager entityManager);
}
