package com.jewelry.dao;

import com.jewelry.dto.OrderType;
import com.jewelry.entity.OrderTypePriceEntity;

/**
 * Created by aramayis.antonyan on 2/26/14.
 */

public interface OrderTypePriceDAO extends GenericDAO<OrderTypePriceEntity, Integer> {
    public  void updatePrice(OrderType type, Double price);
}
