package com.jewelry.dao;

import com.jewelry.entity.ProductEntity;

/**
 * Created by Mos on 25.02.14.
 */
public interface ProductDAO extends GenericDAO<ProductEntity, Integer>{
}
