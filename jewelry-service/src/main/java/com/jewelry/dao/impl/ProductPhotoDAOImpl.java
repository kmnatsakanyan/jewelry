package com.jewelry.dao.impl;

import com.jewelry.dao.ProductPhotoDAO;
import com.jewelry.entity.ProductPhotoEntity;
import org.springframework.stereotype.Repository;

/**
 * User: karen.mnatsakanyan
 */
@Repository("productPhotoDAO")
public class ProductPhotoDAOImpl extends GenericDAOImpl<ProductPhotoEntity, Integer> implements ProductPhotoDAO {

}
