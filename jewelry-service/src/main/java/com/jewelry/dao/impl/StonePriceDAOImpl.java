package com.jewelry.dao.impl;

import com.jewelry.dao.StonePriceDAO;
import com.jewelry.entity.StonePriceEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by aramayis.antonyan on 3/3/14.
 */
@Repository("stonePriceDAO")
public class StonePriceDAOImpl extends GenericDAOImpl<StonePriceEntity, Integer> implements StonePriceDAO {
}
