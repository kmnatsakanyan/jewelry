package com.jewelry.dao.impl;

import com.jewelry.dao.ProductDAO;
import com.jewelry.entity.ProductEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Mos on 25.02.14.
 */
@Repository("productDAO")
public class ProductDaoImpl extends GenericDAOImpl<ProductEntity, Integer> implements ProductDAO {
}
