package com.jewelry.dao.impl;

import com.jewelry.dao.OrderDAO;
import com.jewelry.entity.OrderEntity;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
@Repository("orderDAO")
public class OrderDAOImpl extends GenericDAOImpl<OrderEntity, Integer> implements OrderDAO {

    @Override
    public List<OrderEntity> findUpdatedRecords(DateTime startDate, Integer userId) {
        return entityManager.createQuery("Select t from OrderEntity  t  where t.updateDate > :date and t.user.id  = :userId")
                .setParameter("date", startDate)
                .setParameter("userId",userId)
                .getResultList();
    }
}
