package com.jewelry.dao.impl;

import com.jewelry.dao.OrderTypePriceDAO;
import com.jewelry.dto.OrderType;
import com.jewelry.entity.OrderTypePriceEntity;
import org.hibernate.criterion.Restrictions;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

/**
 * Created by aramayis.antonyan .
 */
@Repository("orderTypePriceDAO")
public class OrderTypePriceDaoImpl extends GenericDAOImpl<OrderTypePriceEntity, Integer> implements OrderTypePriceDAO {

    @Override
    public void updatePrice(OrderType type, Double price) {
        Criteria criteria = entityManager.unwrap(Session.class).createCriteria(OrderTypePriceEntity.class);
        criteria.add(Restrictions.eq("type", type));
        OrderTypePriceEntity orderTypePriceEntity = (OrderTypePriceEntity) criteria.uniqueResult();
        if (orderTypePriceEntity == null) {
            orderTypePriceEntity = new OrderTypePriceEntity();
            orderTypePriceEntity.setType(type);
            orderTypePriceEntity.setCost(price);
        }
        orderTypePriceEntity.setCost(price);
        entityManager.merge(orderTypePriceEntity);
    }
}
