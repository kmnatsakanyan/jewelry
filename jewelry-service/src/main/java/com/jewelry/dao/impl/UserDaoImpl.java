package com.jewelry.dao.impl;

import com.jewelry.dao.UserDAO;
import com.jewelry.entity.UserEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("userDAO")
public class UserDaoImpl extends GenericDAOImpl<UserEntity, Integer> implements UserDAO {

    @Override
    public UserEntity getUserByEmailAndPassword(String email, String password) {
        Criteria criteria = entityManager.unwrap(Session.class).createCriteria(UserEntity.class);
        criteria.add(Restrictions.and(Restrictions.eq("email", email), Restrictions.eq("password", password))).add(Restrictions.eq("isDeleted", false));
        return (UserEntity) criteria.uniqueResult();
    }

    @Override
    public UserEntity getUserByEmail(String email) {
        Criteria criteria = entityManager.unwrap(Session.class).createCriteria(UserEntity.class);
        criteria.add(Restrictions.eq("email", email)).add(Restrictions.eq("isDeleted", false));
        return (UserEntity) criteria.uniqueResult();
    }

}
