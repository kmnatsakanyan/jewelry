package com.jewelry.dao.impl;

import com.jewelry.dao.CatalogDAO;
import com.jewelry.entity.CatalogEntity;
import org.springframework.stereotype.Repository;

/**
 * User: karen.mnatsakanyan
 */
@Repository("catalogDAO")
public class CatalogDAOImpl extends GenericDAOImpl<CatalogEntity, Integer> implements CatalogDAO {
}
