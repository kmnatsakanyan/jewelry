package com.jewelry.dao.impl;

import com.jewelry.dao.ProductTypeDAO;
import com.jewelry.entity.ProductTypeEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Mos on 27.02.14.
 */
@Repository("typeDAO")
public class ProductTypeDaoImpl extends GenericDAOImpl<ProductTypeEntity, Integer> implements ProductTypeDAO {
}
