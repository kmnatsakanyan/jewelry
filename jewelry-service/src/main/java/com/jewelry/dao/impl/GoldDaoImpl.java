package com.jewelry.dao.impl;

import com.jewelry.dao.GoldDAO;
import com.jewelry.entity.GoldEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by aramayis.antonyan on 2/26/14.
 */
@Repository("goldDAO")
public class GoldDaoImpl extends GenericDAOImpl<GoldEntity, Integer> implements GoldDAO {
}
