package com.jewelry.dao.impl;

import com.jewelry.dao.StoneDiameterDAO;
import com.jewelry.entity.StoneDiameterEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
@Repository("diameterDAO")
public class StoneDiametersDaoImpl extends GenericDAOImpl<StoneDiameterEntity, Integer> implements StoneDiameterDAO {
}
