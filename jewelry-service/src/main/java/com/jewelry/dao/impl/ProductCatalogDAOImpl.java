package com.jewelry.dao.impl;

import com.jewelry.dao.ProductCatalogDAO;
import com.jewelry.entity.ProductCatalogEntity;
import org.springframework.stereotype.Repository;

/**
 * User: karen.mnatsakanyan
 */
@Repository("productCatalogDAO")
public class ProductCatalogDAOImpl extends GenericDAOImpl<ProductCatalogEntity, Integer> implements ProductCatalogDAO {
}
