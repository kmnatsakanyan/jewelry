package com.jewelry.dao.impl;


import com.jewelry.dao.StoneQualityDAO;
import com.jewelry.entity.StoneQualityEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
@Repository("qualityDAO")
public class StoneQualityDaoImpl extends GenericDAOImpl<StoneQualityEntity, Integer> implements StoneQualityDAO {
}
