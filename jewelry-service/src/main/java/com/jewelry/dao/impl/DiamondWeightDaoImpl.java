package com.jewelry.dao.impl;

import com.jewelry.dao.DiamondWeightDAO;
import com.jewelry.entity.DiamondWeightEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
@Repository("diamondDAO")
public class DiamondWeightDaoImpl extends GenericDAOImpl<DiamondWeightEntity, Integer> implements DiamondWeightDAO {
    @Override
    public void deleteAllRows() {
        entityManager.createQuery("DELETE from " + DiamondWeightEntity.class.getSimpleName()).executeUpdate();
    }
}
