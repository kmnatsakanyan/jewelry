package com.jewelry.dao;

import com.jewelry.entity.ProductPhotoEntity;

/**
 * User: karen.mnatsakanyan
 */
public interface ProductPhotoDAO extends GenericDAO<ProductPhotoEntity, Integer> {
}
