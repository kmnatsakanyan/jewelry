package com.jewelry.dao;

import com.jewelry.entity.StonePriceEntity;

/**
 * Created by aramayis.antonyan on 3/3/14.
 */
public interface StonePriceDAO extends GenericDAO<StonePriceEntity, Integer>{
}
