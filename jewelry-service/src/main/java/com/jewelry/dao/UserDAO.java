package com.jewelry.dao;

import com.jewelry.entity.UserEntity;

public interface UserDAO extends GenericDAO<UserEntity, Integer>{
    UserEntity getUserByEmailAndPassword(String email, String password);
    UserEntity getUserByEmail(String email);
}