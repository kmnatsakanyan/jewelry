package com.jewelry.dao;

import com.jewelry.entity.ProductTypeEntity;

/**
 * Created by Mos on 27.02.14.
 */
public interface ProductTypeDAO  extends GenericDAO<ProductTypeEntity, Integer> {
}
