package com.jewelry.security;

import com.jewelry.entity.UserEntity;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
public class User implements org.springframework.security.core.userdetails.UserDetails {
    private UserEntity userEntity;
    private List<Authority> authorityList = new ArrayList<Authority>();

    public User(UserEntity userEntity) {
        this.userEntity = userEntity;
        authorityList.add(new Authority(userEntity.getRole().toString()));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorityList;
    }

    @Override
    public String getPassword() {
        return userEntity.getPassword();
    }

    @Override
    public String getUsername() {
        return userEntity.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
