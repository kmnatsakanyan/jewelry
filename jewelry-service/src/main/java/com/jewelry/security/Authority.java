package com.jewelry.security;

import org.springframework.security.core.GrantedAuthority;

/**
 * User: karen.mnatsakanyan
 */
public class Authority implements GrantedAuthority {
    private String authority;

    public Authority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }

}
