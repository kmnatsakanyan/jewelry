package com.jewelry.resource.impl;

import com.jewelry.dto.UserDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.rs.UserResource;
import com.jewelry.service.UserService;
import com.jewelry.util.StringUtils;
import com.jewelry.util.SystemLogger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
@Service("userResource")
public class UserResourceImpl implements UserResource {

    @Autowired
    private UserService userServiceImpl;

    @Override
    public ValueDTO<UserDTO> saveUser(UserDTO userDTO) {
        SystemLogger.d("userResource:saveUser:" + StringUtils.objectToString(userDTO));
        userServiceImpl.saveUser(userDTO);
        return new ValueDTO<UserDTO>(userDTO);
    }

    @Override
    public ValueDTO<UserDTO> login(String email, String password) {
        SystemLogger.d("userResource:login:" + email + ":" + password);
        return new ValueDTO<UserDTO>(userServiceImpl.login(email, password));
    }

    @Override
    public ValueDTO<List<UserDTO>> getAll() {
        SystemLogger.d("userResource:getAll:");
        return new ValueDTO<List<UserDTO>>(userServiceImpl.getAll());
    }

    @Override
    public ValueDTO<UserDTO> getUser(Integer userId) {
        SystemLogger.d("userResource:getUser:" + userId);
        return new ValueDTO<UserDTO>(userServiceImpl.getUser(userId));
    }


    @Override
    public DateTime test() {
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail("alo");
        userDTO.setLastname("blo");
        StringUtils.objectToString(userDTO);
        SystemLogger.d(StringUtils.objectToString(userDTO));
        return new DateTime();
    }

    @Override
    public ValueDTO<Boolean> delete(Integer userId) {
        SystemLogger.d("userResource:delete:" + userId);
        return new ValueDTO<Boolean>(userServiceImpl.delete(userId));
    }

}
