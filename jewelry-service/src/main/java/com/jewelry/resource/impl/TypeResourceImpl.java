package com.jewelry.resource.impl;

import com.jewelry.dto.ProductTypeDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.rs.TypeResource;
import com.jewelry.service.ProductTypeService;
import com.jewelry.util.SystemLogger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
@Service("typeResource")
public class TypeResourceImpl implements TypeResource {

    @Autowired
    private ProductTypeService productTypeService;

    @Override
    public ValueDTO<List<ProductTypeDTO>> getTypes() {
        SystemLogger.d("typeResource:getTypes:");
        return new ValueDTO<List<ProductTypeDTO>>(productTypeService.getTypeList());
    }

    @Override
    public ValueDTO<List<ProductTypeDTO>> getUpdates(DateTime startDate) {
        SystemLogger.d("typeResource:getUpdates:"+startDate);
        return new ValueDTO<List<ProductTypeDTO>>(productTypeService.getUpdates(startDate));
    }
}
