package com.jewelry.resource.impl;

import com.jewelry.dto.ProductCatalogDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.rs.ProductCatalogResource;
import com.jewelry.service.ProductCatalogService;
import com.jewelry.util.StringUtils;
import com.jewelry.util.SystemLogger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
@Service("productCatalogResource")
public class ProductCatalogResourceImpl implements ProductCatalogResource {

    @Autowired
    private ProductCatalogService productCatalogService;

    @Override
    public ValueDTO<ProductCatalogDTO> addProductCatalog(ProductCatalogDTO productCatalogDTO) {
        SystemLogger.d("productCatalogResource:addProductCatalog:" + StringUtils.objectToString(productCatalogDTO));
        productCatalogDTO = productCatalogService.saveProductCatalog(productCatalogDTO);
        return new ValueDTO<ProductCatalogDTO>(productCatalogDTO);
    }

    @Override
    public ValueDTO<ProductCatalogDTO> updateProductCatalog(ProductCatalogDTO productCatalogDTO) {
        SystemLogger.d("productCatalogResource:updateProductCatalog:" + StringUtils.objectToString(productCatalogDTO));
        productCatalogDTO = productCatalogService.updateProductCatalog(productCatalogDTO);
        return new ValueDTO<ProductCatalogDTO>(productCatalogDTO);
    }

    @Override
    public ValueDTO<List<ProductCatalogDTO>> getUpdates(DateTime startDate) {
        SystemLogger.d("productCatalogResource:getUpdates:" + startDate);
        return new ValueDTO<List<ProductCatalogDTO>>(productCatalogService.getUpdates(startDate));
    }
}
