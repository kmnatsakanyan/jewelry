package com.jewelry.resource.impl;

import com.jewelry.dto.GoldDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.rs.GoldResource;
import com.jewelry.service.GoldService;
import com.jewelry.util.SystemLogger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by aramayis.antonyan on 2/26/14.
 */
@Service("goldResource")
public class GoldResourceImpl implements GoldResource {

    @Autowired
    private GoldService goldServiceImpl;

    @Override
    public ValueDTO<Boolean> updateCost(Double cost) {
        SystemLogger.d("goldResource:updateCost:" + cost);
        return new ValueDTO<Boolean>(goldServiceImpl.updateCost(cost));
    }

    @Override
    public ValueDTO<Double> getCost() {
        SystemLogger.d("goldResource:getCost:");
        return new ValueDTO<Double>(goldServiceImpl.getCost());
    }

    @Override
    public ValueDTO<List<GoldDTO>> getUpdates(DateTime startDate) {
        SystemLogger.d("goldResource:getCost:" + startDate);
        return new ValueDTO<List<GoldDTO>>(goldServiceImpl.getUpdates(startDate));
    }
}
