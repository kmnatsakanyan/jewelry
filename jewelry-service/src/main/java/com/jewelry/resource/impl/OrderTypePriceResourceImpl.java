package com.jewelry.resource.impl;


import com.jewelry.dto.OrderTypePriceDTO;
import com.jewelry.dto.OrderTypePriceUpdateDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.rs.OrderTypePriceResource;
import com.jewelry.service.OrderTypePriceService;
import com.jewelry.util.StringUtils;
import com.jewelry.util.SystemLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by aramayis.antonyan.
 */
@Service("orderTypePriceResource")
public class OrderTypePriceResourceImpl implements OrderTypePriceResource {
    @Autowired
    private OrderTypePriceService orderTypePriceServiceImpl;

    @Override
    public ValueDTO<Boolean> updateCost(OrderTypePriceUpdateDTO orderTypePriceUpdateDTO) {
        SystemLogger.d("orderTypePriceResource:updateCost:" + StringUtils.objectToString(orderTypePriceUpdateDTO));
        return new ValueDTO<Boolean>(orderTypePriceServiceImpl.updatePrice(orderTypePriceUpdateDTO.getType(), orderTypePriceUpdateDTO.getPrice()));
    }

    @Override
    public ValueDTO<List<OrderTypePriceDTO>> getUpdates() {
        SystemLogger.d("orderTypePriceResource:getUpdates:");
        return new ValueDTO<List<OrderTypePriceDTO>>(orderTypePriceServiceImpl.getOrderTypePrices());
    }
}
