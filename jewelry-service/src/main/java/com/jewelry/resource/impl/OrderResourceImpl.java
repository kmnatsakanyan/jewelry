package com.jewelry.resource.impl;

import com.jewelry.dto.OrderDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.rs.OrderResource;
import com.jewelry.service.OrderService;
import com.jewelry.util.StringUtils;
import com.jewelry.util.SystemLogger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * User: karen.mnatsakanyan
 */
@Service("orderResource")
public class OrderResourceImpl implements OrderResource {
    @Autowired
    private OrderService orderService;

    @Override
    public ValueDTO<OrderDTO> addOrder(OrderDTO orderDTO) {
        SystemLogger.d("orderResource:addOrder:" + StringUtils.objectToString(orderDTO));
        orderDTO = orderService.addOrder(orderDTO);
        return new ValueDTO<OrderDTO>(orderDTO);
    }

    @Override
    public ValueDTO<Boolean> updateOrder(OrderDTO orderDTO) {
        SystemLogger.d("orderResource:updateOrder:" + StringUtils.objectToString(orderDTO));
        orderService.updateOrder(orderDTO);
        return new ValueDTO<Boolean>(Boolean.TRUE);
    }

    @Override
    public ValueDTO<List<OrderDTO>> updateOrders(List<OrderDTO> orders) {
        if(SystemLogger.isDebug()){
            for(OrderDTO orderDTO:orders){
                SystemLogger.d("orderResource:updateOrders:" + StringUtils.objectToString(orderDTO));
            }
        }
        return new ValueDTO<List<OrderDTO>>(orderService.updateOrders(orders));

    }

    @Override
    public ValueDTO<List<OrderDTO>> getUpdates(DateTime startDate,Integer userId) {
        SystemLogger.d("orderResource:getUpdates:" +startDate+":"+userId);
        return new ValueDTO<List<OrderDTO>>(orderService.getUpdates(startDate, userId));
    }

    @Override
    public ValueDTO<Boolean> delete(Integer orderId) {
        SystemLogger.d("orderResource:delete:" +orderId);
        return new ValueDTO<Boolean>(orderService.delete(orderId));
    }
}
