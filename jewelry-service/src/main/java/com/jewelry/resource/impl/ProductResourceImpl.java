package com.jewelry.resource.impl;

import com.jewelry.dto.ProductDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.rs.ProductResource;
import com.jewelry.service.ProductService;
import com.jewelry.util.StringUtils;
import com.jewelry.util.SystemLogger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Mos on 24.02.14.
 */
@Service("productResource")
public class ProductResourceImpl implements ProductResource {
    @Autowired
    private ProductService productServiceImpl;

    @Override
    public ValueDTO<ProductDTO> saveProduct(ProductDTO productDTO) {
        SystemLogger.d("productResource:saveProduct:" + StringUtils.objectToString(productDTO));
        productDTO = productServiceImpl.saveProduct(productDTO);
        return new ValueDTO<ProductDTO>(productDTO);
    }

    @Override
    public ValueDTO<ProductDTO> updateProduct(ProductDTO productDTO) {
        SystemLogger.d("productResource:updateProduct:" + StringUtils.objectToString(productDTO));
        ProductDTO upd = productServiceImpl.updateProduct(productDTO);
        return new ValueDTO<ProductDTO>(upd);
    }

    @Override
    public ValueDTO<Boolean> updateProductList(List<ProductDTO> productDTOs) {
        if(SystemLogger.isDebug()){
            for(ProductDTO productDTO:productDTOs){
                SystemLogger.d("productResource:updateProductList:" + StringUtils.objectToString(productDTO));
            }
        }
        productServiceImpl.updateProductList(productDTOs);
        return new ValueDTO<Boolean>(Boolean.TRUE);
    }

    @Override
    public ValueDTO<List<ProductDTO>> getUpdates(DateTime startDate) {
        SystemLogger.d("productResource:getUpdates:" + startDate);
        return new ValueDTO<List<ProductDTO>>(productServiceImpl.getUpdates(startDate));
    }
}
