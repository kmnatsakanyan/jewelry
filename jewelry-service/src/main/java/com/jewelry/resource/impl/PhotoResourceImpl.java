package com.jewelry.resource.impl;

import com.jewelry.dto.PhotoDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.rs.PhotoResource;
import com.jewelry.service.PhotoService;
import com.jewelry.util.JewelryConstant;
import com.jewelry.util.ServiceConfigs;
import com.jewelry.util.StringUtils;
import com.jewelry.util.SystemLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.io.File;

/**
 * User: karen.mnatsakanyan
 */
@Service("photoResource")
public class PhotoResourceImpl implements PhotoResource {

    @Autowired
    private PhotoService photoServiceImpl;

    @Autowired
    private ServiceConfigs serviceConfigs;

    @Override
    public ValueDTO<Boolean> save(PhotoDTO photoDTO) {
        SystemLogger.d("photoResource:save:" + StringUtils.objectToString(photoDTO));
        return new ValueDTO<Boolean>(photoServiceImpl.save(photoDTO.getProductId(), photoDTO.getPhoto()));
    }

    @Override
    public Response get(String photo) {
        SystemLogger.d("photoResource:get:" + StringUtils.objectToString(photo));
        File file = new File(serviceConfigs.getPhotosURL() + photo);
        if (file.exists()) {
            return Response.ok(file, "image/jpg").build();
        } else {
            return Response.status(JewelryConstant.RESPONSE_ERROR).entity("Photo does not exists").build();
        }
    }

}
