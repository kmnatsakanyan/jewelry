package com.jewelry.resource.impl;

import com.jewelry.dto.StonePriceDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.rs.StonePriceResource;
import com.jewelry.service.StonePriceService;
import com.jewelry.util.StringUtils;
import com.jewelry.util.SystemLogger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by aramayis.antonyan on 2/28/14.
 */
@Service("stonePriceResource")
public class StonePriceResourceImpl implements StonePriceResource {

    @Autowired
    private StonePriceService stonePriceService;

    @Override
    public ValueDTO<Boolean> updatePrice(List<StonePriceDTO> stonePrices) {
        if (SystemLogger.isDebug()) {
            for (StonePriceDTO stonePriceDTO : stonePrices) {
                SystemLogger.d("stonePriceResource:updatePrice:" + StringUtils.objectToString(stonePriceDTO));
            }
        }

        return new ValueDTO<Boolean>(stonePriceService.updatePrice(stonePrices));
    }

    @Override
    public ValueDTO<List<StonePriceDTO>> getPrices() {
        SystemLogger.d("stonePriceResource:getPrices:");
        return new ValueDTO<List<StonePriceDTO>>(stonePriceService.getPrices());
    }

    @Override
    public ValueDTO<List<StonePriceDTO>> addPrice(List<StonePriceDTO> priceList) {
        if (SystemLogger.isDebug()) {
            for (StonePriceDTO stonePriceDTO : priceList) {
                SystemLogger.d("stonePriceResource:addPrice:" + StringUtils.objectToString(stonePriceDTO));
            }
        }
        return new ValueDTO<List<StonePriceDTO>>(stonePriceService.addPrice(priceList));
    }

    @Override
    public ValueDTO<List<StonePriceDTO>> getUpdates(DateTime startDate) {
        SystemLogger.d("stonePriceResource:getPrices:" + startDate);
        return new ValueDTO<List<StonePriceDTO>>(stonePriceService.getUpdates(startDate));
    }
}
