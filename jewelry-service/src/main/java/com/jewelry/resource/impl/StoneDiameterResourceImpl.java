package com.jewelry.resource.impl;

import com.jewelry.dto.StoneDiameterDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.rs.StoneDiameterResource;
import com.jewelry.service.StoneDiameterService;
import com.jewelry.util.StringUtils;
import com.jewelry.util.SystemLogger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
@Service("stoneDiameterResource")
public class StoneDiameterResourceImpl implements StoneDiameterResource {

    @Autowired
    private StoneDiameterService stoneDiameterService;

    @Override
    public ValueDTO<List<StoneDiameterDTO>> getDiameters() {
        SystemLogger.d("stoneDiameterResource:getDiameters:");
        return new ValueDTO<List<StoneDiameterDTO>>(stoneDiameterService.getDiametersList());
    }

    @Override
    public ValueDTO<List<StoneDiameterDTO>> getUpdates(DateTime startDate) {
        SystemLogger.d("stoneDiameterResource:getUpdates:"+startDate);
        return new ValueDTO<List<StoneDiameterDTO>>(stoneDiameterService.getUpdates(startDate));
    }

    @Override
    public ValueDTO<StoneDiameterDTO> saveDiameter(StoneDiameterDTO diameterDTO) {
        SystemLogger.d("stoneDiameterResource:saveDiameter:"+ StringUtils.objectToString(diameterDTO));
        diameterDTO.setId(stoneDiameterService.saveDiameter(diameterDTO));
        return new ValueDTO<StoneDiameterDTO>(diameterDTO);
    }

    @Override
    public ValueDTO<Boolean> delete(Integer diameterId) {
        SystemLogger.d("stoneDiameterResource:delete:"+diameterId);
        return new ValueDTO<Boolean>(stoneDiameterService.delete(diameterId));
    }
}
