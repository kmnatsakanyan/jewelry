package com.jewelry.resource.impl;

import com.jewelry.dto.CatalogDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.rs.CatalogResource;
import com.jewelry.service.CatalogService;
import com.jewelry.util.StringUtils;
import com.jewelry.util.SystemLogger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * User: karen.mnatsakanyan
 */
@Service("catalogResource")
public class CatalogResourceImpl implements CatalogResource {

    @Autowired
    private CatalogService catalogService;

    @Override
    public ValueDTO<CatalogDTO> addCatalog(CatalogDTO catalogDTO) {
        SystemLogger.d("catalogResource:addCatalog:" + StringUtils.objectToString(catalogDTO));
        catalogDTO = catalogService.saveCatalog(catalogDTO);
        return new ValueDTO<CatalogDTO>(catalogDTO);
    }

    @Override
    public ValueDTO<CatalogDTO> updateCatalog(CatalogDTO catalogDTO) {
        SystemLogger.d("catalogResource:updateCatalog:" + StringUtils.objectToString(catalogDTO));
        catalogDTO = catalogService.updateCatalog(catalogDTO);
        return new ValueDTO<CatalogDTO>(catalogDTO);
    }

    @Override
    public ValueDTO<List<CatalogDTO>> getUpdates(DateTime startDate) {
        SystemLogger.d("catalogResource:getUpdates:" + startDate);
        return new ValueDTO<List<CatalogDTO>>(catalogService.getUpdates(startDate));
    }

    @Override
    public ValueDTO<List<CatalogDTO>> getAll() {
        SystemLogger.d("catalogResource:getAll:");
        return new ValueDTO<List<CatalogDTO>>(catalogService.getAll());
    }

    @Override
    public ValueDTO<Boolean> delete(Integer userId) {
        SystemLogger.d("catalogResource:delete:" + userId);
        return new ValueDTO<Boolean>(catalogService.delete(userId));
    }
}
