package com.jewelry.resource.impl;

import com.jewelry.dto.StoneQualityDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.rs.StoneQualityResource;
import com.jewelry.service.StoneQualityService;
import com.jewelry.util.StringUtils;
import com.jewelry.util.SystemLogger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by aramayis.antonyan on 2/27/14.
 */
@Service("stoneQualityResource")
public class StoneQualityResourceImpl implements StoneQualityResource {

    @Autowired
    private StoneQualityService stoneQualityService;


    @Override
    public ValueDTO<List<StoneQualityDTO>> getQualities() {
        SystemLogger.d("stoneQualityResource:getQualities:");
        return new ValueDTO<List<StoneQualityDTO>>(stoneQualityService.getQualityList());
    }

    @Override
    public ValueDTO<List<StoneQualityDTO>> getUpdates(DateTime startDate) {
        SystemLogger.d("stoneQualityResource:getUpdates:"+startDate);
        return new ValueDTO<List<StoneQualityDTO>>(stoneQualityService.getUpdates(startDate));
    }


    @Override
    public ValueDTO<StoneQualityDTO> saveQuality(StoneQualityDTO qualityDTO) {
        SystemLogger.d("stoneQualityResource:saveQuality:" + StringUtils.objectToString(qualityDTO));
        qualityDTO.setId(stoneQualityService.saveQuality(qualityDTO));
        return new ValueDTO<StoneQualityDTO>(qualityDTO);
    }

    @Override
    public ValueDTO<Boolean> delete(Integer qualityId) {
        SystemLogger.d("stoneQualityResource:delete:" + qualityId);
        return new ValueDTO<Boolean>(stoneQualityService.delete(qualityId));
    }
}
