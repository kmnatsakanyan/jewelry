package com.jewelry.resource.impl;

import com.jewelry.dto.DiamondWeightDTO;
import com.jewelry.dto.ValueDTO;
import com.jewelry.rs.DiamondWeightResource;
import com.jewelry.service.DiamondWeightService;
import com.jewelry.util.SystemLogger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by aramayis.antonyan on 5/10/14.
 */
@Service("diamondResource")
public class DiamondWeightResourceImpl implements DiamondWeightResource {

    @Autowired
    private DiamondWeightService diamondWeightService;


    @Override
    public ValueDTO<List<DiamondWeightDTO>> getWeightList() {
        SystemLogger.d("diamondResource:getWeightList:");
        return new ValueDTO<List<DiamondWeightDTO>>(diamondWeightService.getWeightList());
    }

    @Override
    public ValueDTO<List<DiamondWeightDTO>> getUpdates(DateTime startDate) {
        SystemLogger.d("diamondResource:getUpdates:" + startDate);
        return new ValueDTO<List<DiamondWeightDTO>>(diamondWeightService.getUpdates(startDate));
    }

    @Override
    public ValueDTO<Boolean> loadDiamondWeightFromCSV() {
        SystemLogger.d("diamondResource:loadDiamondWeightFromCSV:");
        diamondWeightService.loadDiamondWeightFromCSV();
        return new ValueDTO<Boolean>(true);
    }
}
